<?php

class conexion
{

    private $user;
    private $password;
    private $server;
    private $database;
    private $con;

    public function __construct()
    {
        $user = 'root';
        $server = "localhost";
        $database = "icontcarlitos";
        $password = '';
        $this->con = new mysqli($server, $user, $password, $database);
    }


    public function getTotalByMonthVentas($mes,$anio)
    {
        $query = $this->con->query("SELECT SUM(cantidad * precio) as total, DAY(fecha) as dia FROM ventasdetalle WHERE MONTH(fecha) = '$mes' AND YEAR(fecha) = '$anio' GROUP BY DAY(fecha) ASC");
        return $query;
    }

    public function getTotal6MonthVentas()
    {
        $query = $this->con->query("SELECT SUM(cantidad * precio) as total, MonthName(fecha) as mes FROM ventasdetalle WHERE fecha BETWEEN date_sub(now(), interval 6 month) AND NOW() GROUP BY MONTH(fecha) ASC");
        return $query;
    }

    public function getTotalYearVentas($anio)
    {
        $query = $this->con->query("SELECT SUM(cantidad * precio) as total, MonthName(fecha) as mes FROM ventasdetalle  WHERE  YEAR(fecha) = '$anio'   GROUP BY MONTH(fecha) ASC");
        return $query;
    }

    public function getTotalVentas()
    {
        $query = $this->con->query("SELECT SUM(cantidad * precio) as total, MonthName(fecha) as mes FROM ventasdetalle  GROUP BY MONTH(fecha) ASC");
        return $query;
    }

    public function getSearchContact($nitClient)
    {
        $query = $this->con->query("select * from cliente2 where ci = '".$nitClient."'");
        return $query;
    }

    public function getPreventaSales()
    {
        $query = $this->con->query('SELECT idpreventa,imagen,producto, count(producto) AS cantidad, sum(precio) as totalPrecio, idproducto, pventa, idUser, precio ,tipo
                                           FROM `preventa` GROUP BY producto , idproducto ,tipo ORDER BY idpreventa ASC');
        return $query;
    }

    public function getTotalPreventaSales()
    {
        $query = $this->con->query('SELECT SUM( precio ) as total , idUser  FROM `preventa`');
        return $query;
    }

    public function getAllUsers()
    {
        $query = $this->con->query('SELECT nombre, password,tipo,foto,login FROM usuarios');

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }

        return $retorno;
    }

    public function getAllUsersData()
    {
        $query = $this->con->query('SELECT * FROM usuarios');
        return $query;
    }

    public function getAllTipoProducto()
    {
        $query = $this->con->query('SELECT * FROM tipoproducto');
        return $query;
    }

    public function getAllProveedor()
    {
        $query = $this->con->query('SELECT * FROM proveedor');
        return $query;
    }

    public function getVentasDetalle()
    {
        $query = $this->con->query('SELECT * FROM ventasdetalle WHERE estadoTransaccion != \'Consolidado\'');
        return $query;
    }

    public function getAllProductos()
    {
        $query = $this->con->query('SELECT * FROM `producto`');
        return $query;
    }

    public function getAllGastos()
    {
        $query = $this->con->query('SELECT * FROM gastos order by idgastos desc ');
        return $query;
    }

    public function getAllPedido()
    {
        $query = $this->con->query('SELECT * FROM pedido order by idpedido desc ');
        return $query;
    }
    public function getAllStockProductos()
    {
        $query = $this->con->query('SELECT * ,Round(cantidadVendido*100/cantidadStock,0) as porcentaje FROM stock order by cantidadStock ASC');
        return $query;
    }

    public function getAllStockActualProductos()
    {
        $query = $this->con->query('SELECT * FROM `stock`');
        return $query;
    }

    public function getTipoProducto()
    {
        $query = $this->con->query('SELECT * FROM `tipoproducto`');
        return $query;
    }

    public function getTipoProductoEdit()
    {
        $query = $this->con->query('SELECT * FROM `tipoproducto`');
        return $query;
    }

    public function getAllClient()
    {
        $query = $this->con->query('SELECT * FROM `cliente`');
        return $query;
    }

    public function getTipoMensajeAlerta()
    {
        $query = $this->con->query('SELECT * FROM `alerta`');
        return $query;
    }

    public function getMensajeAlerta()
    {
        $query = $this->con->query('SELECT * FROM `mensajeAlerta`');
        return $query;
    }

    public function getVentasByProducto($fechaVentasI,$fechaVentasF)
    {
        $query = $this->con->query("SELECT *,SUM(cantidad) as cantidadVendida,SUM(total) as totalVenta ,  (SELECT  SUM(total) AS totalVentas
                                           FROM `ventasDiaC`
                                           WHERE fecha
                                           BETWEEN '" . $fechaVentasI . "'  AND '" . $fechaVentasF . "' ) as ventasTotaporFecha  
                                           FROM `ventasdiac`  WHERE fecha
                                           BETWEEN '" . $fechaVentasI . "'  AND '" . $fechaVentasF . "'  GROUP by producto");
        return $query;
    }

    public function getGatosDeLaEmpresa($fechaVentasI,$fechaVentasF)
    {
        $query = $this->con->query("SELECT  *
                                           FROM `gastos`
                                           WHERE fechaRegistro 
                                           BETWEEN '" . $fechaVentasI . "'  AND '" . $fechaVentasF . "' ");
        return $query;
    }

    public function getEntradasDeLaEmpresa($fechaVentasI,$fechaVentasF)
    {
        $query = $this->con->query("SELECT  SUM(entrada) as totalEntrada
                                           FROM `gastos`
                                           WHERE fechaRegistro 
                                           BETWEEN '" . $fechaVentasI . "'  AND '" . $fechaVentasF . "' ");
        return $query;
    }

    public function getUtilidadDeLaEmpresa($fechaVentasI,$fechaVentasF)
    {
        $query = $this->con->query("SELECT  (SUM(entrada) - SUM(salida)) as utilidad 
                                           FROM `gastos`
                                           WHERE fechaRegistro 
                                           BETWEEN '" . $fechaVentasI . "'  AND '" . $fechaVentasF . "' ");
        return $query;
    }

    public function getTotalGatosDeLaEmpresa($fechaVentasI,$fechaVentasF)
    {
        $query = $this->con->query("SELECT  SUM(salida) as totalSalida
                                           FROM `gastos`
                                           WHERE fechaRegistro 
                                           BETWEEN '" . $fechaVentasI . "'  AND '" . $fechaVentasF . "' ");
        return $query;
    }

    public function getVentasByProductoDia($fechaVentasI,$fechaVentasF)
    {
        $query = $this->con->query("SELECT * FROM ventasdiac
                                            WHERE fecha >= '" . $fechaVentasI . "' and fecha <= '" . $fechaVentasF . "'");
        return $query;
    }

    public function getVentasTotalByProductoDia($fechaVentasI,$fechaVentasF)
    {
        $query = $this->con->query("SELECT  SUM(total) AS totalVentas FROM ventasdiac
                                            WHERE fecha >= '" . $fechaVentasI . "' and fecha <= '" . $fechaVentasF . "'");
        return $query;
    }


    public function getVentasByDia($fechaVentasI,$fechaVentasF)
    {
        $query = $this->con->query("SELECT * FROM ventasdetalle
                                            WHERE fecha >= '" . $fechaVentasI . "' and fecha <= '" . $fechaVentasF . "'");
        return $query;
    }

    public function getVentasTotalByDia($fechaVentasI,$fechaVentasF)
    {
        $query = $this->con->query("SELECT  SUM(total) AS totalVentas FROM ventasdetalle
                                            WHERE fecha >= '" . $fechaVentasI . "' and fecha <= '" . $fechaVentasF . "'");
        return $query;
    }






    public function updateMensajeAlerta($mensaje)
    {
        $query = $this->con->query("UPDATE `mensajeAlerta` SET `mensaje` ='".$mensaje." ' WHERE `mensajeAlerta`.`mensajeAlertaId` = 1");
        return $query;
    }

    public function updateAlerta($alerta)
    {
        $query = $this->con->query("UPDATE `alerta` SET `tipoAlerta` ='".$alerta." ' WHERE `alerta`.`alertaId` = 1");
        return $query;
    }
    
    public function getUser($usuario, $contrasena)
    {
        $query = $this->con->query("SELECT * FROM usuarios WHERE login='" . $usuario . "' AND password='" . $contrasena . "'");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getUserById($userId)
    {
        $query = $this->con->query(" SELECT * FROM `usuarios` WHERE id_usu='".$userId."' ");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }



    public function getStatus()
    {
        $query = $this->con->query("SELECT * FROM `menu` WHERE estado='Activo' ");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getMenu()
    {
        $query = $this->con->query("SELECT * FROM `menu` ");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getUpdateMenu($idmenu, $color){
        $query = $this->con->query( "UPDATE `menu` SET `color` = '$color' WHERE `idmenu` = $idmenu");
        return $query;
    }

    public function getAllUpdateMenu($idmenu, $color){
        $query = $this->con->query( "UPDATE `menu` SET `color` = '$color' WHERE `idmenu` != $idmenu");
        return $query;
    }

    public function getProduct()
    {
        $query = $this->con->query("SELECT * FROM producto");
        return $query;
    }

    public function getPreVenta()
    {
        $query = $this->con->query("SELECT idpreventa,imagen,producto, count(producto) AS cantidad, sum(precio) as totalPrecio, idproducto, pventa, idUser, precio ,tipo
                                           FROM `preventa` GROUP BY producto , idproducto ,tipo ORDER BY idpreventa ASC");
        return $query;
    }

    public function getTotalPreVenta()
    {
        $query = $this->con->query("SELECT SUM( precio ) as total  FROM `preventa`");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }


    public function deleteUnaPreVenta($idproducto,$tipo){
        $query = $this->con->query("DELETE FROM `preventa` WHERE `preventa`.`idproducto` = $idproducto and `preventa`.`tipo` = '$tipo'");
        return $query;
    }

    public function deleteAllPreVenta($idUser){
        $query = $this->con->query("DELETE FROM `preventa` WHERE `preventa`.`idUser` = $idUser");
        return $query;
    }

    public function editPreVenta($idproducto,$tipo){

        $query = $this->con->query("SELECT * FROM preventa WHERE idproducto=$idproducto and tipo='$tipo'" );

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getCantidad($idproducto,$tipoPedido){

        $query = $this->con->query("SELECT  count( idproducto ) AS cantidad  FROM `preventa`  where idproducto = '$idproducto' and tipo='$tipoPedido'");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function deleteCantidadActual($idproducto,$tipoPedido){
        $query = $this->con->query("DELETE FROM `preventa` WHERE `preventa`.`idProducto` = '$idproducto' and  `preventa`.`tipo` = '$tipoPedido'");
        return $query;
    }

    public function insertNewPreventa($imagen,$productoUpdate,$precio,$idproducto,$pventa,$userId,$tipoPedido){

        $query = $this->con->query("INSERT INTO preventa (`idPreventa`, `imagen`, `producto`, `precio`, `idProducto`, `pventa`, `idUser`, `tipo`)
			                      	VALUES (NULL,'$imagen','$productoUpdate','$precio','$idproducto','$pventa', $userId, '$tipoPedido')");
        return $query;
    }

    public function getPreventaTotal(){

        $query = $this->con->query("SELECT  SUM(precio) as pventa FROM preventa");
        return $query;
    }

    public function getVentasMensuales(){

        $query = $this->con->query("SELECT MonthName(fecha) as mes FROM ventasdetalle GROUP BY MONTH(fecha) ASC");
        return $query;
    }

    public function getSumTotalVentasMensuales(){

        $query = $this->con->query("SELECT MonthName(fecha) as mes FROM ventasdiac  GROUP BY MONTH(fecha) ASC");
        return $query;
    }

    public function getTotalVentasMensual(){
        $query = $this->con->query("SELECT SUM(cantidad * precio) as total, MonthName(fecha) as mes FROM ventasdiac  GROUP BY MONTH(fecha) ASC");
        return $query;
    }

    public function getTotalVentasByAnio($anio){
        $query = $this->con->query("SELECT SUM(cantidad * precio) as total, MonthName(fecha) as mes FROM ventasdiac  WHERE  YEAR(fecha) = '$anio'   GROUP BY MONTH(fecha) ASC");
        return $query;
    }

    public function getGrandTotalVentas6Meses(){
        $query = $this->con->query("SELECT SUM(cantidad * precio) as totalVentas FROM ventasdiac WHERE fecha BETWEEN date_sub(now(), interval 6 month) AND NOW()");
        return $query;
    }

    public function getTotalVentasByMes($mes,$anio){
         $query = $this->con->query("SELECT SUM(cantidad * precio) as total, DAY(fecha) as dia FROM ventasdetalle WHERE MONTH(fecha) = '$mes' AND YEAR(fecha) = '$anio' GROUP BY DAY(fecha) ASC");
        return $query;
    }

    public function getSumaTotalVentasByMes($mes,$anio){
        $query = $this->con->query("SELECT SUM(cantidad * precio) as totalVentas FROM ventasdetalle WHERE MONTH(fecha) = '$mes' AND YEAR(fecha) = '$anio'");
        return $query;
    }

    public function getTotalVentasByYear($anio){
        $query = $this->con->query("SELECT SUM(cantidad * precio) as totalVentas FROM ventasdetalle WHERE  YEAR(fecha) = '$anio'");
        return $query;
    }


    public function getTotalVentas6Meses(){
       $query = $this->con->query("SELECT SUM(cantidad * precio) as total, MonthName(fecha) as mes FROM ventasdiac  WHERE fecha BETWEEN date_sub(now(), interval 6 month) AND NOW() GROUP BY MONTH(fecha) ASC");
        return $query;
    }

    public function getUserSession(){

        $query = $this->con->query("SELECT * FROM `datosUsuarioSesion` limit 1");
        return $query;
    }

    public function getUserPreventa($ci){
        $query = $this->con->query("select * from cliente2 where ci = $ci ");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function registerPreventa($nombre, $nitci, $fecha, $totalApagar, $efectivo, $cambio, $idClientei){

        $query = $this->con->query( "INSERT INTO `clienteDato` (`idCliente`, `nombre`, `ci`, `fecha`, `totalApagar`, `efectivo`, `cambio` , `idClientei`) 
                                           VALUES (NULL, '$nombre', '$nitci', '$fecha', '$totalApagar', '$efectivo', '$cambio', '$idClientei')");
        return $query;
    }

    public function registerNewCi($nombre, $nitci){

        $query = $this->con->query( "INSERT INTO `cliente2` (`idcliente`, `nombre`, `ci`) VALUES (NULL, '$nombre', '$nitci')");
        return $query;
    }

    public function registerNewTipo($TipoProducto){

        $query = $this->con->query( "INSERT INTO `tipoproducto` (`idtipoproducto`, `tipoproducto`) VALUES (NULL, '$TipoProducto')");
        return $query;
    }

    public function getDataFactura(){

        $query = $this->con->query("SELECT  * from datos ");
        return $query;
    }

    public function getMoneda(){

        $query = $this->con->query("SELECT  * from moneda ");
        return $query;
    }

    public function getIdioma(){

        $query = $this->con->query("SELECT  * from idioma ");
        return $query;
    }


    public function getDataCodigoControl(){

        $query = $this->con->query("SELECT  * from codigocontrol ");
        return $query;
    }

    public function getDataFacturaTiquet(){

        $query = $this->con->query("SELECT  * from datosFactura ");
        return $query;
    }

    public function getDataLibroDeVenta(){

        $query = $this->con->query("SELECT  * from librov  order by idlibro DESC LIMIT 1 ");
        return $query;
    }

    public function getClientDataFactura(){

        $query = $this->con->query("SELECT * from clientedato order by idcliente DESC limit 1 ");
        return $query;
    }

    public function getVentaTotalForFactura(){

        $query = $this->con->query("SELECT * from clientetotal limit 1");
        return $query;
    }

    public function getPedidoTotalForFactura(){
        $query = $this->con->query("SELECT idpreventa,imagen,producto,precio, count( idproducto ) AS cantidad, precio*count( idproducto ) as totalPrecio, idproducto, pventa ,tipo FROM `preventa`  GROUP BY idproducto");
        return $query;
    }

    public function getTotalVentaForFactura(){
        $query = $this->con->query("SELECT SUM( precio ) as total  FROM `preventa`");
        return $query;
    }

    public function getVentasDiaCurrent(){
        $query = $this->con->query("select (count(*)+1) as maxid from ventasdia");
        return $query;
    }

    public function registerPreventaDetalle($fecha, $codTransaccion, $nombre, $cantidad, $producto, $precio, $total, $tipo, $usuario, $estadoTransaccion){

        $query = $this->con->query( "INSERT INTO `ventasDetalle` (`ventasDetalleId`, `fecha`, `codTransaccion`, `nombre`, `cantidad`, `producto`, `precio`, `total`, `tipo`, `usuario`, `estadoTransaccion`,`commentario`) 
                                           VALUES (NULL, '$fecha', '$codTransaccion', '$nombre', '$cantidad', '$producto', '$precio', '$total', '$tipo', '$usuario', '$estadoTransaccion', '')");
        return $query;
    }

    public function getPreVentaTotalByPerson()
    {
        $query = $this->con->query("SELECT sum(precio) as totalPrecio, idUser FROM `preventa` GROUP BY idUser");
        return $query;
    }

    public function registerPreventaDetalleTotal($fecha, $codTransaccion, $nombre, $total, $usuario, $estadoTransaccion){

        $query = $this->con->query( "INSERT INTO `ventasDetalleTotal` (`ventasDetalleId`, `fecha`, `codTransaccion`, `nombre`, `total`, `usuario`, `estadoTransaccion`) 
                                            VALUES (NULL, '$fecha', '$codTransaccion', '$nombre', '$total', '$usuario', '$estadoTransaccion')");
        return $query;
    }


    public function cleanRegisterPreventa(){

        $query = $this->con->query( "truncate `preventa`");
        return $query;
    }

    public function cleanCodigoControl(){

        $query = $this->con->query( "truncate `codigoTransaccion`");
        return $query;
    }

    public function cleanClienteData(){

        $query = $this->con->query( "truncate `clientedato`");
        return $query;
    }
    public function cleanDataFactura(){

        $query = $this->con->query( "truncate `datosFactura`");
        return $query;
    }

    public function getProductoData($idproducto){
        $query = $this->con->query("SELECT  * FROM producto where idproducto='$idproducto'");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function registerPreventaProducto($imagen, $producto, $precio, $idProducto, $pventa, $idUser, $tipo){

        $query = $this->con->query( "INSERT INTO `preventa` (`idPreventa`, `imagen`, `producto`, `precio`, `idProducto`, `pventa`, `idUser`, `tipo`) VALUES
                                           (NULL, '$imagen', '$producto', '$precio', '$idProducto', '$pventa', '$idUser', '$tipo')");
        return $query;
    }


    public function registerDatosFactura($nit, $cambio, $efectivo, $totalApagar, $usuario, $password){

        $query = $this->con->query( "INSERT INTO `datosFactura` (`datosFacturaId`, `nit`, `cambio`, `efectivo`, `totalApagar`, `usuario`, `password`) 
                                            VALUES (NULL, '$nit', '$cambio', '$efectivo', '$totalApagar', '$usuario', '$password');");
        return $query;
    }



    public function registerCodigoControl($codigo){

        $query = $this->con->query( "INSERT INTO `codigoTransaccion` (`idCodigo`, `codigoTransaccion`) VALUES (NULL, '$codigo')");
        return $query;
    }

    public function getCodigoControl()
    {
        $query = $this->con->query("SELECT * FROM `codigoTransaccion`");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getVentasDetalleTotal()
    {
        $query = $this->con->query("SELECT count(*) as total FROM `ventasDetalleTotal`");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function registerDatosCliente($nombre,$ci,$fecha,$totalApagar,$efectivo,$cambio,$dClientei,$tipoVenta){

        $query = $this->con->query( "INSERT INTO `clientedato` (`idCliente`, `nombre`, `ci`, `fecha`, `totalApagar`, `efectivo`, `cambio`, `idClientei`, `tipoVenta`)
                                                               VALUES (NULL, '$nombre', '$ci', '$fecha', '$totalApagar', '$efectivo', '$cambio', '$dClientei', '$tipoVenta')");
        return $query;
    }

    public function registerNewUser($nombre, $tipo, $login, $password, $destino){

        echo "INSERT INTO `usuarios` (`id_usu`, `login`, `tipo`, `nombre`, `password`, `foto`) VALUES (NULL, '$login', '$tipo', '$nombre', '$password', '$destino')";

        $query = $this->con->query( "INSERT INTO `usuarios` (`id_usu`, `login`, `tipo`, `nombre`, `password`, `foto`) 
                                            VALUES (NULL, '$login', '$tipo', '$nombre', '$password', '$destino')");
        return $query;
    }

    public function deleteUsuario($idUsuario){
        $query = $this->con->query("DELETE FROM usuarios WHERE id_usu=$idUsuario");
        return $query;
    }

    public function updateUsuario($idUsuario,$nombre, $tipo, $login, $password,$destino){
        $query = $this->con->query("UPDATE  `usuarios` SET  `login` =  '$login', 
                                                                   `tipo` =  '$tipo', 
                                                                    `nombre` =  '$nombre', 
                                                                    `password` =  '$password' ,`foto` =  '$destino' WHERE  `usuarios`.`id_usu` ='$idUsuario'");
        return $query;
    }


    public function updateDataFactura($iddatos, $propietario, $razon, $direccion, $nro,$telefono){
        $query = $this->con->query("UPDATE  `datos` SET  `propietario` =  '$propietario', 
                                                                `razon` =  '$razon', 
                                                                `direccion` =  '$direccion', 
                                                                `telefono` =  '$telefono', 
                                                                `nro` =  '$nro'    WHERE  `datos`.`iddatos` ='$iddatos'");

        return $query;
    }

    public function updateMoneda($idMoneda, $pais, $tipoMoneda, $contexto){
        $query = $this->con->query("UPDATE `moneda` SET 
                                                  `pais` = '$pais', 
                                                  `tipoMoneda` = '$tipoMoneda', 
                                                  `contexto` = '$contexto' 
                                                   WHERE `moneda`.`idMoneda` = $idMoneda");

        return $query;
    }

    public function updateIdioma($idioma, $idIdioma){
        $query = $this->con->query("UPDATE `idioma` 
                                         SET `idioma` = '$idioma' WHERE `idioma`.`idIdioma` = $idIdioma");

        return $query;
    }

    public function updateMenuIdioma(){
        $query = $this->con->query("UPDATE `idioma` 
                                         SET `idioma` = '$idioma' WHERE `idioma`.`idIdioma` = $idIdioma");

        return $query;
    }

    public function deleteProveedor($idProveedor){
        $query = $this->con->query("DELETE FROM proveedor WHERE idproveedor=$idProveedor");
        return $query;
    }

    public function deleteClient($idCliente){
        $query = $this->con->query("DELETE FROM cliente WHERE idcliente=$idCliente");
        return $query;
    }

    public function deleteProducto($idProducto){
        $query = $this->con->query("DELETE FROM producto WHERE idproducto=$idProducto");
        return $query;
    }

    public function deleteAccount($idAccount){
        $query = $this->con->query("DELETE FROM gastos WHERE idgastos=$idAccount");
        return $query;
    }

    public function deleteTipoProducto($idAccount){
        $query = $this->con->query("DELETE FROM `tipoproducto` WHERE `idtipoproducto` = $idAccount");
        return $query;
    }

    public function updateTipoProducto($idTipoProducto,$tipoProducto){

        $query = $this->con->query( "UPDATE `tipoproducto` SET `tipoproducto` = '$tipoProducto' WHERE `idtipoproducto` = $idTipoProducto");
        return $query;
    }


    public function deletePedido($idPedido){
        $query = $this->con->query("DELETE FROM pedido WHERE idPedido=$idPedido");
        return $query;
    }

    public function consolidarPedido($idPedido){
        $query = $this->con->query("UPDATE `ventasdetalle` SET `estadoTransaccion` = 'Consolidado' WHERE `ventasDetalleId` = $idPedido");
        return $query;
    }

    public function deleteStockProducto($idProducto){
        $query = $this->con->query("DELETE FROM stock WHERE idproducto=$idProducto");
        return $query;
    }

    public function registerNewProveedor($proveedor, $responsable, $direccion, $telefono, $fechaRegistro){

        $query = $this->con->query( "INSERT INTO `proveedor` (`idproveedor`, `proveedor`, `responsable`, `fechaRegistro`, `direccion`, `telefono`, `estado`, `fechaAviso`, `valor`, `valorCobrado`, `saldo`)
        VALUES (NULL, '$proveedor', '$responsable', '$fechaRegistro', '$direccion', '$telefono', 'Registro', '2020-05-14', '100', '50', 'Saldo')");
        return $query;
    }

    public function registerNewCliente($nuevo_archivo, $nombre, $apellido, $direccion, $telefonoFijo,$telefonoCelular,$email,$contactoReferencia,$telefonoReferencia,$observaciones,$fechaRegistro,$ci,$destino){

        $query = $this->con->query( "INSERT INTO cliente(foto, nombre, apellido, direccion, telefonoFijo, telefonoCelular, email, contactoReferencia, telefonoReferencia, observaciones, fechaRegistro, ci)
        VALUES ('$destino','$nombre','$apellido','$direccion','$telefonoFijo','$telefonoCelular','$email','$contactoReferencia','$telefonoReferencia','$observaciones','$fechaRegistro','$ci')");
        return $query;
    }

    public function registerNewProducto($destino, $codigo, $nombreProducto, $cantidad, $fechaRegistro, $precioVenta, $proveedor,$tipoproducto, $precioCompra){

        $query = $this->con->query( "INSERT INTO `producto` (`idproducto`, `imagen`, `codigo`, `nombreProducto`, `cantidad`, `fechaRegistro`, `precioVenta`, `tipo`, `proveedor`, `precioCompra`) 
         VALUES ('', '$destino', '$codigo', '$nombreProducto', '$cantidad', '$fechaRegistro', '$precioVenta', '$tipoproducto', '$proveedor', '$precioCompra')");
        return $query;
    }

    public function registerNewAccount($tipo, $descripcion, $entrada, $fechaRegistro, $usuario,$salida){

        $query = $this->con->query( "INSERT INTO `gastos` (`idgastos`, `descripcion`, `entrada`, `fechaRegistro`, `usuario`, `salida`, `tipo`) 
                                            VALUES (NULL, '$descripcion', '$entrada', '$fechaRegistro', '$usuario', '$salida', '$tipo')");
        return $query;
    }

    public function registerNewPedido($descripcion, $total, $empresa, $usuario,$fechaRegistro){

        $query = $this->con->query( "INSERT INTO `pedido` (`idPedido`, `descripcion`, `total`, `proveedor`, `usuario`, `fechaRegistro`) 
                                            VALUES (NULL, '$descripcion', '$total', '$empresa', '$usuario', '$fechaRegistro')");
        return $query;
    }

    public function updateAccount($tipo, $descripcion, $entrada, $fechaRegistro, $usuario,$salida,$idgastos){

    $query = $this->con->query( "UPDATE `gastos` SET `descripcion` = '$descripcion', 
                                                                `entrada` = '$entrada',
                                                                `fechaRegistro` = '$fechaRegistro',
                                                                 `usuario` = '$usuario', 
                                                                 `salida` = '$salida', 
                                                                 `tipo` = '$tipo' WHERE `idgastos` = $idgastos");
    return $query;
   }

    public function updatePedido($descripcion, $total, $proveedor, $usuarioLogin,$fechaRegistro,$idPedido){

        $query = $this->con->query( "UPDATE `pedido` SET `descripcion` = '$descripcion',
                                                    `total` = '$total', `proveedor` = '$proveedor',
                                                     `usuario` = '$usuarioLogin', `fechaRegistro` = '$fechaRegistro'
                                                      WHERE `pedido`.`idPedido` = $idPedido ");
        return $query;
    }


    public function updateDetalleVenta($idConsolidar, $comentario){

        $query = $this->con->query( "UPDATE `ventasdetalle` SET `commentario` = '$comentario' WHERE `ventasDetalleId` = $idConsolidar ");
        return $query;
    }



    public function registerNewProductoStock($destino, $proveedor, $codigo, $nombreProducto, $precioDeCompra, $cantidad, $alarma,$fechaRegistro){

        $query = $this->con->query( "INSERT INTO `stock` (`idproducto`, `imagen`, `codigo`, `nombreProducto`, `cantidadStock`, `cantidadVendido`, `fechaRegistro`, `proveedor`, `alarma`, `precioCompra`) 
         VALUES ('', '$destino', '$codigo', '$nombreProducto', '$cantidad','', '$fechaRegistro', '$proveedor', '$alarma', '$precioDeCompra')");
        return $query;
    }

    public function updateProducto($destino, $codigo, $nombreProducto, $cantidad, $fechaRegistro, $precioVenta, $proveedor,$tipoproducto, $precioCompra,$idProducto){

    $query = $this->con->query( "UPDATE `producto` SET `codigo` = '$codigo', 
                                                   `nombreProducto` = '$nombreProducto', 
                                                   `fechaRegistro` = '$fechaRegistro', 
                                                   `precioVenta` = '$precioVenta', 
                                                   `proveedor` = '$proveedor', 
                                                   `cantidad` = '$cantidad', 
                                                   `tipo` = '$tipoproducto', 
                                                   `precioCompra` = '$precioCompra',
                                                   `imagen` = '$destino'
                                                   WHERE `idproducto` = $idProducto");
    return $query;
}

    public function updateProductoStock($destino, $proveedor, $codigo, $nombreProducto, $precioDeCompra, $cantidad, $alarma,$fechaRegistro,$idProducto){

        $query = $this->con->query( "UPDATE `stock` SET `codigo` = '$codigo', 
                                                   `nombreProducto` = '$nombreProducto', 
                                                   `fechaRegistro` = '$fechaRegistro', 
                                                   `cantidadStock` = '$cantidad', 
                                                   `proveedor` = '$proveedor', 
                                                   `alarma` = '$alarma', 
                                                   `precioCompra` = '$precioDeCompra',
                                                   `imagen` = '$destino'
                                                   WHERE `idproducto` = '$idProducto'");
        return $query;
    }

    public function editProveedor($proveedorId,$proveedor, $responsable, $direccion, $telefono, $fechaRegistro){

        $query = $this->con->query( "UPDATE `proveedor` SET `proveedor` = '$proveedor', 
                                                                   `responsable` = '$responsable', 
                                                                   `fechaRegistro` = '$fechaRegistro', 
                                                                   `direccion` = '$direccion', 
                                                                   `telefono` = '$telefono'
                                                                    WHERE `proveedor`.`idproveedor` = $proveedorId");
        return $query;
    }

    public function editClient($nombre,$apellido, $direccion, $telefonoFijo, $telefonoCelular, $fechaRegistro,$ci,$idcliente,$email,$destino){

        $query = $this->con->query( "UPDATE cliente  SET nombre='$nombre', 
			                                       apellido= '$apellido', 
			                                       direccion='$direccion', 
			                                       telefonoFijo= '$telefonoFijo', 
			                                       telefonoCelular='$telefonoCelular', 
			                                       email= '$email',
			                                       fechaRegistro='$fechaRegistro', 
			                                       foto='$destino', 
			                                       ci='$ci'
			                                       WHERE idcliente= '$idcliente'");
        return $query;
    }



}

?>