-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2020 at 09:50 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `icontcarlitos`
--

-- --------------------------------------------------------

--
-- Table structure for table `alerta`
--

CREATE TABLE `alerta` (
  `alertaId` int(11) NOT NULL,
  `tipoAlerta` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `mensaje` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `alerta`
--

INSERT INTO `alerta` (`alertaId`, `tipoAlerta`, `mensaje`) VALUES
(1, ' ', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cantidad`
--

CREATE TABLE `cantidad` (
  `id_cantidad` int(11) NOT NULL,
  `nom_cantidad` int(11) NOT NULL,
  `idproducto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cantidad`
--

INSERT INTO `cantidad` (`id_cantidad`, `nom_cantidad`, `idproducto`) VALUES
(1, 150, 1),
(2, 100, 2),
(3, 50, 3),
(4, 20, 4),
(5, 50, 5),
(6, 30, 6),
(7, 15, 7),
(8, 100, 8),
(9, 100, 9),
(10, 102, 10),
(11, 100, 11),
(16, 5, 12),
(17, 5, 13),
(18, 8, 14),
(19, 18, 15),
(0, 50000, 18),
(1, 150, 1),
(2, 100, 2),
(3, 50, 3),
(4, 20, 4),
(5, 50, 5),
(6, 30, 6),
(7, 15, 7),
(8, 100, 8),
(9, 100, 9),
(10, 102, 10),
(11, 100, 11),
(16, 5, 12),
(17, 5, 13),
(18, 8, 14),
(19, 18, 15),
(0, 50000, 18);

-- --------------------------------------------------------

--
-- Table structure for table `cantidadyogurt`
--

CREATE TABLE `cantidadyogurt` (
  `id` int(11) NOT NULL,
  `cantidad` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cantidadyogurt`
--

INSERT INTO `cantidadyogurt` (`id`, `cantidad`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `clase`
--

CREATE TABLE `clase` (
  `idclase` int(11) NOT NULL,
  `clase` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clase`
--

INSERT INTO `clase` (`idclase`, `clase`) VALUES
(1, 'POLLO'),
(2, 'REFRESCO'),
(4, 'HERVIDO'),
(6, 'PAPA'),
(7, 'ARROZ');

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL,
  `foto` varchar(200) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `apellido` varchar(200) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `telefonoFijo` varchar(200) NOT NULL,
  `telefonoCelular` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `contactoReferencia` varchar(200) NOT NULL,
  `telefonoReferencia` varchar(200) NOT NULL,
  `observaciones` text NOT NULL,
  `fechaRegistro` date NOT NULL,
  `ci` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`idcliente`, `foto`, `nombre`, `apellido`, `direccion`, `telefonoFijo`, `telefonoCelular`, `email`, `contactoReferencia`, `telefonoReferencia`, `observaciones`, `fechaRegistro`, `ci`) VALUES
(17, 'fotoUsuario/user.png', 'SUSANA', 'BARRETO', 'AV. GUAYACAN', '433234', '70434566', 'SUSY@HOTMAIL.ES', 'NO HAY NADA', 'HENRY', '3333234', '2016-04-15', '64446765'),
(31, 'fotoUsuario/user.png', 'peter', 'block', 'av. bolviia', '434553', '7889875', 'peter@hotmal.com', '', '', '', '2016-06-08', '566554'),
(19, 'fotoUsuario/user.png', 'Javier', 'Martinez', 'sucre', '6767676', '656565', 'nasareno_2005@hotmail.com', '', '', '', '2020-06-06', '9999999'),
(20, 'fotoUsuario/user.png', 'henry', 'calani', 'av. iempre viva', '343543', '675667', 'henpeck@hotmal.com', '', '', '', '2016-05-24', '55445'),
(29, 'fotoUsuario/user.png', 'MAURICIO', 'MOSCOSO', 'AV SANTA CRUZ', '454556', '67565445', 'maurico@hotmail.es', '', '', '', '2020-06-06', '9056574999');

-- --------------------------------------------------------

--
-- Table structure for table `cliente2`
--

CREATE TABLE `cliente2` (
  `idcliente` int(11) NOT NULL,
  `nombre` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `ci` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cliente2`
--

INSERT INTO `cliente2` (`idcliente`, `nombre`, `ci`) VALUES
(10, 'PEREZ JUAN', '44451241  '),
(17, 'BARRETO SUSANA', '64446765'),
(18, 'CALANI LETICIA', '5444565'),
(19, 'MARTINEZ HERNAN', '4545454'),
(20, 'BORTOLINI', '3454554'),
(21, 'BAPTISTA', '6756647'),
(23, 'SIMPSON', '7756576'),
(24, 'BARRETO', '344443'),
(25, 'CARSON', '454433'),
(35, 'rivera', '66767676'),
(34, 'DEL CARPIO', '7876764'),
(33, 'S/N', '0'),
(83, '', '55555'),
(84, 'Calani', '6444685'),
(82, 'Flores', '123'),
(81, 'Aguirre', '445445'),
(78, 'BARRETO SUSANA', '64446765'),
(85, 'Rojas', '4383025'),
(86, 'Leila Aranda', '50894544'),
(87, 'Javier Mendez', '45784784'),
(88, 'Maria Miranda', '453678784'),
(89, 'Daniel Miranda', '5070546'),
(90, 'Javier NiÃ±es', '4050123'),
(91, 'Caren NiÃ±a', '5060123'),
(92, 'Ricardo Mamani', '3783783833'),
(93, 'Carlos Rocha', '1234567'),
(94, 'goy', '78787878');

-- --------------------------------------------------------

--
-- Table structure for table `clienteb`
--

CREATE TABLE `clienteb` (
  `idcliente` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `ci` varchar(100) NOT NULL,
  `idclientei` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clientedato`
--

CREATE TABLE `clientedato` (
  `idCliente` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `ci` varchar(50) NOT NULL,
  `fecha` datetime NOT NULL,
  `totalApagar` double NOT NULL,
  `efectivo` double NOT NULL,
  `cambio` double NOT NULL,
  `idClientei` varchar(150) NOT NULL,
  `tipoVenta` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clientetotal`
--

CREATE TABLE `clientetotal` (
  `idcliente` int(11) NOT NULL,
  `nombreCliente` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `ci` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `montoTotal` double NOT NULL,
  `fecha` date NOT NULL,
  `atendido` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `clientetotal`
--

INSERT INTO `clientetotal` (`idcliente`, `nombreCliente`, `ci`, `montoTotal`, `fecha`, `atendido`) VALUES
(1, 'Calni', '0', 23, '2016-01-05', 'Calani'),
(2, 'Avila', '0', 32, '2016-01-05', ''),
(3, 'Barreto', '0', 1, '2016-01-05', ''),
(4, 'Flores', '0', 88, '2016-01-05', ''),
(5, 'Meneses', '12933', 47, '2016-01-05', ''),
(6, 'Tarifa', '0', 51, '2016-01-05', ''),
(7, 'BArreda', '0', 15, '2016-01-05', ''),
(8, 'Bortolini', '0', 31, '2016-01-05', ''),
(9, 'Calani', '0', 6, '2016-01-05', ''),
(10, 'Suarez', '123456', 22, '2016-01-05', ''),
(11, 'Bolivar', '0', 22, '2016-01-05', 'Calani'),
(12, 'Camacho', '12345', 52, '2016-01-05', 'Calani'),
(13, 'Kamikase', '12342', 38, '2016-01-05', 'Calani'),
(14, 'Leti', '0', 33, '2016-01-05', ''),
(15, 'Barreto', '0', 17, '2016-01-05', 'Calani'),
(16, 'Flores', '0', 37, '2016-01-05', 'Calani'),
(17, 'Avila', '0', 12, '2016-01-05', 'Calani'),
(18, 'Castro', '0', 23, '2016-01-05', 'Calani'),
(19, 'Barreto', '0', 87, '2016-01-05', 'Calani'),
(20, 'Henry', '0', 0, '2016-01-07', 'Calani'),
(21, 'Calani', '0', 0, '2016-01-07', 'Calani'),
(22, 'calani', '0', 22, '2016-02-11', 'Calani'),
(23, 'calani', '0', 10, '2016-02-11', 'Calani'),
(24, 'Zabala', '0', 22, '2016-02-11', 'Ventas'),
(25, 'henry', '0', 22, '2016-02-22', 'Ventas'),
(26, 'henry', '0', 25, '2016-02-22', 'Ventas'),
(27, 'henry', '0', 32, '2016-02-22', 'Ventas'),
(28, 'herrera', '0', 22, '2016-02-22', 'Ventas'),
(29, 'miranda', '0', 19, '2016-02-22', 'Ventas'),
(30, 'henry', '0', 10, '2016-02-22', 'Ventas'),
(31, 'henry', '0', 32, '2016-02-22', 'Ventas'),
(32, 'henry', '0', 33, '2016-02-22', 'Ventas'),
(33, 'henry', '0', 6, '2016-02-22', 'Ventas'),
(34, 'HENRY', '0', 10, '2016-02-22', 'Ventas'),
(35, 'HERRERA', '0', 65, '2016-02-22', 'Ventas'),
(36, 'BARRETO', '293838', 22, '2016-02-22', 'Ventas'),
(37, 'SUAREZ', '0', 20, '2016-02-22', 'Ventas'),
(38, 'calani', '0', 22, '2016-02-22', 'Ventas'),
(39, 'henry', '0', 22, '2016-02-22', 'Ventas'),
(40, 'suarez', '0', 22, '2016-02-22', 'Ventas'),
(41, 'barreto', '0', 10, '2016-02-22', 'Ventas'),
(42, 'meneses', '0', 22, '2016-02-22', 'Ventas'),
(43, 'barreto', '1223', 22, '2016-02-22', 'Ventas'),
(44, 'leticia', '0', 34, '2016-02-22', 'Ventas'),
(45, 'calani', '0', 22, '2016-02-22', 'Ventas'),
(46, 'barreto', '0', 75, '2016-02-22', 'Ventas'),
(47, 'alanes', '1234', 20, '2016-02-22', 'Ventas'),
(48, 'avila', '0', 62, '2016-02-22', 'Ventas'),
(49, 'avila', '100', 62, '2016-02-22', 'Ventas'),
(50, 'herrera', '0', 60, '2016-02-22', 'Ventas'),
(51, 'herrera', '0', 60, '2016-02-22', 'Ventas'),
(52, 'henry', '0', 44, '2016-02-22', 'Ventas'),
(53, 'henry', '0', 22, '2016-02-22', 'Ventas'),
(54, 'henry', '0', 22, '2016-02-22', 'Ventas'),
(55, 'juan', '0', 22, '2016-02-22', 'Ventas'),
(56, 'susana', '0', 22, '2016-02-22', 'Ventas'),
(57, 'susana', '0', 22, '2016-02-22', 'Ventas'),
(58, 'juan', '0', 22, '2016-02-22', 'Ventas'),
(59, 'salceso', '0', 22, '2016-02-22', 'Ventas'),
(60, 'salcedo', '0', 22, '2016-02-22', 'Ventas'),
(61, 'mesenes', '0', 22, '2016-02-22', 'Ventas'),
(62, 'barreto', '0', 10, '2016-02-22', 'Ventas'),
(63, 'barreo', '0', 10, '2016-02-22', 'Ventas'),
(64, 'barreto', '0', 22, '2016-02-22', 'Ventas'),
(65, 'barreto', '0', 22, '2016-02-22', 'Ventas'),
(66, 'henry', '0', 22, '2016-02-22', 'Ventas'),
(67, 'suarez', '0', 32, '2016-02-22', 'Ventas'),
(68, 'suarez', '0', 32, '2016-02-22', 'Ventas'),
(69, 'jaro', '0', 11, '2016-02-22', 'Ventas'),
(70, 'calani', '0', 72, '2016-02-22', 'Ventas'),
(71, 'juanes', '0', 60, '2016-02-22', 'Ventas');

-- --------------------------------------------------------

--
-- Table structure for table `codigocontrol`
--

CREATE TABLE `codigocontrol` (
  `idcodigo` int(11) NOT NULL,
  `autorizacion` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `factura` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `llave` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `nit` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fechaL` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `codigocontrol`
--

INSERT INTO `codigocontrol` (`idcodigo`, `autorizacion`, `factura`, `llave`, `nit`, `fechaL`) VALUES
(1, '29040011007', '1503', '9rCB7Sv4X29d)5k7N%3ab89p-3(5[A', '1689353', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `codigotransaccion`
--

CREATE TABLE `codigotransaccion` (
  `idCodigo` int(11) NOT NULL,
  `codigoTransaccion` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `confirmarpedido`
--

CREATE TABLE `confirmarpedido` (
  `idConfirmar` int(11) NOT NULL,
  `imagen` varchar(150) NOT NULL,
  `producto` varchar(150) NOT NULL,
  `cantidad` varchar(150) NOT NULL,
  `precio` double NOT NULL,
  `idCliente` int(11) NOT NULL,
  `obervacionPedido` text NOT NULL,
  `fotoCliente` varchar(150) NOT NULL,
  `fechaRegistro` date NOT NULL,
  `porPagar` varchar(150) NOT NULL,
  `nombreCompleto` varchar(150) NOT NULL,
  `ci` varchar(150) NOT NULL,
  `pventa` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `confirmarpedido`
--

INSERT INTO `confirmarpedido` (`idConfirmar`, `imagen`, `producto`, `cantidad`, `precio`, `idCliente`, `obervacionPedido`, `fotoCliente`, `fechaRegistro`, `porPagar`, `nombreCompleto`, `ci`, `pventa`) VALUES
(1, 'fotoproducto/tarjetadevioProgamerB85.jpeg', 'TARJETA DE VIO PRO GAMER B85  ', '1 ', 135, 0, '', '', '0000-00-00', '', '', '', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `control`
--

CREATE TABLE `control` (
  `idcontrol` int(11) NOT NULL,
  `producto` varchar(50) NOT NULL,
  `clase` varchar(50) NOT NULL,
  `fechacontrol` datetime NOT NULL,
  `idjalea` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `control`
--

INSERT INTO `control` (`idcontrol`, `producto`, `clase`, `fechacontrol`, `idjalea`) VALUES
(900, 'Resource id #6', '', '0000-00-00 00:00:00', ''),
(943, 'Pollo Entero', 'POLLO', '2016-08-20 00:00:00', '59'),
(946, 'Popular Coca Cola', 'REFRESCO', '2016-08-20 00:00:00', '75'),
(951, 'Gordita', 'REFRESCO', '2016-08-22 00:00:00', '73'),
(952, 'Limonada', 'HERVIDO', '2016-08-22 00:00:00', '84'),
(956, 'Limonada', 'HERVIDO', '2016-08-22 00:00:00', '85'),
(973, 'Tostada', 'HERVIDO', '2016-08-22 00:00:00', '88'),
(974, 'Tostada', 'HERVIDO', '2016-08-22 00:00:00', '88'),
(980, 'Limonada', 'HERVIDO', '2016-08-22 00:00:00', '84'),
(987, 'Gordita', 'REFRESCO', '2016-08-21 00:00:00', '73'),
(988, 'Tostada', 'HERVIDO', '2016-08-21 00:00:00', '88'),
(989, 'Milanesa', 'POLLO', '2016-08-21 00:00:00', '63'),
(993, 'Fanta ', 'REFRESCO', '2016-08-21 00:00:00', '79'),
(1006, 'Limonada', 'HERVIDO', '2016-08-22 00:00:00', '84'),
(1014, 'Fanta 2lts', 'REFRESCO', '2016-08-22 00:00:00', '79'),
(1015, 'Tostada 1 1/2 lts', 'HERVIDO', '2016-08-22 00:00:00', '88'),
(1016, 'Milanesa', 'POLLO', '2016-08-22 00:00:00', '63'),
(1018, 'Limonada 1 1/2 ', 'HERVIDO', '2016-08-22 00:00:00', '85'),
(1025, 'Tostada', 'HERVIDO', '2016-08-22 00:00:00', '83'),
(1028, 'Popular Coca Cola', 'REFRESCO', '2016-08-22 00:00:00', '75'),
(1034, 'Presa', 'POLLO', '2016-08-22 00:00:00', '68'),
(1035, 'Presa', 'POLLO', '2016-08-22 00:00:00', '68'),
(1036, 'Presa', 'POLLO', '2016-08-22 00:00:00', '68'),
(1038, 'Tostada', 'HERVIDO', '2016-08-22 00:00:00', '83'),
(1046, 'Tostada 1 1/2 lts', 'HERVIDO', '2016-08-22 00:00:00', '88'),
(1051, 'Popular Fanta', 'REFRESCO', '2016-08-22 00:00:00', '89'),
(1057, 'Popular Fanta', 'REFRESCO', '2016-08-22 00:00:00', '89'),
(1063, 'Tostada 1 1/2 lts', 'HERVIDO', '2016-08-22 00:00:00', '88'),
(1071, 'Tostada 1lts', 'HERVIDO', '2016-08-22 00:00:00', '87'),
(1085, 'Coca Cola 2 1/2 lts', 'REFRESCO', '2016-08-22 00:00:00', '78'),
(1092, 'Popular Fanta', 'REFRESCO', '2016-08-22 00:00:00', '89'),
(1099, 'Fanta 2 1/2 lts ', 'REFRESCO', '2016-08-22 00:00:00', '80'),
(1106, 'Popular Fanta', 'REFRESCO', '2016-08-22 00:00:00', '89'),
(1123, 'Cuero', 'POLLO', '2016-08-23 00:00:00', '66'),
(1124, 'Milanesa', 'POLLO', '2016-08-23 00:00:00', '65'),
(1125, 'Porcion Papa', 'POLLO', '2016-08-23 00:00:00', '70'),
(1126, 'Presa', 'POLLO', '2016-08-23 00:00:00', '68'),
(1127, 'Hueso', 'POLLO', '2016-08-23 00:00:00', '67'),
(1131, 'Popular Coca Cola', 'REFRESCO', '2016-08-23 00:00:00', '75'),
(1132, 'Tropical', 'REFRESCO', '2016-08-23 00:00:00', '76'),
(1133, 'Coca Cola 2lts', 'REFRESCO', '2016-08-23 00:00:00', '77'),
(1134, 'Coca Cola 2 1/2 lts', 'REFRESCO', '2016-08-23 00:00:00', '78'),
(1135, 'Fanta 2lts', 'REFRESCO', '2016-08-23 00:00:00', '79'),
(1136, 'Fanta 2 1/2 lts ', 'REFRESCO', '2016-08-23 00:00:00', '80'),
(1138, 'Simba', 'REFRESCO', '2016-08-23 00:00:00', '82'),
(1139, 'Popular Fanta', 'REFRESCO', '2016-08-23 00:00:00', '89'),
(1140, 'Tostada', 'HERVIDO', '2016-08-23 00:00:00', '83'),
(1141, 'Limonada 1/2', 'HERVIDO', '2016-08-23 00:00:00', '84'),
(1142, 'Limonada 1 1/2 ', 'HERVIDO', '2016-08-23 00:00:00', '85'),
(1143, 'Limonada 1lts', 'HERVIDO', '2016-08-23 00:00:00', '86'),
(1144, 'Tostada 1lts', 'HERVIDO', '2016-08-23 00:00:00', '87'),
(1145, 'Tostada 1 1/2 lts', 'HERVIDO', '2016-08-23 00:00:00', '88'),
(1146, 'Tostada', 'HERVIDO', '2016-08-23 00:00:00', '83'),
(1147, 'Limonada 1/2', 'HERVIDO', '2016-08-23 00:00:00', '84'),
(1148, 'Limonada 1 1/2 ', 'HERVIDO', '2016-08-23 00:00:00', '85'),
(1149, 'Limonada 1lts', 'HERVIDO', '2016-08-23 00:00:00', '86'),
(1159, 'Pollo Entero', 'POLLO', '2016-08-23 00:00:00', '59'),
(1161, 'Churrasco', 'POLLO', '2016-08-23 00:00:00', '61'),
(1162, 'Pollo a la Parrilla', 'POLLO', '2016-08-23 00:00:00', '62'),
(1163, 'Milanesa', 'POLLO', '2016-08-23 00:00:00', '65'),
(1164, 'Cuero', 'POLLO', '2016-08-23 00:00:00', '66'),
(1243, 'Pollo Entero', 'POLLO', '2016-08-23 00:00:00', '59'),
(1245, 'Churrasco', 'POLLO', '2016-08-23 00:00:00', '61'),
(1246, 'Pollo a la Parrilla', 'POLLO', '2016-08-23 00:00:00', '62'),
(1247, 'Milanesa', 'POLLO', '2016-08-23 00:00:00', '63'),
(1250, 'Porcion Papa', 'POLLO', '2016-08-23 00:00:00', '70'),
(1251, 'Porcion Arroz', 'POLLO', '2016-08-23 00:00:00', '69'),
(1252, 'Presa', 'POLLO', '2016-08-23 00:00:00', '68'),
(1253, 'Hueso', 'POLLO', '2016-08-23 00:00:00', '67'),
(1254, 'Cuero', 'POLLO', '2016-08-23 00:00:00', '66'),
(1261, 'Milanesa', 'POLLO', '2016-08-23 00:00:00', '63'),
(1262, 'Milanesa', 'POLLO', '2016-08-23 00:00:00', '65'),
(1303, 'Nuggets', 'POLLO', '2016-08-25 00:00:00', '58'),
(1304, 'Nuggets', 'POLLO', '2016-08-25 00:00:00', '58'),
(1328, 'Nuggets', 'POLLO', '2016-08-25 00:00:00', '57'),
(1329, 'Nuggets', 'POLLO', '2016-08-25 00:00:00', '57'),
(1330, 'Nuggets', 'POLLO', '2016-08-25 00:00:00', '57'),
(1331, 'Nuggets', 'POLLO', '2016-08-25 00:00:00', '57'),
(1332, 'Pollo Entero', 'POLLO', '2016-08-25 00:00:00', '59'),
(1333, 'Pollo Entero', 'POLLO', '2016-08-25 00:00:00', '59'),
(1337, 'Nuggets', 'POLLO', '2016-08-25 00:00:00', '57'),
(1338, 'Nuggets', 'POLLO', '2016-08-25 00:00:00', '57'),
(1339, 'Nuggets', 'POLLO', '2016-08-25 00:00:00', '57'),
(1340, 'Nuggets', 'POLLO', '2016-08-25 00:00:00', '57'),
(1341, 'Nuggets', 'POLLO', '2016-08-25 00:00:00', '57'),
(1342, 'Nuggets', 'POLLO', '2016-08-25 00:00:00', '57'),
(1351, 'Un Cuarto', 'POLLO', '2016-08-25 00:00:00', '50'),
(1352, 'Un Cuarto', 'POLLO', '2016-08-25 00:00:00', '50'),
(1353, 'Un Cuarto', 'POLLO', '2016-08-25 00:00:00', '50'),
(1354, 'Carlitos', 'POLLO', '2016-08-25 00:00:00', '51'),
(1355, 'Carlitos', 'POLLO', '2016-08-25 00:00:00', '51'),
(1356, 'Carlita', 'POLLO', '2016-08-25 00:00:00', '52'),
(1357, 'Carlita', 'POLLO', '2016-08-25 00:00:00', '52'),
(1358, 'Economico', 'POLLO', '2016-08-25 00:00:00', '54'),
(1359, 'Economico', 'POLLO', '2016-08-25 00:00:00', '54'),
(1360, 'CabaÃ±ita', 'POLLO', '2016-08-25 00:00:00', '55'),
(1361, 'CabaÃ±ita', 'POLLO', '2016-08-25 00:00:00', '55'),
(1362, 'Carlitos', 'POLLO', '2016-08-25 00:00:00', '51'),
(1363, 'Carlitos', 'POLLO', '2016-08-25 00:00:00', '51'),
(1364, 'CabaÃ±ita', 'POLLO', '2016-08-25 00:00:00', '55'),
(1365, 'CabaÃ±ita', 'POLLO', '2016-08-25 00:00:00', '55'),
(1366, 'CabaÃ±ita ', 'POLLO', '2016-08-25 00:00:00', '56'),
(1367, 'CabaÃ±ita ', 'POLLO', '2016-08-25 00:00:00', '56'),
(1368, 'Carlita', 'POLLO', '2016-08-25 00:00:00', '52'),
(1369, 'Carlita', 'POLLO', '2016-08-25 00:00:00', '52'),
(1370, 'Tropical', 'REFRESCO', '2016-08-25 00:00:00', '76'),
(1371, 'Popular Coca Cola', 'REFRESCO', '2016-08-25 00:00:00', '75'),
(1372, 'Coca Cola Mini', 'REFRESCO', '2016-08-25 00:00:00', '91'),
(1373, 'Fanta 2lts', 'REFRESCO', '2016-08-25 00:00:00', '79'),
(1374, 'Tropical', 'REFRESCO', '2016-08-25 00:00:00', '76'),
(1375, 'Carlita', 'POLLO', '2016-08-26 00:32:15', '52'),
(1376, 'Medio Pollos', 'POLLO', '2016-08-26 11:20:17', '60'),
(1377, 'Medio Pollos', 'POLLO', '2016-08-26 11:20:19', '60'),
(1378, 'Pollo Entero', 'POLLO', '2016-08-26 11:49:47', '59'),
(1379, 'Un Cuarto', 'POLLO', '2016-08-26 14:19:10', '50'),
(1380, 'Carlitos', 'POLLO', '2016-08-26 14:20:18', '51'),
(1381, 'Carlitos', 'POLLO', '2016-08-26 14:20:22', '51'),
(1382, 'CabaÃ±ita', 'POLLO', '2016-08-26 16:34:08', '55'),
(1383, 'Economico', 'POLLO', '2016-08-26 16:38:48', '54'),
(1384, 'Carlita', 'POLLO', '2016-08-26 16:40:02', '52'),
(1385, 'Economico', 'POLLO', '2016-08-26 16:44:12', '54'),
(1386, 'Carlitos', 'POLLO', '2016-08-26 16:46:01', '51'),
(1387, 'Carlitos', 'POLLO', '2016-08-26 16:47:36', '51'),
(1388, 'Carlitos', 'POLLO', '2016-08-26 16:49:02', '51'),
(1389, 'Carlita', 'POLLO', '2016-08-26 16:56:07', '52'),
(1390, 'Carlita', 'POLLO', '2016-08-26 16:56:10', '52'),
(1391, 'Carlita', 'POLLO', '2016-08-26 16:57:11', '52'),
(1392, 'Carlita', 'POLLO', '2016-08-26 16:57:13', '52'),
(1393, 'Carlitos', 'POLLO', '2016-08-26 17:00:46', '51'),
(1394, 'Carlitos', 'POLLO', '2016-08-26 17:00:48', '51'),
(1395, 'Carlita', 'POLLO', '2016-08-26 17:01:16', '52'),
(1396, 'Carlita', 'POLLO', '2016-08-26 17:01:17', '52'),
(1397, 'Un Cuarto', 'POLLO', '2016-08-27 16:24:29', '50'),
(1398, 'Limonada 1/2', 'HERVIDO', '2016-08-27 16:29:40', '84'),
(1399, 'Limonada 1 1/2 ', 'HERVIDO', '2016-08-27 16:29:41', '85'),
(1400, 'Limonada 1lts', 'HERVIDO', '2016-08-27 16:29:42', '86'),
(1401, 'Tostada 1 1/2 lts', 'HERVIDO', '2016-08-27 16:29:44', '88'),
(1402, 'Tostada 1lts', 'HERVIDO', '2016-08-27 16:29:45', '87'),
(1403, 'Tostada', 'HERVIDO', '2016-08-27 16:29:48', '83'),
(1404, 'Tostada', 'HERVIDO', '2016-08-27 16:29:48', '83'),
(1405, 'Un Cuarto', 'POLLO', '2016-08-27 16:39:04', '50'),
(1406, 'Carlitos', 'POLLO', '2016-08-27 16:39:06', '51'),
(1407, 'Carlita', 'POLLO', '2016-08-27 16:39:08', '52'),
(1408, 'Economico', 'POLLO', '2016-08-27 16:46:22', '54'),
(1409, 'Economico', 'POLLO', '2016-08-27 16:46:23', '54'),
(1410, 'Economico', 'POLLO', '2016-08-27 16:48:41', '54'),
(1411, 'CabaÃ±ita', 'POLLO', '2016-08-27 16:56:32', '55'),
(1412, 'CabaÃ±ita', 'POLLO', '2016-08-27 16:56:35', '55'),
(1413, 'CabaÃ±ita', 'POLLO', '2016-08-27 17:01:42', '55'),
(1414, 'CabaÃ±ita', 'POLLO', '2016-08-27 17:01:42', '55'),
(1415, 'Economico', 'POLLO', '2016-08-27 17:19:44', '54'),
(1416, 'Economico', 'POLLO', '2016-08-27 17:19:45', '54'),
(1417, 'CabaÃ±ita', 'POLLO', '2016-08-27 17:21:55', '55'),
(1418, 'CabaÃ±ita', 'POLLO', '2016-08-27 17:21:56', '55'),
(1419, 'Carlita', 'POLLO', '2016-08-27 17:23:25', '52'),
(1420, 'Carlita', 'POLLO', '2016-08-27 17:23:26', '52'),
(1421, 'Carlita', 'POLLO', '2016-08-27 17:23:27', '52'),
(1422, 'Economico', 'POLLO', '2016-08-27 17:28:35', '54'),
(1423, 'Economico', 'POLLO', '2016-08-27 17:28:36', '54'),
(1424, 'Carlita', 'POLLO', '2016-08-27 17:29:18', '52'),
(1425, 'Carlita', 'POLLO', '2016-08-27 17:29:18', '52'),
(1426, 'Carlita', 'POLLO', '2016-08-27 17:29:18', '52'),
(1427, 'Carlita', 'POLLO', '2016-08-27 17:29:19', '52'),
(1428, 'Carlita', 'POLLO', '2016-08-27 17:29:20', '52'),
(1429, 'Carlita', 'POLLO', '2016-08-27 17:29:20', '52'),
(1430, 'Carlita', 'POLLO', '2016-08-27 17:29:20', '52'),
(1431, 'Carlita', 'POLLO', '2016-08-27 17:29:21', '52'),
(1432, 'Carlita', 'POLLO', '2016-08-27 17:29:21', '52'),
(1433, 'Carlita', 'POLLO', '2016-08-27 17:29:23', '52'),
(1434, 'Carlita', 'POLLO', '2016-08-27 17:29:23', '52'),
(1435, 'Carlita', 'POLLO', '2016-08-27 17:29:23', '52'),
(1436, 'Carlita', 'POLLO', '2016-08-27 17:29:24', '52'),
(1437, 'Carlita', 'POLLO', '2016-08-27 17:29:24', '52'),
(1438, 'Carlita', 'POLLO', '2016-08-27 17:32:49', '52'),
(1439, 'Carlita', 'POLLO', '2016-08-27 17:32:50', '52'),
(1440, 'Carlita', 'POLLO', '2016-08-27 17:32:51', '52'),
(1441, 'Carlita', 'POLLO', '2016-08-27 17:32:51', '52'),
(1442, 'Economico', 'POLLO', '2016-08-27 17:33:31', '54'),
(1443, 'Economico', 'POLLO', '2016-08-27 17:33:32', '54'),
(1444, 'Economico', 'POLLO', '2016-08-27 17:33:32', '54'),
(1445, 'Economico', 'POLLO', '2016-08-27 17:33:33', '54'),
(1446, 'Economico', 'POLLO', '2016-08-27 17:33:33', '54'),
(1447, 'CabaÃ±ita', 'POLLO', '2016-08-29 17:24:56', '55'),
(1448, 'Medio Pollos', 'POLLO', '2016-08-29 17:24:58', '60'),
(1449, 'Carlita', 'POLLO', '2016-08-29 17:31:56', '52'),
(1450, 'Carlita', 'POLLO', '2016-08-29 17:31:57', '52'),
(1451, 'Carlitos', 'POLLO', '2016-08-29 17:37:55', '51'),
(1452, 'Carlita', 'POLLO', '2016-08-29 17:53:30', '52'),
(1453, 'Pollo Entero', 'POLLO', '2016-08-29 17:53:32', '59'),
(1454, 'Milanesa', 'POLLO', '2016-08-29 17:53:33', '65'),
(1455, 'CabaÃ±ita', 'POLLO', '2016-08-29 17:54:35', '55'),
(1456, 'CabaÃ±ita', 'POLLO', '2016-08-29 17:54:37', '55'),
(1457, 'Nuggets', 'POLLO', '2016-08-29 17:59:25', '57'),
(1458, 'Un Cuarto', 'POLLO', '2016-08-29 17:59:26', '50'),
(1459, 'Popular Sprite', 'REFRESCO', '2016-08-29 17:59:29', '74'),
(1460, 'Carlitos', 'POLLO', '2016-08-29 17:59:49', '51'),
(1461, 'Medio Pollos', 'POLLO', '2016-08-29 17:59:51', '60'),
(1462, 'Economico', 'POLLO', '2016-08-29 18:01:16', '54'),
(1463, 'Carlita', 'POLLO', '2016-08-29 18:02:38', '52'),
(1464, 'Medio Pollos', 'POLLO', '2016-08-29 18:02:40', '60'),
(1465, 'Carlita', 'POLLO', '2016-08-29 18:02:51', '52'),
(1466, 'Carlita', 'POLLO', '2016-08-29 18:03:27', '52'),
(1467, 'Pollo a la Parrilla', 'POLLO', '2016-08-29 18:03:28', '62'),
(1468, 'Economico', 'POLLO', '2016-08-29 18:04:18', '54'),
(1469, 'Medio Pollos', 'POLLO', '2016-08-29 18:04:20', '60'),
(1470, 'Carlita', 'POLLO', '2016-08-29 18:07:14', '52'),
(1471, 'Carlitos', 'POLLO', '2016-08-29 18:07:58', '51'),
(1472, 'Carlitos', 'POLLO', '2016-08-29 04:03:23', '51');

-- --------------------------------------------------------

--
-- Table structure for table `controljalea`
--

CREATE TABLE `controljalea` (
  `idcontrol` int(11) NOT NULL,
  `producto` varchar(50) NOT NULL,
  `clase` varchar(50) NOT NULL,
  `fechacontrol` date NOT NULL,
  `cantidadInicial` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `controljalea`
--

INSERT INTO `controljalea` (`idcontrol`, `producto`, `clase`, `fechacontrol`, `cantidadInicial`) VALUES
(1, 'MARACUYA', '3', '2016-08-01', ''),
(2, 'MOCOCHINCHI', '4', '2016-08-01', ''),
(3, 'MOCOCHINCHI', '4', '2016-08-01', ''),
(4, 'PERA - MANZANA ', '5', '2016-08-01', ''),
(5, 'MOCOCHINCHI', '4', '2016-08-01', ''),
(6, 'MOCOCHINCHI', '4', '2016-08-01', ''),
(7, 'MOCOCHINCHI', '4', '2016-08-01', ''),
(8, 'MARACUYA', '3', '2016-08-01', ''),
(9, 'MOCOCHINCHI', '4', '2016-08-01', ''),
(10, 'ZANAHORIA - NARANJA ', '1', '2016-08-01', ''),
(11, 'MARACUYA', '3', '2016-08-01', ''),
(12, 'Yogurt ZERO 250 ml + JALEA DE MOCOCHINCHI', '4', '2016-08-01', ''),
(13, 'Yogurt Normal 250 ml + JALEA DE MARACUYA', '3', '2016-08-01', ''),
(14, 'MARACUYA', '3', '2016-08-01', ''),
(15, 'ZANAHORIA - NARANJA ', '1', '2016-08-01', ''),
(16, 'Yogurt Normal 250 ml + JALEA DE REMOLACHA - FRUTIL', '2', '2016-08-01', ''),
(17, 'MARACUYA', '3', '2016-08-01', ''),
(18, 'PERA - MANZANA ', '5', '2016-08-01', ''),
(19, 'MOCOCHINCHI', '4', '2016-08-01', ''),
(20, 'Yogurt Normal 500 ml + JALEA DE MOCOCHINCHI', '32', '2016-08-01', ''),
(21, 'FRUTILLA - DURAZNO', '8', '2016-08-01', ''),
(22, 'Yogurt Normal 500 ml + JALEA DE MARACUYA', '31', '2016-08-01', ''),
(23, 'REMOLACHA - FRUTILLA', '2', '2016-08-01', ''),
(24, 'Yogurt Normal 250 ml + JALEA DE MOCOCHINCHI', '30', '2016-08-01', ''),
(25, 'DURAZNO ', '7', '2016-08-02', ''),
(26, 'DURAZNO ', '7', '2016-08-02', ''),
(27, 'DURAZNO ', '7', '2016-08-02', ''),
(28, 'DURAZNO ', '7', '2016-08-02', ''),
(29, 'DURAZNO ', '7', '2016-08-02', ''),
(30, 'DURAZNO ', '7', '2016-08-02', ''),
(31, 'DURAZNO ', '7', '2016-08-02', ''),
(32, 'DURAZNO ', '7', '2016-08-02', ''),
(33, 'DURAZNO ', '7', '2016-08-02', ''),
(34, 'DURAZNO ', '7', '2016-08-02', ''),
(35, 'DURAZNO ', '7', '2016-08-02', ''),
(36, 'DURAZNO ', '7', '2016-08-02', ''),
(37, 'DURAZNO ', '7', '2016-08-02', ''),
(38, 'DURAZNO ', '7', '2016-08-02', ''),
(39, 'DURAZNO ', '7', '2016-08-02', ''),
(40, 'DURAZNO ', '7', '2016-08-02', ''),
(41, 'DURAZNO ', '7', '2016-08-02', ''),
(42, 'DURAZNO ', '7', '2016-08-02', ''),
(43, 'DURAZNO ', '7', '2016-08-02', ''),
(44, 'DURAZNO ', '7', '2016-08-02', ''),
(45, 'DURAZNO ', '7', '2016-08-02', ''),
(46, 'DURAZNO ', '7', '2016-08-02', ''),
(47, 'DURAZNO ', '7', '2016-08-02', ''),
(48, 'MARACUYA', '3', '2016-08-02', ''),
(49, 'MARACUYA', '3', '2016-08-02', ''),
(50, 'MARACUYA', '3', '2016-08-02', ''),
(51, 'MARACUYA', '3', '2016-08-02', ''),
(52, 'MARACUYA', '3', '2016-08-02', ''),
(53, 'MARACUYA', '3', '2016-08-02', ''),
(54, 'MARACUYA', '3', '2016-08-02', ''),
(55, 'MARACUYA', '3', '2016-08-02', ''),
(56, 'MARACUYA', '3', '2016-08-02', ''),
(57, 'DURAZNO ', '7', '2016-08-02', ''),
(58, 'PERA - MANZANA ', '5', '2016-08-02', ''),
(59, 'Yogurt Normal 500 ml + JALEA DE MOCOCHINCHI', '32', '2016-08-02', ''),
(60, 'Yogurt Normal 500 ml + JALEA ZANAHORIA - NARANJA <', '28', '2016-08-03', ''),
(61, 'Yogurt de 40 REMOLACHA - FRUTILLA<br>***********<b', '32', '2016-08-03', ''),
(62, 'Yogurt Normal 250 ml + JALEA FRUTILLA <br>********', '26', '2016-08-03', ''),
(63, 'Yogurt  + JALEA REMOLACHA - FRUTILLA<br>**********', '32', '2016-08-03', ''),
(64, 'FRUTILLA - DURAZNO', '8', '2016-08-03', ''),
(65, 'Yogurt Normal 250 ml + JALEA REMOLACHA - FRUTILLA<', '26', '2016-08-03', ''),
(66, 'FRUTILLA - DURAZNO', '8', '2016-08-03', ''),
(67, 'Yogurt Normal 250 ml + JALEA REMOLACHA - FRUTILLA<', '26', '2016-08-03', ''),
(68, 'Yogurt  + JALEA REMOLACHA - FRUTILLA<br>**********', '32', '2016-08-03', ''),
(69, 'Yogurt ZERO 250 ml + JALEA REMOLACHA - FRUTILLA<br', '27', '2016-08-03', ''),
(70, 'Yogurt ZERO 500 ml + JALEA REMOLACHA - FRUTILLA<br', '29', '2016-08-03', ''),
(71, 'FRUTILLA - DURAZNO', '8', '2016-08-03', ''),
(72, 'Yogurt Normal 500 ml + JALEA MOCOCHINCHI<br>******', '28', '2016-08-03', ''),
(73, 'Yogurt ZERO 500 ml + JALEA PERA - MANZANA <br>****', '29', '2016-08-03', ''),
(74, 'Yogurt Normal 500 ml + JALEA ZANAHORIA - NARANJA <', '28', '2016-08-04', ''),
(75, 'ZANAHORIA - NARANJA ', '1', '2016-08-04', ''),
(76, 'FRUTILLA - DURAZNO', '8', '2016-08-04', ''),
(77, 'Yogurt ZERO 250 ml + JALEA MOCOCHINCHI<br>********', '27', '2016-08-04', ''),
(78, 'Yogurt ZERO 250 ml + JALEA MOCOCHINCHI<br>********', '27', '2016-08-04', ''),
(79, 'Yogurt Normal 500 ml + JALEA MOCOCHINCHI<br>******', '28', '2016-08-04', ''),
(80, 'Yogurt Normal 250 ml + JALEA MOCOCHINCHI<br>******', '26', '2016-08-04', ''),
(81, 'Yogurt ZERO 250 ml + JALEA <br> - MARACUYA/DURAZNO', '27', '2016-08-04', ''),
(82, 'Yogurt ZERO 250 ml + JALEA <Br>- MARACUYA<Br> - DU', '27', '2016-08-04', ''),
(83, 'Yogurt ZERO 250 ml + JALEA<br>  4 | zanahoria / na', '27', '2016-08-05', ''),
(84, 'Yogurt Normal 250 ml + JALEA<br>  3 | maracuya<br>', '26', '2016-08-05', ''),
(85, 'Yogurt ZERO 250 ml + JALEA<br>  2 | frutilla / dur', '27', '2016-08-05', ''),
(86, 'Yogurt Normal 500 ml + JALEA<br>  4 | frutilla ', '28', '2016-08-05', ''),
(87, 'Yogurt Normal 500 ml + JALEA<br>  1 | frutilla / d', '28', '2016-08-05', ''),
(88, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-05', ''),
(89, 'Yogurt Normal 250 ml + JALEA<br>  1 | zanahoria / ', '26', '2016-08-05', ''),
(90, 'Yogurt  + JALEA<br>  1 | maracuya', '32', '2016-08-05', ''),
(91, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-05', ''),
(92, 'Yogurt Normal 250 ml + JALEA<br>  1 | durazno ', '26', '2016-08-05', ''),
(93, 'Yogurt Normal 250 ml + JALEA<br>  1 | durazno ', '26', '2016-08-05', ''),
(94, 'Yogurt Normal 500 ml + JALEA<br>  1 | maracuya', '28', '2016-08-05', ''),
(95, 'Yogurt Normal 500 ml + JALEA<br>  1 | maracuya', '28', '2016-08-05', ''),
(96, 'Yogurt Normal 500 ml + JALEA<br>  3 | remolacha / ', '28', '2016-08-05', ''),
(97, 'Yogurt Normal 500 ml + JALEA<br>  1 | zanahoria / ', '28', '2016-08-05', ''),
(98, 'Yogurt Normal 500 ml + JALEA<br>  2 | remolacha / ', '28', '2016-08-05', ''),
(99, 'Yogurt Normal 500 ml + JALEA<br>  1 | zanahoria / ', '28', '2016-08-05', ''),
(100, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-05', ''),
(101, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-05', ''),
(102, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-05', ''),
(103, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-05', ''),
(104, 'Yogurt ZERO 250 ml + JALEA<br>  1 | maracuya', '27', '2016-08-05', ''),
(105, 'Yogurt Normal 500 ml + JALEA<br>  1 | maracuya', '28', '2016-08-05', ''),
(106, 'Yogurt Normal 500 ml + JALEA<br>  1 | frutilla ', '28', '2016-08-05', ''),
(107, 'Yogurt ZERO 250 ml + JALEA<br>  1 | remolacha / fr', '27', '2016-08-05', ''),
(108, 'Yogurt ZERO 250 ml + JALEA<br>  1 | remolacha / fr', '27', '2016-08-05', ''),
(109, 'Yogurt ZERO 250 ml + JALEA<br>  1 | remolacha / fr', '27', '2016-08-05', ''),
(110, 'Yogurt Normal 250 ml + JALEA<br>  1 | remolacha / ', '26', '2016-08-05', ''),
(111, 'Yogurt Normal 500 ml + JALEA<br>  1 | maracuya', '28', '2016-08-05', ''),
(112, 'Yogurt Normal 500 ml + JALEA<br>  1 | maracuya<br>', '28', '2016-08-05', ''),
(113, 'PERA / MANZANA ', '5', '2016-08-05', ''),
(114, 'PERA / MANZANA ', '5', '2016-08-05', ''),
(115, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(116, 'Yogurt Normal 500 ml + JALEA<br>  1 | maracuya', '28', '2016-08-06', ''),
(117, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(118, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(119, 'MARACUYA', '3', '2016-08-06', ''),
(120, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(121, 'MARACUYA', '3', '2016-08-06', ''),
(122, 'Yogurt Normal 500 ml + JALEA<br>  1 | maracuya', '28', '2016-08-06', ''),
(123, 'Yogurt Normal 500 ml + JALEA<br>  1 | durazno ', '28', '2016-08-06', ''),
(124, 'Yogurt Normal 500 ml + JALEA<br>  1 | zanahoria / ', '28', '2016-08-06', ''),
(125, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(126, 'MARACUYA', '3', '2016-08-06', ''),
(127, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(128, 'Yogurt Normal 500 ml + JALEA<br>  1 | maracuya<br>', '28', '2016-08-06', ''),
(129, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(130, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(131, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(132, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(133, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(134, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(135, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(136, 'REMOLACHA / FRUTILLA', '2', '2016-08-06', ''),
(137, 'MARACUYA', '3', '2016-08-06', ''),
(138, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(139, 'MARACUYA', '3', '2016-08-06', ''),
(140, 'REMOLACHA / FRUTILLA', '2', '2016-08-06', ''),
(141, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(142, 'MARACUYA', '3', '2016-08-06', ''),
(143, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(144, 'MARACUYA', '3', '2016-08-06', ''),
(145, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(146, 'FRUTILLA / DURAZNO', '8', '2016-08-06', ''),
(147, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(148, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(149, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(150, 'REMOLACHA / FRUTILLA', '2', '2016-08-06', ''),
(151, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(152, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(153, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(154, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(155, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(156, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(157, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(158, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(159, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(160, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(161, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(162, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(163, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(164, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(165, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(166, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(167, 'MARACUYA', '3', '2016-08-06', ''),
(168, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(169, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(170, 'MARACUYA', '3', '2016-08-06', ''),
(171, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(172, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(173, 'MARACUYA', '3', '2016-08-06', ''),
(174, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(175, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(176, 'MARACUYA', '3', '2016-08-06', ''),
(177, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(178, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(179, 'MARACUYA', '3', '2016-08-06', ''),
(180, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(181, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(182, 'MARACUYA', '3', '2016-08-06', ''),
(183, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(184, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(185, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(186, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(187, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(188, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(189, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(190, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(191, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(192, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(193, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(194, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(195, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(196, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(197, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(198, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(199, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(200, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(201, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(202, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(203, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(204, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(205, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(206, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(207, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(208, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(209, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(210, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(211, 'MARACUYA', '3', '2016-08-06', ''),
(212, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(213, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(214, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(215, 'MARACUYA', '3', '2016-08-06', ''),
(216, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(217, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(218, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(219, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(220, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(221, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(222, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(223, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(224, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(225, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(226, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(227, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(228, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(229, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(230, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(231, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(232, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(233, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(234, 'REMOLACHA / FRUTILLA', '2', '2016-08-06', ''),
(235, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(236, 'REMOLACHA / FRUTILLA', '2', '2016-08-06', ''),
(237, 'MARACUYA', '3', '2016-08-06', ''),
(238, 'DURAZNO ', '7', '2016-08-06', ''),
(239, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(240, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(241, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(242, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(243, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(244, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(245, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(246, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(247, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(248, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(249, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(250, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(251, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(252, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(253, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(254, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(255, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(256, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(257, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(258, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(259, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(260, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(261, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(262, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(263, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(264, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(265, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(266, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(267, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(268, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(269, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(270, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(271, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(272, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(273, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(274, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(275, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(276, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(277, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(278, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(279, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(280, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(281, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(282, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(283, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(284, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(285, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(286, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(287, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(288, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(289, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(290, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(291, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(292, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(293, 'MARACUYA', '3', '2016-08-06', ''),
(294, 'DURAZNO ', '7', '2016-08-06', ''),
(295, 'ZANAHORIA / NARANJA ', '1', '2016-08-06', ''),
(296, 'REMOLACHA / FRUTILLA', '2', '2016-08-06', ''),
(297, 'MARACUYA', '3', '2016-08-06', ''),
(298, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(299, 'DURAZNO ', '7', '2016-08-06', ''),
(300, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(301, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(302, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(303, 'FRUTILLA ', '6', '2016-08-06', ''),
(304, 'DURAZNO ', '7', '2016-08-06', ''),
(305, 'DURAZNO ', '7', '2016-08-06', ''),
(306, 'DURAZNO ', '7', '2016-08-06', ''),
(307, 'DURAZNO ', '7', '2016-08-06', ''),
(308, 'MARACUYA', '3', '2016-08-06', ''),
(309, 'FRUTILLA ', '6', '2016-08-06', ''),
(310, 'ZANAHORIA / NARANJA ', '1', '2016-08-06', ''),
(311, 'ZANAHORIA / NARANJA ', '1', '2016-08-06', ''),
(312, 'REMOLACHA / FRUTILLA', '2', '2016-08-06', ''),
(313, 'MARACUYA', '3', '2016-08-06', ''),
(314, 'MOCOCHINCHI', '4', '2016-08-06', ''),
(315, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(316, 'FRUTILLA ', '6', '2016-08-06', ''),
(317, 'FRUTILLA / DURAZNO', '8', '2016-08-06', ''),
(318, 'DURAZNO ', '7', '2016-08-06', ''),
(319, 'Yogurt Normal 250 ml + JALEA<br>  1 | remolacha / ', '26', '2016-08-06', ''),
(320, 'Yogurt Normal 250 ml + JALEA<br>  1 | remolacha / ', '26', '2016-08-06', ''),
(321, 'Yogurt Normal 250 ml + JALEA<br>  1 | remolacha / ', '26', '2016-08-06', ''),
(322, 'Yogurt Normal 250 ml + JALEA<br>  1 | remolacha / ', '26', '2016-08-06', ''),
(323, 'Yogurt Normal 250 ml + JALEA<br>  1 | remolacha / ', '26', '2016-08-06', ''),
(324, 'Yogurt Normal 250 ml + JALEA<br>  1 | remolacha / ', '26', '2016-08-06', ''),
(325, 'Yogurt Normal 250 ml + JALEA<br>  1 | remolacha / ', '26', '2016-08-06', ''),
(326, 'Yogurt Normal 250 ml + JALEA<br>  1 | remolacha / ', '26', '2016-08-06', ''),
(327, 'Yogurt Normal 250 ml + JALEA<br>  1 | remolacha / ', '26', '2016-08-06', ''),
(328, 'Yogurt Normal 500 ml + JALEA<br>  1 | maracuya', '28', '2016-08-06', ''),
(329, 'Yogurt Normal 250 ml + JALEA<br>  1 | remolacha / ', '26', '2016-08-06', ''),
(330, 'MARACUYA', '3', '2016-08-06', ''),
(331, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(332, 'DURAZNO ', '7', '2016-08-06', ''),
(333, 'ZANAHORIA / NARANJA ', '1', '2016-08-06', ''),
(334, 'Yogurt ZERO 250 ml + JALEA<br>  1 | maracuya', '27', '2016-08-06', ''),
(335, 'Yogurt Normal 500 ml + JALEA<br>  1 | maracuya', '28', '2016-08-06', ''),
(336, 'Yogurt  + JALEA<br>  1 | remolacha / frutilla<br> ', '32', '2016-08-06', ''),
(337, 'Yogurt  + JALEA<br>  1 | remolacha / frutilla<br> ', '32', '2016-08-06', ''),
(338, 'Yogurt  + JALEA<br>  1 | remolacha / frutilla<br> ', '32', '2016-08-06', ''),
(339, 'Yogurt  + JALEA<br>  1 | remolacha / frutilla<br> ', '32', '2016-08-06', ''),
(340, 'Yogurt  + JALEA<br>  1 | remolacha / frutilla<br> ', '32', '2016-08-06', ''),
(341, 'Yogurt  + JALEA<br>  1 | remolacha / frutilla<br> ', '32', '2016-08-06', ''),
(342, 'Yogurt  + JALEA<br>  1 | remolacha / frutilla<br> ', '32', '2016-08-06', ''),
(343, 'Yogurt Normal 500 ml + JALEA<br>  1 | maracuya', '28', '2016-08-06', ''),
(344, 'Yogurt Normal 500 ml + JALEA<br>  1 | maracuya<br>', '28', '2016-08-06', ''),
(345, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(346, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(347, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(348, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(349, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(350, 'Yogurt ZERO 250 ml + JALEA<br>  1 | remolacha / fr', '27', '2016-08-06', ''),
(351, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(352, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(353, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(354, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(355, 'Yogurt Normal 500 ml + JALEA<br>  1 | frutilla <br', '28', '2016-08-06', ''),
(356, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(357, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(358, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(359, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(360, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(361, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(362, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(363, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(364, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(365, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(366, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(367, 'Yogurt ZERO 250 ml + JALEA<br>  1 | remolacha / fr', '27', '2016-08-06', ''),
(368, 'Yogurt ZERO 250 ml + JALEA<br>  1 | remolacha / fr', '27', '2016-08-06', ''),
(369, 'Yogurt Normal 500 ml + JALEA<br>  1 | zanahoria / ', '28', '2016-08-06', ''),
(370, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(371, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(372, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(373, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(374, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(375, 'PERA / MANZANA ', '5', '2016-08-06', ''),
(376, 'Yogurt Normal 500 ml + JALEA<br>  1 | remolacha / ', '28', '2016-08-06', ''),
(377, 'Yogurt Normal 500 ml + JALEA<br>  1 | zanahoria / ', '28', '2016-08-06', ''),
(378, 'MARACUYA', '3', '2016-08-07', ''),
(379, 'MOCOCHINCHI', '4', '2016-08-07', ''),
(380, 'PERA / MANZANA ', '5', '2016-08-07', ''),
(381, 'REMOLACHA / FRUTILLA', '2', '2016-08-07', ''),
(382, 'ZANAHORIA / NARANJA ', '1', '2016-08-07', ''),
(383, 'FRUTILLA / DURAZNO', '8', '2016-08-07', ''),
(384, 'DURAZNO ', '7', '2016-08-07', ''),
(385, 'FRUTILLA ', '6', '2016-08-07', ''),
(386, 'MARACUYA', '3', '2016-08-07', ''),
(387, 'MOCOCHINCHI', '4', '2016-08-07', ''),
(388, 'PERA / MANZANA ', '5', '2016-08-07', ''),
(389, 'REMOLACHA / FRUTILLA', '2', '2016-08-07', ''),
(390, 'ZANAHORIA / NARANJA ', '1', '2016-08-07', ''),
(391, 'FRUTILLA / DURAZNO', '8', '2016-08-07', ''),
(392, 'DURAZNO ', '7', '2016-08-07', ''),
(393, 'FRUTILLA ', '6', '2016-08-07', ''),
(394, 'MARACUYA', '3', '2016-08-07', ''),
(395, 'MOCOCHINCHI', '4', '2016-08-07', ''),
(396, 'PERA / MANZANA ', '5', '2016-08-07', ''),
(397, 'REMOLACHA / FRUTILLA', '2', '2016-08-07', ''),
(398, 'ZANAHORIA / NARANJA ', '1', '2016-08-07', ''),
(399, 'FRUTILLA / DURAZNO', '8', '2016-08-07', ''),
(400, 'DURAZNO ', '7', '2016-08-07', ''),
(401, 'FRUTILLA ', '6', '2016-08-07', ''),
(402, 'ZANAHORIA / NARANJA ', '1', '2016-08-07', ''),
(403, 'REMOLACHA / FRUTILLA', '2', '2016-08-07', ''),
(404, 'MARACUYA', '3', '2016-08-07', ''),
(405, 'MOCOCHINCHI', '4', '2016-08-07', ''),
(406, 'PERA / MANZANA ', '5', '2016-08-07', ''),
(407, 'FRUTILLA / DURAZNO', '8', '2016-08-07', ''),
(408, 'DURAZNO ', '7', '2016-08-07', ''),
(409, 'FRUTILLA ', '6', '2016-08-07', ''),
(410, 'ZANAHORIA / NARANJA ', '1', '2016-08-07', ''),
(411, 'REMOLACHA / FRUTILLA', '2', '2016-08-07', ''),
(412, 'MARACUYA', '3', '2016-08-07', ''),
(413, 'MOCOCHINCHI', '4', '2016-08-07', ''),
(414, 'PERA / MANZANA ', '5', '2016-08-07', ''),
(415, 'FRUTILLA / DURAZNO', '8', '2016-08-07', ''),
(416, 'DURAZNO ', '7', '2016-08-07', ''),
(417, 'FRUTILLA ', '6', '2016-08-07', ''),
(418, 'ZANAHORIA / NARANJA ', '1', '2016-08-07', ''),
(419, 'REMOLACHA / FRUTILLA', '2', '2016-08-07', ''),
(420, 'MARACUYA', '3', '2016-08-07', ''),
(421, 'MOCOCHINCHI', '4', '2016-08-07', ''),
(422, 'PERA / MANZANA ', '5', '2016-08-07', ''),
(423, 'FRUTILLA / DURAZNO', '8', '2016-08-07', ''),
(424, 'DURAZNO ', '7', '2016-08-07', ''),
(425, 'FRUTILLA ', '6', '2016-08-07', ''),
(426, 'ZANAHORIA / NARANJA ', '1', '2016-08-07', ''),
(427, 'REMOLACHA / FRUTILLA', '2', '2016-08-07', ''),
(428, 'MARACUYA', '3', '2016-08-07', ''),
(429, 'MOCOCHINCHI', '4', '2016-08-07', ''),
(430, 'PERA / MANZANA ', '5', '2016-08-07', ''),
(431, 'FRUTILLA / DURAZNO', '8', '2016-08-07', ''),
(432, 'DURAZNO ', '7', '2016-08-07', ''),
(433, 'FRUTILLA ', '6', '2016-08-07', ''),
(434, 'ZANAHORIA / NARANJA ', '1', '2016-08-07', ''),
(435, 'REMOLACHA / FRUTILLA', '2', '2016-08-07', ''),
(436, 'MARACUYA', '3', '2016-08-07', ''),
(437, 'MOCOCHINCHI', '4', '2016-08-07', ''),
(438, 'PERA / MANZANA ', '5', '2016-08-07', ''),
(439, 'FRUTILLA / DURAZNO', '8', '2016-08-07', ''),
(440, 'DURAZNO ', '7', '2016-08-07', ''),
(441, 'FRUTILLA ', '6', '2016-08-07', ''),
(442, 'Yogurt Normal 500 ml + JALEA<br>  1 | zanahoria / ', '1 | 1,2 | 1,3 | 1,4 | 1,5 | 1,6 | 1,7 | 1,8 | 1', '2016-08-07', ''),
(443, 'Yogurt Normal 500 ml + JALEA<br>  1 | zanahoria / ', '1 | 1,2 | 1,3 | 1,4 | 1,5 | 1,6 | 1,7 | 1,8 | 1', '2016-08-08', ''),
(444, 'Yogurt Normal 500 ml + JALEA<br>  1 | zanahoria / ', '1 | 1,2 | 1,3 | 1,4 | 1,5 | 1,6 | 1,7 | 1,8 | 1', '2016-08-08', ''),
(445, 'Yogurt Normal 500 ml + JALEA<br>  1 | durazno <br>', '7 | 1,6 | 1,5 | 1,4 | 1,3 | 1,2 | 1,1 | 1,8 | 1', '2016-08-08', ''),
(446, 'Yogurt Normal 500 ml + JALEA<br>  1 | zanahoria / ', '1 | 1', '2016-08-08', ''),
(447, 'Yogurt Normal 500 ml + JALEA<br>  2 | remolacha / ', '2 | 2', '2016-08-08', ''),
(448, 'Yogurt Normal 500 ml + JALEA<br>  3 | maracuya', '3 | 3', '2016-08-08', ''),
(449, 'Yogurt Normal 500 ml + JALEA<br>  3 | mocochinchi', '4 | 3', '2016-08-08', ''),
(450, 'Yogurt Normal 500 ml + JALEA<br>  3 | mocochinchi', '4 | 3', '2016-08-08', ''),
(451, 'Yogurt Normal 500 ml + JALEA<br>  3 | mocochinchi', '4 | 3', '2016-08-08', ''),
(452, 'Yogurt Normal 500 ml + JALEA<br>  3 | mocochinchi', '4 | 3', '2016-08-08', ''),
(453, 'Yogurt Normal 500 ml + JALEA<br>  1 | pera / manza', '5 | 1', '2016-08-08', ''),
(454, 'Yogurt Normal 500 ml + JALEA<br>  1 | pera / manza', '5 | 1', '2016-08-08', ''),
(455, 'Yogurt Normal 500 ml + JALEA<br>  1 | pera / manza', '5 | 1', '2016-08-08', ''),
(456, 'Yogurt Normal 500 ml + JALEA<br>  1 | frutilla ', '6 | 1', '2016-08-08', ''),
(457, 'Yogurt Normal 500 ml + JALEA<br>  1 | durazno ', '7 | 1', '2016-08-08', ''),
(458, 'Yogurt Normal 500 ml + JALEA<br>  1 | frutilla / d', '8 | 1', '2016-08-08', ''),
(459, 'Yogurt Normal 500 ml + JALEA<br>  1 | durazno <br>', '7 | 1,6 | 1,5 | 1,4 | 1,3 | 1,2 | 1,1 | 1,8 | 1', '2016-08-08', ''),
(460, 'Yogurt Normal 500 ml + JALEA<br>  1 | frutilla / d', '8 | 1', '2016-08-08', ''),
(461, 'Yogurt Normal 500 ml + JALEA<br> 1 1 | zanahoria /', '1 | 1,2 | 1,3 | 1,4 | 1,5 | 1,6 | 1,7 | 1,8 | 1', '2016-08-08', ''),
(462, 'Yogurt Normal 500 ml + JALEA<br> 1 1 | zanahoria /', '1 | 1,2 | 1,3 | 1,4 | 1,5 | 1,6 | 1,7 | 1,8 | 1', '2016-08-08', ''),
(463, 'Yogurt ZERO 250 ml + JALEA<br>  1 | durazno <br> 1', '7 | 1,6 | 1,5 | 1,4 | 1,3 | 1,2 | 1,1 | 1,8 | 1', '2016-08-08', ''),
(464, 'Yogurt Normal 500 ml + JALEA<br>  1 | zanahoria / ', '1 | 1,2 | 1,3 | 1,4 | 1,5 | 1,6 | 1,7 | 1,8 | 1', '2016-08-08', ''),
(465, 'Yogurt Normal 500 ml + JALEA<br>  1 | zanahoria / ', '1 | 1,2 | 1,3 | 1,4 | 1,5 | 1,6 | 1,7 | 1,8 | 1', '2016-08-08', ''),
(466, 'Yogurt Normal 500 ml + JALEA<br>  1 | zanahoria / ', '1 | 1,2 | 1,3 | 1,4 | 1,5 | 1,6 | 1,7 | 1,8 | 1', '2016-08-08', ''),
(467, 'PERA / MANZANA ', '5', '2016-08-08', ''),
(468, 'PERA / MANZANA ', '5', '2016-08-08', ''),
(469, 'PERA / MANZANA ', '5', '2016-08-08', ''),
(470, 'PERA / MANZANA ', '5', '2016-08-08', ''),
(471, 'PERA / MANZANA ', '5', '2016-08-08', ''),
(472, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(473, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(474, 'DURAZNO ', '7', '2016-08-08', ''),
(475, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(476, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(477, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(478, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(479, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(480, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(481, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(482, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(483, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(484, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(485, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(486, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(487, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(488, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(489, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(490, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(491, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(492, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(493, 'ZANAHORIA / NARANJA ', '1', '2016-08-08', ''),
(494, 'REMOLACHA / FRUTILLA', '2', '2016-08-08', ''),
(495, 'ZANAHORIA / NARANJA ', '1', '2016-08-08', ''),
(496, 'REMOLACHA / FRUTILLA', '2', '2016-08-08', ''),
(497, 'ZANAHORIA / NARANJA ', '1', '2016-08-08', ''),
(498, 'REMOLACHA / FRUTILLA', '2', '2016-08-08', ''),
(499, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(500, 'PERA / MANZANA ', '5', '2016-08-08', ''),
(501, 'MARACUYA', '3', '2016-08-08', ''),
(502, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(503, 'ZANAHORIA / NARANJA ', '1', '2016-08-08', ''),
(504, 'REMOLACHA / FRUTILLA', '2', '2016-08-08', ''),
(505, 'MARACUYA', '3', '2016-08-08', ''),
(506, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(507, 'PERA / MANZANA ', '5', '2016-08-08', ''),
(508, 'FRUTILLA ', '6', '2016-08-08', ''),
(509, 'DURAZNO ', '7', '2016-08-08', ''),
(510, 'FRUTILLA / DURAZNO', '8', '2016-08-08', ''),
(511, 'PERA / MANZANA ', '5', '2016-08-08', ''),
(512, 'PERA / MANZANA ', '5', '2016-08-08', ''),
(513, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(514, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(515, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(516, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(517, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(518, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(519, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(520, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(521, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(522, 'MOCOCHINCHI', '4', '2016-08-08', ''),
(523, 'Yogurt Normal 500 ml GZ<br>  1 | frutilla / durazn', '24 | 1,25 | 1', '2016-08-08', ''),
(524, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | frutilla / dura', '24 | 1,25 | 2', '2016-08-09', ''),
(525, 'MOCOCHINCHI', '28', '2016-08-12', ''),
(526, 'MOCOCHINCHI', '28', '2016-08-12', ''),
(527, 'Yogurt ZERO 250 ml+ Jalea<br>  1 | frutilla / dura', '24 | 1', '2016-08-12', ''),
(528, 'MOCOCHINCHI', '28|2', '2016-08-12', ''),
(529, 'PERA / MANZANA ', '27|1', '2016-08-12', ''),
(530, 'FRUTILLA ', '26|1', '2016-08-12', ''),
(531, 'PERA / MANZANA ', '27|2', '2016-08-12', ''),
(532, 'MOCOCHINCHI', '28|3', '2016-08-12', ''),
(533, 'Yogurt ZERO 250 ml+ Jalea<br>  2 | frutilla / dura', '24 | 2', '2016-08-12', ''),
(534, 'MOCOCHINCHI', '28|3', '2016-08-12', ''),
(535, 'PERA / MANZANA ', '27|4', '2016-08-12', ''),
(536, 'MOCOCHINCHI', '28|3', '2016-08-12', ''),
(537, 'MOCOCHINCHI', '28|3', '2016-08-12', ''),
(538, 'MOCOCHINCHI', '28|5', '2016-08-12', ''),
(539, 'MOCOCHINCHI', '28|3', '2016-08-12', ''),
(540, 'MOCOCHINCHI', '28|3', '2016-08-12', ''),
(541, 'DURAZNO ', '25|2', '2016-08-12', ''),
(542, 'FRUTILLA ', '26|2', '2016-08-12', ''),
(543, 'DURAZNO ', '25|5', '2016-08-12', ''),
(544, 'DURAZNO ', '25|2', '2016-08-13', ''),
(545, 'DURAZNO ', '25|2|1', '2016-08-13', ''),
(546, 'MOCOCHINCHI', '28|3', '2016-08-13', ''),
(547, 'MOCOCHINCHI', '28|3|1', '2016-08-13', ''),
(548, 'MOCOCHINCHI', '28|5', '2016-08-13', ''),
(549, 'MOCOCHINCHI', '28|5|1', '2016-08-13', ''),
(550, 'FRUTILLA ', '26|3', '2016-08-13', ''),
(551, 'FRUTILLA ', '26|3', '2016-08-13', ''),
(552, 'MARACUYA', '29|5', '2016-08-13', ''),
(553, 'Yogurt ZERO 250 ml+ Jalea<br>  1 | durazno <br> 1 ', '25 | 1,26 | 1', '2016-08-13', ''),
(554, 'Yogurt Normal 500 ml+ Jalea<br>  1 | frutilla / du', '24 | 1', '2016-08-13', ''),
(555, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |DURAZNO ', '5|25', '2016-08-13', ''),
(556, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |DURAZNO ', '5|25', '2016-08-13', ''),
(557, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | frutilla / dura', '24 | 1', '2016-08-13', ''),
(558, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |DURAZNO ', '25|5', '2016-08-13', ''),
(559, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |DURAZNO ', '25|5', '2016-08-13', ''),
(560, 'mocochinchi', '28|3', '2016-08-13', ''),
(561, 'mocochinchi', '28|1', '2016-08-13', ''),
(562, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(563, 'mocochinchi', '28|1|1', '2016-08-13', ''),
(564, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(565, 'mocochinchi', '28|1|1|1', '2016-08-13', ''),
(566, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(567, 'mocochinchi', '28|1|1|1|1', '2016-08-13', ''),
(568, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(569, 'mocochinchi', '28|1|1|1|1|1', '2016-08-13', ''),
(570, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(571, 'mocochinchi', '28|1|1|1|1|1|1', '2016-08-13', ''),
(572, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(573, 'mocochinchi', '28|1|1|1|1|1|1|1', '2016-08-13', ''),
(574, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(575, 'mocochinchi', '28|1|1|1|1|1|1|1|1', '2016-08-13', ''),
(576, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(577, 'mocochinchi', '28|1|1|1|1|1|1|1|1|1', '2016-08-13', ''),
(578, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(579, 'mocochinchi', '28|1|1|1|1|1|1|1|1|1|1', '2016-08-13', ''),
(580, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(581, 'mocochinchi', '28|1|1|1|1|1|1|1|1|1|1|1', '2016-08-13', ''),
(582, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(583, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(584, 'mocochinchi', '28|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-13', ''),
(585, 'mocochinchi', '28|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-13', ''),
(586, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(587, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(588, 'mocochinchi', '28|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-13', ''),
(589, 'mocochinchi', '28|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-13', ''),
(590, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(591, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(592, 'mocochinchi', '28|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-13', ''),
(593, 'mocochinchi', '28|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-13', ''),
(594, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(595, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |frutilla / duraz', '24|5', '2016-08-13', ''),
(596, 'mocochinchi', '28|2', '2016-08-13', ''),
(597, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(598, 'mocochinchi', '28|1', '2016-08-13', ''),
(599, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-13', ''),
(600, 'mocochinchi', '28|1|1', '2016-08-13', ''),
(601, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | frutilla / dura', '24 | 1,25 | 2', '2016-08-13', ''),
(602, 'mocochinchi', '28|2', '2016-08-13', ''),
(603, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |frutilla / duraz', '24|5', '2016-08-13', ''),
(604, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | frutilla / dura', '24 | 1,25 | 2', '2016-08-13', ''),
(605, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |frutilla / duraz', '24|5', '2016-08-13', ''),
(606, 'mocochinchi', '28|2|1', '2016-08-13', ''),
(607, 'Yogurt Normal 500 ml+ Jalea<br>  1 | frutilla / du', '24 | 1,25 | 2', '2016-08-13', ''),
(608, 'mocochinchi', '28|2', '2016-08-13', ''),
(609, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |frutilla ', '26|5', '2016-08-13', ''),
(610, 'mocochinchi', '28|3', '2016-08-13', ''),
(611, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |maracuya', '29|5', '2016-08-13', ''),
(612, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | pera / manzana ', '27 | 1,29 | 1', '2016-08-13', ''),
(613, 'mocochinchi', '28|3|1', '2016-08-13', ''),
(614, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |maracuya', '29|5', '2016-08-13', ''),
(615, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | pera / manzana ', '27 | 1,29 | 1', '2016-08-13', ''),
(616, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |frutilla / duraz', '24|5', '2016-08-13', ''),
(617, 'mocochinchi', '28|3', '2016-08-13', ''),
(618, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | frutilla / dura', '24 | 1,25 | 2', '2016-08-13', ''),
(619, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | frutilla / dura', '24 | 1,25 | 2', '2016-08-13', ''),
(620, 'Yogurt  + Jalea<br>  1 | frutilla ,<br> 1 | pera /', '26 | 1,27 | 1,28 | 1,29 | 1', '2016-08-13', ''),
(621, 'Yogurt  + Jalea<br>  1 | frutilla / durazno,<br> 1', '24 | 1,27 | 1,30 | 1,31 | 1', '2016-08-13', ''),
(622, 'Yogurt  + Jalea<br>  1 | frutilla / durazno,<br> 1', '24 | 1,27 | 1,30 | 1,31 | 1', '2016-08-13', ''),
(623, 'Yogurt  + Jalea<br>  1 | frutilla / durazno,<br> 1', '24 | 1,27 | 1,30 | 1,31 | 1', '2016-08-13', ''),
(624, 'Yogurt  + Jalea<br>  1 | frutilla / durazno,<br> 1', '24 | 1,27 | 1,30 | 1,31 | 1', '2016-08-13', ''),
(625, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '25 | 1,26 | 1,27 | 1,30 | 1', '2016-08-13', ''),
(626, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '25 | 1,26 | 1,27 | 1,30 | 1', '2016-08-13', ''),
(627, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1,28 | 1,29 | 1', '2016-08-13', ''),
(628, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1,26 | 1,30 | 1', '2016-08-13', ''),
(629, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1,26 | 1', '2016-08-13', ''),
(630, 'Yogurt  + Jalea<br>  1 | pera / manzana <br> 1 | r', '27 | 1,30 | 1,31 | 1', '2016-08-13', ''),
(631, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,28 | 1,29 | 1', '2016-08-13', ''),
(632, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1,26 | 1', '2016-08-13', ''),
(633, 'Yogurt  + Jalea<br>  1 | mocochinchi<br> 1 | marac', '28 | 1,29 | 1,30 | 1,31 | 1', '2016-08-13', ''),
(634, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1,26 | 1', '2016-08-13', ''),
(635, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1,26 | 1', '2016-08-13', ''),
(636, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1,29 | 1', '2016-08-13', ''),
(637, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1,26 | 1,27 | 1', '2016-08-13', ''),
(638, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1,26 | 1,30 | 1', '2016-08-13', ''),
(639, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '25 | 1,26 | 1,27 | 1', '2016-08-13', ''),
(640, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1,26 | 1,27 | 1', '2016-08-13', ''),
(641, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1,26 | 1', '2016-08-13', ''),
(642, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | frutilla / dura', '24 | 1,25 | 1', '2016-08-13', ''),
(643, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | frutilla / dura', '24 | 1', '2016-08-13', ''),
(644, 'mocochinchi', '28|4', '2016-08-13', ''),
(645, 'mocochinchi', '28|1', '2016-08-13', ''),
(646, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1,26 | 1', '2016-08-13', ''),
(647, 'mocochinchi', '28|1', '2016-08-13', ''),
(648, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1', '2016-08-13', ''),
(649, 'mocochinchi', '28|1', '2016-08-13', ''),
(650, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1', '2016-08-13', ''),
(651, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1,26 | 1,27 | 1', '2016-08-14', ''),
(652, 'zanahoria / naranja ', '31|1', '2016-08-14', ''),
(653, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | mocochinchi<br>', '28 | 1,29 | 1', '2016-08-14', ''),
(654, 'Yogurt Normal 500 ml+ Jalea<br>  1 | frutilla <br>', '26 | 1,30 | 1', '2016-08-14', ''),
(655, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |zanahoria / nara', '31|5', '2016-08-14', ''),
(656, 'Yogurt Normal 250 ml + Jalea<br>  1 |maracuya', '29|6', '2016-08-14', ''),
(657, 'remolacha / frutilla', '30|1', '2016-08-14', ''),
(658, 'maracuya', '29|1', '2016-08-14', ''),
(659, 'mocochinchi', '28|1', '2016-08-14', ''),
(660, 'pera / manzana ', '27|1', '2016-08-14', ''),
(661, 'frutilla ', '26|1', '2016-08-14', ''),
(662, 'durazno ', '25|1', '2016-08-14', ''),
(663, 'frutilla / durazno', '24|1', '2016-08-14', ''),
(664, 'mocochinchi', '28|5', '2016-08-14', ''),
(665, 'pera / manzana ', '27|3', '2016-08-14', ''),
(666, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(667, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(668, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(669, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(670, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(671, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(672, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(673, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(674, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(675, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(676, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(677, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(678, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(679, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(680, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(681, 'Yogurt ZERO 250 ml+ Jalea<br>  1 |durazno ', '25|5', '2016-08-15', ''),
(682, 'Yogurt Normal 500 ml+ Jalea<br>  1 | frutilla / du', '24 | 1,25 | 1', '2016-08-15', ''),
(683, 'Yogurt Normal 500 ml+ Jalea<br>  1 | frutilla / du', '24 | 1,25 | 1', '2016-08-15', ''),
(684, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | frutilla / dura', '24 | 1,25 | 1', '2016-08-15', ''),
(685, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(686, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(687, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(688, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(689, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(690, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(691, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(692, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(693, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(694, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(695, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(696, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(697, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(698, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(699, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(700, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(701, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(702, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(703, 'Yogurt Normal 250 ml + Jalea<br>  1 |durazno ', '25|6', '2016-08-15', ''),
(704, 'mocochinchi', '28|3', '2016-08-16', ''),
(705, 'pera / manzana ', '27|1', '2016-08-16', ''),
(706, 'mocochinchi', '28|3', '2016-08-16', ''),
(707, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '24 | 1', '2016-08-16', ''),
(708, 'mocochinchi', '28|4', '2016-08-16', ''),
(709, 'mocochinchi', '28|4|1', '2016-08-16', ''),
(710, 'mocochinchi', '28|4', '2016-08-16', ''),
(711, 'mocochinchi', '28|4|1', '2016-08-16', ''),
(712, 'mocochinchi', '28|4', '2016-08-16', ''),
(713, 'mocochinchi', '28|4', '2016-08-16', ''),
(714, 'mocochinchi', '28|4|1', '2016-08-16', ''),
(715, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1', '2016-08-16', ''),
(716, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '24 | 1', '2016-08-16', ''),
(717, 'mocochinchi', '28|3', '2016-08-16', ''),
(718, 'mocochinchi', '28|7', '2016-08-16', ''),
(719, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '24 | 1', '2016-08-16', ''),
(720, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '24 | 1', '2016-08-16', ''),
(721, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '24 | 1', '2016-08-16', ''),
(722, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '24 | 1', '2016-08-16', ''),
(723, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '24 | 1', '2016-08-16', ''),
(724, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '|3', '2016-08-16', ''),
(725, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '|3|1', '2016-08-16', ''),
(726, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '|4', '2016-08-16', ''),
(727, 'Yogurt  + Jalea<br>  1 | durazno ', '|4', '2016-08-16', ''),
(728, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '|3', '2016-08-16', ''),
(729, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '|3|1', '2016-08-16', ''),
(730, 'Yogurt  + Jalea<br>  1 | remolacha / frutilla', '|4', '2016-08-16', ''),
(731, 'Yogurt  + Jalea<br>  2 | mocochinchi', '|5', '2016-08-16', ''),
(732, 'mocochinchi', '28|2', '2016-08-16', ''),
(733, 'mocochinchi', '28|4', '2016-08-17', ''),
(734, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '|2', '2016-08-17', ''),
(735, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '|2|1', '2016-08-17', ''),
(736, 'mocochinchi', '28|4|1', '2016-08-17', ''),
(737, 'mocochinchi', '28|4|1|1', '2016-08-17', ''),
(738, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '|2|1|1', '2016-08-17', ''),
(739, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '|2|1|1|1', '2016-08-17', ''),
(740, 'mocochinchi', '28|4|1|1|1', '2016-08-17', ''),
(741, 'mocochinchi', '28|4|1|1|1|1', '2016-08-17', ''),
(742, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '|2|1|1|1|1', '2016-08-17', ''),
(743, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '|2|1|1|1|1|1', '2016-08-17', ''),
(744, 'mocochinchi', '28|4|1|1|1|1|1', '2016-08-17', ''),
(745, 'mocochinchi', '28|3', '2016-08-17', ''),
(746, 'mocochinchi', '28|3|1', '2016-08-17', ''),
(747, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4', '2016-08-17', ''),
(748, 'mocochinchi', '28|3', '2016-08-17', ''),
(749, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1', '2016-08-17', ''),
(750, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1', '2016-08-17', ''),
(751, 'mocochinchi', '28|3|1', '2016-08-17', ''),
(752, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1', '2016-08-17', ''),
(753, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1', '2016-08-17', ''),
(754, 'mocochinchi', '28|3|1|1', '2016-08-17', ''),
(755, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1', '2016-08-17', ''),
(756, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1', '2016-08-17', ''),
(757, 'mocochinchi', '28|3|1|1|1', '2016-08-17', ''),
(758, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1', '2016-08-17', ''),
(759, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1|1', '2016-08-17', ''),
(760, 'mocochinchi', '28|3|1|1|1|1', '2016-08-17', ''),
(761, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1|1', '2016-08-17', ''),
(762, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1|1|1', '2016-08-17', ''),
(763, 'mocochinchi', '28|3|1|1|1|1|1', '2016-08-17', ''),
(764, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1|1|1', '2016-08-17', ''),
(765, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1|1|1|1', '2016-08-17', ''),
(766, 'mocochinchi', '28|3|1|1|1|1|1|1', '2016-08-17', ''),
(767, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1|1|1|1', '2016-08-17', ''),
(768, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(769, 'mocochinchi', '28|3|1|1|1|1|1|1|1', '2016-08-17', ''),
(770, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1|1|1|1|1', '2016-08-17', ''),
(771, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(772, 'mocochinchi', '28|3|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(773, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(774, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(775, 'mocochinchi', '28|3|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(776, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(777, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(778, 'mocochinchi', '28|3|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(779, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(780, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(781, 'mocochinchi', '28|3|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(782, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(783, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(784, 'mocochinchi', '28|3|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(785, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(786, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(787, 'mocochinchi', '28|3|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(788, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(789, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(790, 'mocochinchi', '28|3|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(791, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(792, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(793, 'mocochinchi', '28|3|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(794, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(795, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(796, 'mocochinchi', '28|3|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(797, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(798, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(799, 'mocochinchi', '28|3|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(800, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(801, 'Yogurt  + Jalea<br>  1 | durazno <br> 1 | frutilla', '|4|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(802, 'mocochinchi', '28|3|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', ''),
(803, 'Yogurt ZERO 500 ml+ Jalea<br>  1 | durazno ', '25 | 1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1', '2016-08-17', '');
INSERT INTO `controljalea` (`idcontrol`, `producto`, `clase`, `fechacontrol`, `cantidadInicial`) VALUES
(804, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '|3', '2016-08-17', ''),
(805, 'mocochinchi', '28|2', '2016-08-17', ''),
(806, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '|5', '2016-08-17', ''),
(807, 'mocochinchi', '28|2', '2016-08-17', ''),
(808, 'mocochinchi', '28|2|1', '2016-08-17', ''),
(809, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '|5|1', '2016-08-17', ''),
(810, 'mocochinchi', '28|4', '2016-08-17', ''),
(811, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1|1', '2016-08-17', ''),
(812, 'mocochinchi', '|10', '2016-08-17', ''),
(813, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '|30', '2016-08-17', ''),
(814, 'mocochinchi', '|10', '2016-08-17', ''),
(815, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '|10', '2016-08-17', ''),
(816, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '24 | 1|1', '2016-08-17', ''),
(817, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1|1', '2016-08-17', ''),
(818, 'mocochinchi', '28|1', '2016-08-17', ''),
(819, 'Yogurt  + Jalea<br>  1 | frutilla / durazno', '24 | 1|1', '2016-08-17', ''),
(820, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1|1', '2016-08-17', ''),
(821, 'Yogurt  + Jalea<br>  2 | remolacha / frutilla', '30 | 2|1', '2016-08-17', ''),
(822, 'Yogurt  + Jalea<br>  2 | frutilla / durazno', '24 | 2|1', '2016-08-17', ''),
(823, 'Yogurt ZERO 500 ml+ Jalea<br>  2 | frutilla / dura', '24 | 2|1', '2016-08-17', ''),
(824, 'Yogurt  + Jalea<br>  2 | zanahoria / naranja ', '31 | 2|1', '2016-08-17', ''),
(825, 'Yogurt  + Jalea<br>  2 | zanahoria / naranja ', '|5', '2016-08-17', ''),
(826, 'Yogurt  + Jalea<br>  2 | frutilla / durazno', '|5', '2016-08-17', ''),
(827, 'Yogurt  + Jalea<br>  1 | remolacha / frutilla', '|5', '2016-08-17', ''),
(828, 'Yogurt  + Jalea<br>  1 | frutilla / durazno<br> 1 ', '24 | 1,25 | 1,26 | 1,27 | 1|1', '2016-08-17', ''),
(829, 'Yogurt  + Jalea<br>  4 | frutilla / durazno', '24 | 4|1', '2016-08-17', ''),
(830, 'Yogurt  + Jalea<br>  4 | frutilla / durazno', '|1', '2016-08-17', ''),
(831, 'Yogurt  +JALEA<br>3|mocochinchi', '1|1', '2016-08-19', ''),
(832, 'Yogurt  +JALEA<br>2|durazno <br> 2|frutilla / dura', '1|1', '2016-08-19', ''),
(833, 'Yogurt  +JALEA<br>2|zanahoria / naranja ', '1|1', '2016-08-19', ''),
(834, 'Yogurt  +JALEA<br>2|frutilla / durazno<br> 2|remol', '3|1', '2016-08-19', ''),
(835, 'Yogurt  +JALEA<br>1|durazno <br> 1|frutilla / dura', '4|1', '2016-08-19', '');

-- --------------------------------------------------------

--
-- Table structure for table `cuentaporpagar`
--

CREATE TABLE `cuentaporpagar` (
  `idPorPagar` int(11) NOT NULL,
  `nombreCliente` varchar(150) NOT NULL,
  `ci` int(11) NOT NULL,
  `total` double NOT NULL,
  `xpagar` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuentaporpagar`
--

INSERT INTO `cuentaporpagar` (`idPorPagar`, `nombreCliente`, `ci`, `total`, `xpagar`) VALUES
(1, 'CALANI HENRY', 6444685, 992, 39);

-- --------------------------------------------------------

--
-- Table structure for table `datos`
--

CREATE TABLE `datos` (
  `iddatos` int(11) NOT NULL,
  `propietario` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `razon` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `nro` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `datos`
--

INSERT INTO `datos` (`iddatos`, `propietario`, `razon`, `direccion`, `nro`, `telefono`) VALUES
(1, 'Carlos Herrera', 'Pollos Carlitos', 'Av. Circunvalacion Melchor Perez', '1517', '4477129');

-- --------------------------------------------------------

--
-- Table structure for table `datosfactura`
--

CREATE TABLE `datosfactura` (
  `datosFacturaId` int(11) NOT NULL,
  `nit` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `cambio` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `efectivo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `totalApagar` double NOT NULL,
  `usuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `datosusuariosesion`
--

CREATE TABLE `datosusuariosesion` (
  `datosUsuarioId` int(11) NOT NULL,
  `usuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `datosusuariosesion`
--

INSERT INTO `datosusuariosesion` (`datosUsuarioId`, `usuario`, `password`) VALUES
(1, 'henry', 'henry');

-- --------------------------------------------------------

--
-- Table structure for table `estadocobro`
--

CREATE TABLE `estadocobro` (
  `idestado` int(11) NOT NULL,
  `proveedor` varchar(150) NOT NULL,
  `total` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `estadocobro`
--

INSERT INTO `estadocobro` (`idestado`, `proveedor`, `total`) VALUES
(1, 'Hypertrading ', 12),
(2, 'Hypertrading ', 60),
(3, 'Hypertrading ', 0),
(4, '', 0),
(5, '', 0),
(6, 'Hypertrading ', 3),
(7, 'Hypertrading ', 2),
(8, 'Hypertrading ', 3),
(9, 'Hypertrading ', 8),
(10, 'Hypertrading ', 30),
(11, 'Hypertrading ', 11),
(12, 'Hypertrading ', 14),
(13, 'Hypertrading ', 1113),
(14, 'Hypertrading ', 24),
(15, 'Hypertrading ', 21),
(16, '1', 0),
(17, '4', 0),
(18, '4', 0),
(19, '6', 0),
(20, '4', 0),
(21, '4', 1),
(22, '3', 6),
(23, '4', 1),
(24, '1', 4),
(25, '1', 4),
(26, '1', 4),
(27, '1', 1),
(28, '6', 6),
(29, '6', 6),
(30, '6', 6),
(31, '6', 6),
(32, '6', 6),
(33, '6', 6),
(34, '6', 6),
(35, '6', 6),
(36, '6', 6),
(37, '6', 6),
(38, '6', 6),
(39, '6', 6),
(40, '6', 6),
(41, '6', 6),
(42, '6', 6),
(43, '6', 6),
(44, '6', 6),
(45, '6', 6),
(46, '6', 6),
(47, '6', 6),
(48, '6', 6),
(49, '6', 6),
(50, '6', 6),
(51, '6', 6),
(52, '6', 6),
(53, '6', 6),
(54, '6', 6),
(55, '6', 6),
(56, '6', 6),
(57, '6', 6),
(58, '6', 6),
(59, '6', 6),
(60, '6', 6),
(61, '6', 6),
(62, '6', 6),
(63, '6', 6),
(64, '6', 6),
(65, '6', 6),
(66, '6', 6),
(67, '6', 6),
(68, '6', 6),
(69, '6', 6),
(70, '6', 6);

-- --------------------------------------------------------

--
-- Table structure for table `fechareportes`
--

CREATE TABLE `fechareportes` (
  `idfecha` int(11) NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFinal` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fechareportes`
--

INSERT INTO `fechareportes` (`idfecha`, `fechaInicio`, `fechaFinal`) VALUES
(1, '2016-06-10', '2016-06-16');

-- --------------------------------------------------------

--
-- Table structure for table `gastos`
--

CREATE TABLE `gastos` (
  `idgastos` int(11) NOT NULL,
  `descripcion` varchar(150) NOT NULL,
  `entrada` double NOT NULL,
  `fechaRegistro` date NOT NULL,
  `usuario` varchar(150) NOT NULL,
  `salida` varchar(150) NOT NULL,
  `tipo` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gastos`
--

INSERT INTO `gastos` (`idgastos`, `descripcion`, `entrada`, `fechaRegistro`, `usuario`, `salida`, `tipo`) VALUES
(57, 'Tinte de Cabello', 12, '2020-06-01', 'henry', '0', 'E'),
(56, 'Ventas de Gel', 0, '2020-06-14', 'henry', '100', 'S'),
(58, 'Llantas de auto', 0, '2020-06-14', 'henry', '120', 'S');

-- --------------------------------------------------------

--
-- Table structure for table `jalea`
--

CREATE TABLE `jalea` (
  `idjalea` int(11) NOT NULL,
  `jalea` varchar(150) NOT NULL,
  `producto` varchar(150) NOT NULL,
  `cantidad` varchar(150) NOT NULL,
  `idjallea` varchar(150) NOT NULL,
  `imagen` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jalea2`
--

CREATE TABLE `jalea2` (
  `idjalea` int(11) NOT NULL,
  `producto` varchar(150) NOT NULL,
  `idjaleaw` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `librov`
--

CREATE TABLE `librov` (
  `idlibro` int(11) NOT NULL,
  `fecha` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `nit` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `factura` int(11) NOT NULL,
  `autorizacion` int(11) NOT NULL,
  `codigoC` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `total` double NOT NULL,
  `ice` double NOT NULL,
  `impoexe` double NOT NULL,
  `neto` double NOT NULL,
  `credifoFiscal` double NOT NULL,
  `fechaI` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `librov`
--

INSERT INTO `librov` (`idlibro`, `fecha`, `nit`, `nombre`, `factura`, `autorizacion`, `codigoC`, `total`, `ice`, `impoexe`, `neto`, `credifoFiscal`, `fechaI`) VALUES
(1002, '09/02/2015', '6444468507', 'SIN NOMBRE', 1001, 2147483647, '76-43-70-01', 65, 0, 0, 65, 8.45, '2015-02-10');

-- --------------------------------------------------------

--
-- Table structure for table `mensajealerta`
--

CREATE TABLE `mensajealerta` (
  `mensajeAlertaId` int(11) NOT NULL,
  `mensaje` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `mensajealerta`
--

INSERT INTO `mensajealerta` (`mensajeAlertaId`, `mensaje`) VALUES
(1, ' ');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `idmenu` int(11) NOT NULL,
  `opcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `location` varchar(150) DEFAULT NULL,
  `color` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`idmenu`, `opcion`, `estado`, `icon`, `location`, `color`) VALUES
(1, 'principal', 'Activo', 'icon_house_alt', 'AccessUsers.php', '#0061c2'),
(2, 'registros', 'NoActivo', 'icon_pencil-edit', 'Usuario.php', '#4e4e4e'),
(3, 'proveedores', 'NoActivo', 'icon_briefcase', 'Proveedor.php', '#0061c2'),
(4, 'clientes', 'NoActivo', 'icon_group', 'Cliente.php', '#0061c2'),
(5, 'productos', 'NoActivo', 'icon_bag_alt', 'Producto.php', '#0061c2'),
(6, 'Inventario', 'NoActivo', 'icon_refresh', 'Inventario.php', '#0061c2'),
(7, 'Ventas', 'NoActivo', 'icon_cart', 'Ventas.php', '#0061c2'),
(8, 'Cuentas', 'NoActivo', 'arrow_down_alt', 'Cuenta.php', '#0061c2'),
(9, 'Pedidos', 'NoActivo', 'icon_zoom-in_alt', 'Pedido.php', '#0061c2'),
(10, 'Consolidar', 'NoActivo', 'icon_documents_alt', 'consolidar.php', '#0061c2'),
(11, 'reporte', 'NoActivo', 'icon_piechart', 'ReportesVentas.php', '#0061c2'),
(12, 'Reportes Graficos', 'NoActivo', 'icon_datareport', 'Estadistica.php', '#0061c2');

-- --------------------------------------------------------

--
-- Table structure for table `nota`
--

CREATE TABLE `nota` (
  `idnota` int(11) NOT NULL,
  `nota` text NOT NULL,
  `fecha` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nota`
--

INSERT INTO `nota` (`idnota`, `nota`, `fecha`) VALUES
(5, '', '2016-08-17'),
(4, '2  a hipermaxi', '2016-08-15');

-- --------------------------------------------------------

--
-- Table structure for table `pedido`
--

CREATE TABLE `pedido` (
  `idPedido` int(11) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `total` double NOT NULL,
  `proveedor` varchar(500) NOT NULL,
  `usuario` varchar(150) NOT NULL,
  `fechaRegistro` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pedido`
--

INSERT INTO `pedido` (`idPedido`, `descripcion`, `total`, `proveedor`, `usuario`, `fechaRegistro`) VALUES
(12, 'Tela de mascarilla', 34, 'Hypertrading ', 'henry', '2020-06-13'),
(14, '23 computadoras', 10, 'Bolivia Sport', 'henry', '2020-06-15');

-- --------------------------------------------------------

--
-- Table structure for table `prductofactory`
--

CREATE TABLE `prductofactory` (
  `idproducto` int(11) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `item` varchar(150) NOT NULL,
  `hipermaxi` varchar(150) NOT NULL,
  `plazanorte` varchar(150) NOT NULL,
  `fechaRegistro` date NOT NULL,
  `usuario` varchar(150) NOT NULL,
  `plazaoeste` varchar(150) NOT NULL,
  `proveedor` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prductofactory`
--

INSERT INTO `prductofactory` (`idproducto`, `imagen`, `item`, `hipermaxi`, `plazanorte`, `fechaRegistro`, `usuario`, `plazaoeste`, `proveedor`) VALUES
(1, 'fotoproducto/yogurt.jpg', 'Yogurt Normal 250ml', '100', '100', '2016-08-09', 'Paola', '50', 'Hypertrading '),
(2, 'fotoproducto/yogurt.jpg', 'Yogurt Zero 250ml', '10', '10', '2016-08-09', 'Paola', '10', 'Hypertrading ');

-- --------------------------------------------------------

--
-- Table structure for table `prductoiogo`
--

CREATE TABLE `prductoiogo` (
  `idproducto` int(11) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `item` varchar(150) NOT NULL,
  `saldo` double NOT NULL,
  `ingreso` double NOT NULL,
  `totalParcial` double NOT NULL,
  `ventaDia` double NOT NULL,
  `ventaConsumo` double NOT NULL,
  `totalFinal` double NOT NULL,
  `totalEfectivoA` double NOT NULL,
  `ingresoB` double NOT NULL,
  `totalParcialB` double NOT NULL,
  `ventaDiaB` double NOT NULL,
  `ventaConsumoB` double NOT NULL,
  `saldoFinalB` double NOT NULL,
  `totalEfectivoB` double NOT NULL,
  `fechaRegistro` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prductoiogo`
--

INSERT INTO `prductoiogo` (`idproducto`, `imagen`, `item`, `saldo`, `ingreso`, `totalParcial`, `ventaDia`, `ventaConsumo`, `totalFinal`, `totalEfectivoA`, `ingresoB`, `totalParcialB`, `ventaDiaB`, `ventaConsumoB`, `saldoFinalB`, `totalEfectivoB`, `fechaRegistro`) VALUES
(2, 'fotoproducto/zanahoria.jpg', 'ZANAHORIA - NARANJA ', -33, 40, 84, 3, 1200, -1163, 9, 3, 0, 0, 0, 0, 0, '2016-07-27'),
(3, 'fotoproducto/remolacha.jpg', 'REMOLACHA - FRUTILLA', -26, 0, 19, 3, 1200, -1203, 9, 3, 9, 10, 11, 12, 13, '2016-07-27'),
(4, 'fotoproducto/marcuya.jpg', 'MARACUYA', 62, 0, 3, 3, 3, -6, 9, 3, 9, 10, 11, 12, 13, '2016-07-27'),
(5, 'fotoproducto/mocon.jpg', 'MOCOCHINCHI', 50, 0, 3, 27, 3, -30, 81, 3, 9, 10, 11, 12, 13, '2016-07-27'),
(6, 'fotoproducto/pera.jpg', 'PERA - MANZANA ', -43, 100, 145, 27, 3, 70, 81, 3, 0, 0, 0, 0, 0, '2016-07-27'),
(7, 'fotoproducto/frutilla.jpg', 'FRUTILLA ', 100, 0, 3, 0, 0, 0, 0, 8, 9, 14, 11, 12, 13, '2016-07-27'),
(8, 'fotoproducto/durazno.jpg', 'DURAZNO ', 62, 0, 3, 3, 0, -3, 24, 8, 9, 10, 11, 12, 13, '2016-07-27'),
(9, 'fotoproducto/mixto.jpg', 'FRUTILLA - DURAZNO', 100, 0, 3, 0, 0, 0, 0, 8, 9, 10, 11, 12, 13, '2016-07-27'),
(10, 'fotoproducto/yogurt.jpg', 'Yogurt Normal 250ml', -14, 90, 94, 90, 0, 0, 720, 8, 9, 10, 11, 12, 13, '2016-07-27'),
(11, 'fotoproducto/yogurt.jpg', 'Yogurt Zero 250ml', 100, 0, 3, 0, 0, 0, 0, 8, 9, 9, 11, 12, 13, '2016-07-27'),
(12, 'fotoproducto/yogurt.jpg', 'Yogurt Normal 500ml', 62, 0, 3, 18, 0, -18, 144, 8, 9, 10, 11, 12, 13, '2016-07-27'),
(13, 'fotoproducto/yogurt.jpg', 'Yogurt Zero 500ml', 100, 0, 3, 0, 0, 0, 0, 8, 9, 10, 11, 12, 13, '2016-07-27'),
(16, 'fotoproducto/NoPicture.jpg', 'BOLSAS PEQUEÑAS BLANCAS ', 62, 10, 78, 12, 0, -2, 96, 8, 9, 10, 11, 12, 13, '2016-07-27'),
(19, 'fotoproducto/NoPicture.jpg', 'Servilletas', 62, 0, 3, 0, 12, -12, 0, 12, 9, 10, 11, 12, 13, '2016-07-27'),
(20, 'fotoproducto/NoPicture.jpg', 'Bolsas Grande Basura blanca ', 97, 20, 120, 0, 22, -2, 0, 22, 0, 0, 0, 0, 0, '2016-07-27'),
(38, 'fotoproducto/NoPicture.jpg', 'Limpia Todo', -5, 90, 94, 90, 0, 0, 720, 8, 9, 10, 11, 12, 13, '2016-07-27'),
(37, 'fotoproducto/NoPicture.jpg', 'Sapolio', 100, 0, 3, 0, 0, 0, 0, 8, 9, 9, 11, 12, 13, '2016-07-27'),
(39, 'fotoproducto/NoPicture.jpg', 'Limpia Todo', 65, 0, 3, 18, 0, -18, 144, 8, 9, 10, 11, 12, 13, '2016-07-27'),
(40, 'fotoproducto/NoPicture.jpg', 'Sapolio', 100, 0, 3, 0, 0, 0, 0, 8, 9, 10, 11, 12, 13, '2016-07-27'),
(41, 'fotoproducto/NoPicture.jpg', 'Trapos de Cocina ', 65, 10, 78, 12, 0, -2, 96, 8, 9, 10, 11, 12, 13, '2016-07-27'),
(43, 'fotoproducto/envase.jpg', 'envases y tapas 100 ml ', 62, 0, 3, 0, 12, -12, 0, 12, 9, 10, 11, 12, 13, '2016-07-27'),
(44, 'fotoproducto/NoPicture.jpg', 'guantes Quirurgicos  ', 100, 20, 120, 0, 0, 20, 0, 2, 0, 0, 0, 0, 0, '2016-07-27'),
(45, 'fotoproducto/NoPicture.jpg', 'scocht  ', 100, 0, 3, 0, 0, 0, 0, 8, 9, 10, 11, 12, 13, '2016-07-27'),
(46, 'fotoproducto/NoPicture.jpg', 'toalla papel ', 65, 10, 78, 12, 0, -2, 96, 8, 9, 10, 11, 12, 13, '2016-07-27'),
(47, 'fotoproducto/NoPicture.jpg', 'bolsa peuqeña 7x20', 62, 0, 3, 0, 22, -22, 0, 22, 9, 10, 11, 12, 13, '2016-07-27');

-- --------------------------------------------------------

--
-- Table structure for table `prductoprueba`
--

CREATE TABLE `prductoprueba` (
  `idproducto` int(11) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `codigo` varchar(150) NOT NULL,
  `marca` varchar(150) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fechaRegistro` date NOT NULL,
  `pmayor` double NOT NULL,
  `pmenor` double NOT NULL,
  `clase` varchar(150) NOT NULL,
  `genero` varchar(150) NOT NULL,
  `talla` int(11) NOT NULL,
  `proveedor` varchar(100) NOT NULL,
  `alarma` varchar(150) NOT NULL,
  `precioCompra` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prductoprueba`
--

INSERT INTO `prductoprueba` (`idproducto`, `imagen`, `codigo`, `marca`, `cantidad`, `fechaRegistro`, `pmayor`, `pmenor`, `clase`, `genero`, `talla`, `proveedor`, `alarma`, `precioCompra`) VALUES
(1, 'fotoproducto/f32769.jpg', 'F32769', 'ADIDAS', 23, '2016-05-27', 54, 56, 'Deportivo', 'Hombre', 37, 'La red', '10', 0),
(2, 'fotoproducto/B27113.jpg', 'B27113', 'ADIDAS', 25, '2016-05-27', 48.5, 0, 'ZAPATILLAS', 'MASCULINO', 0, 'MANACO SA', '10', 0),
(3, 'fotoproducto/b32934.jpg', 'B32934', 'ADIDAS', 46, '2016-05-27', 48.5, 0, 'ZAPATILLAS', 'MASCULINO', 0, 'MANACO SA', '10', 0),
(4, 'fotoproducto/B32900.jpg', 'B32900', 'NIKE', 156, '2016-05-27', 57, 0, 'ZAPATILLAS', 'MASCULINO', 0, 'MANACO SA', '10', 0),
(5, 'fotoproducto/B32947.jpg', 'B32947', 'PUMA', 85, '2016-05-27', 57, 0, 'ZAPATILLAS', 'MASCULINO', 0, 'MANACO SA', '10', 0),
(22, 'fotoproducto/nike001.jpg', 'NK00123', 'NIKE', 100, '2016-06-04', 56, 50, 'Deportivo', 'Hombre', 37, 'La red', '14', 3500),
(6, 'fotoproducto/S83222.jpg', 'S83222', 'ADIDAS', 45, '2016-05-27', 60, 0, 'ZAPATILLAS', 'MASCULINO', 0, 'MANACO SA', '10', 0),
(7, 'fotoproducto/B25497.jpg', 'B25497', 'GOLTY', 78, '2016-05-27', 53, 0, 'ZAPATILLAS', 'MASCULINO', 0, 'MANACO SA', '10', 0),
(8, 'fotoproducto/B23944.jpg', 'B23944', 'GOL', 30, '2016-05-27', 57, 0, 'ZAPATILLAS', 'MASCULINO', 0, 'MANACO SA', '10', 0),
(9, 'fotoproducto/s5.jpg', 'F22250', 'PUMA', 50, '2016-05-31', 250, 0, 'Raquet', 'Hombre', 0, 'MANACO SA', '10', 0),
(10, 'fotoproducto/B34361.jpg', 'B34361', 'PUMA', 100, '2016-05-31', 70, 0, 'ZAPATILLAS', 'MASCULINO', 0, 'MANACO SA', '10', 0),
(11, 'fotoproducto/B27113.jpg', 'B27113', 'ADIDAS', 78, '2016-06-02', 80, 0, 'ZAPATILLAS', 'MASCULINO', 0, 'MANACO SA', '10', 0),
(12, 'fotoproducto/S83243.jpg', 'S83243', 'REBBOK', 100, '2016-06-02', 65, 0, 'ZAPATILLAS', 'MASCULINO', 0, 'MANACO SA', '10', 0),
(13, 'fotoproducto/B23944.jpg', 'B23944', 'FILA', 100, '2016-06-02', 76, 0, 'ZAPATILLAS', 'MASCULINO', 0, 'MANACO SA', '10', 0),
(23, 'fotoproducto/puma.jpg', 'P12345', 'PUMA', 200, '2016-06-05', 100, 120, 'Deportivo', 'Hombre', 37, 'SURE', '12', 7500),
(14, 'fotoproducto/1.JPG', '34343', 'manaco', 200, '2016-06-03', 50, 60, '', '', 0, '14', '10', 0),
(16, 'fotoproducto/5.JPG', '7557857', 'adidas3', 50, '2016-06-03', 40, 50, 'Deportivo', 'Hombre', 37, 'Monaco', '10', 0),
(18, 'fotoproducto/b32934.jpg', 'B900100', 'TOPER', 100, '2016-06-03', 70, 60, 'Deportivo', 'Mujer', 37, 'uchu.comp', '10', 0),
(19, 'fotoproducto/B32900.jpg', 'B32900', 'ADIDAS-H', 100, '2016-06-03', 110, 105, 'Deportivo', 'Hombre', 37, 'Mac Solution', '10', 0),
(20, 'fotoproducto/nke.jpg', 'NK1123', 'NIKE', 100, '2016-06-03', 80, 90, 'Deportivo', 'Hombre', 40, 'Sr. Diego  Rodriguez', '12', 0),
(21, 'fotoproducto/nikeM.jpg', 'NK12345', 'NIKE', 123, '2016-06-04', 70, 85, 'Deportivo', 'Mujer', 37, 'Monaco', '12', 60),
(24, 'fotoproducto/rebook123.jpg', 'RBK1234', 'Reebok', 100, '2016-06-06', 120, 150, 'Deportivo', 'Hombre', 37, 'ARIEL SA', '17', 8000),
(25, 'fotoproducto/nike.jpg', 'NK1123', '1234', 100, '2016-06-06', 120, 150, 'Deportivo', 'Hombre', 37, 'ARIEL SA', '20', 13500),
(27, 'fotoproducto/puma123.jpg', 'P43456', 'puma', 68, '2016-06-07', 130, 150, 'Deportivo', 'Hombre', 37, 'Hypertrading ', '23', 2000);

-- --------------------------------------------------------

--
-- Table structure for table `prductozoe`
--

CREATE TABLE `prductozoe` (
  `idproducto` int(11) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `codigo` varchar(150) NOT NULL,
  `marca` varchar(150) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fechaRegistro` date NOT NULL,
  `pmayor` double NOT NULL,
  `pmenor` double NOT NULL,
  `clase` varchar(150) NOT NULL,
  `genero` varchar(150) NOT NULL,
  `talla` int(11) NOT NULL,
  `proveedor` varchar(100) NOT NULL,
  `alarma` varchar(150) NOT NULL,
  `precioCompra` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prductozoe`
--

INSERT INTO `prductozoe` (`idproducto`, `imagen`, `codigo`, `marca`, `cantidad`, `fechaRegistro`, `pmayor`, `pmenor`, `clase`, `genero`, `talla`, `proveedor`, `alarma`, `precioCompra`) VALUES
(24, 'fotoproducto/pomada.jpg', 'HG-300', 'FAT-OFF HOT GEL x 300 ml ', 50, '2016-06-06', 130, 160, 'Deportivo', 'Hombre', 37, 'GEL DE REDUCCION TERMICO CREMA DE REDUCCION TERMICA', '10', 130),
(25, 'fotoproducto/pomada.jpg', 'H-1000', 'FAT-OFF HOT x 1 kg', 34, '2016-06-07', 45, 56, 'Deportivo', 'Hombre', 37, 'CREMA DE REDUCCION TERMICA', '', 0),
(26, 'fotoproducto/pomada.jpg', 'H-500', 'FAT-OFF HOT x 500 gr', 45, '2016-06-07', 45, 66, 'Deportivo', 'Hombre', 37, 'CREMA DE REDUCCION TERMICA', '', 0),
(27, 'fotoproducto/pomada.jpg', 'H-100', 'FAT-OFF HOT x 100 gr ', 0, '2016-06-07', 70, 100, '', '', 0, 'CREMA DE REDUCCION TERMICA', '50,00', 37),
(28, 'fotoproducto/pomada.jpg', 'H-300', 'FAT-OFF HOT x 300 gr', 0, '2016-06-07', 119, 85, '', '', 0, 'CREMA DE REDUCCION TERMICA', '', 63),
(29, 'fotoproducto/pomada.jpg', 'HG-500', 'FAT-OFF HOT GEL x 500 ml', 0, '2016-06-07', 60, 50, '', '', 0, 'GEL DE REDUCCION TERMICO CREMA DE REDUCCION TERMICA', '', 45);

-- --------------------------------------------------------

--
-- Table structure for table `preventa`
--

CREATE TABLE `preventa` (
  `idPreventa` int(11) NOT NULL,
  `imagen` varchar(150) NOT NULL,
  `producto` text NOT NULL,
  `precio` double NOT NULL,
  `idProducto` varchar(100) NOT NULL,
  `pventa` varchar(150) NOT NULL,
  `idUser` varchar(900) NOT NULL,
  `tipo` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `preventa2`
--

CREATE TABLE `preventa2` (
  `idPreventa` int(11) NOT NULL,
  `imagen` varchar(150) NOT NULL,
  `producto` text NOT NULL,
  `precio` double NOT NULL,
  `idProducto` varchar(100) NOT NULL,
  `pventa` varchar(150) NOT NULL,
  `idjalea` varchar(900) NOT NULL,
  `tipo` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `producto`
--

CREATE TABLE `producto` (
  `idproducto` int(11) NOT NULL,
  `imagen` varchar(150) NOT NULL,
  `codigo` varchar(150) NOT NULL,
  `nombreProducto` varchar(150) NOT NULL,
  `cantidad` varchar(150) NOT NULL,
  `fechaRegistro` varchar(150) NOT NULL,
  `precioVenta` varchar(150) NOT NULL,
  `tipo` varchar(150) NOT NULL,
  `proveedor` varchar(150) NOT NULL,
  `precioCompra` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `producto`
--

INSERT INTO `producto` (`idproducto`, `imagen`, `codigo`, `nombreProducto`, `cantidad`, `fechaRegistro`, `precioVenta`, `tipo`, `proveedor`, `precioCompra`) VALUES
(50, 'fotoproducto/imagen_1349563845.jpg', 'Un Cuarto', 'Un Cuarto', '100', '2020-06-12', '23', 'POLLO', '6', '6'),
(51, 'fotoproducto/imagen_1349563882.jpg', 'DUOPOLLO', 'DUOPOLLO', '15', '2018-04-28', '16', 'POLLO', '6', '6'),
(52, 'fotoproducto/imagen_1349563944.jpg', 'LIMITO', 'LIMITO', '12', '2018-04-28', '13', 'POLLO', '6', '6'),
(54, 'fotoproducto/imagen_1349563944.jpg', 'ECONOMICO', 'ECONOMICO', '8', '2018-05-06', '8', 'POLLO', '6', '0'),
(55, 'fotoproducto/imagen_1349564147.jpg', 'SUPER COMBO', 'SUPER COMBO', '100', '2018-05-06', '10', 'POLLO', '6', '6'),
(56, 'fotoproducto/imagen_1349564147.jpg', 'ALMUERZO COMPLETO', 'ALMUERZO COMPLETO', '11', '2018-04-28', '10', 'POLLO', '6', '6'),
(57, 'fotoproducto/imagen_1349564229.jpg', 'SEGUNDO', 'SEGUNDO', '7', '2018-04-28', '8', 'POLLO', '6', '6'),
(58, 'fotoproducto/imagen_1349564269.jpg', 'HAMBURGUESA', 'HAMBURGUESA', '15', '2018-04-28', '8', 'POLLO', '6', '6'),
(59, 'fotoproducto/imagen_1349563882.jpg', 'Pollo Entero', 'Pollo Entero', '12', '2018-04-28', '60', 'POLLO', '6', '6'),
(60, 'fotoproducto/imagen_1349563882.jpg', 'MEDIO POLLO', 'MEDIO POLLO', '25', '2018-04-28', '30', 'POLLO', '6', '6'),
(61, 'fotoproducto/imagen_1349564000.jpg', 'SILLPANCHO', 'SILLPANCHO', '13', '2018-04-28', '13', 'POLLO', '6', '6'),
(62, 'fotoproducto/imagen_1349564103.jpg', 'SOPA DE MANI  GRANDE', 'SOPA DE MANI  GRANDE', '10', '2018-04-28', '7', 'POLLO', '6', '6'),
(63, 'fotoproducto/imagen_1349564195.jpg', 'SOPA DE MANI MEDIANA', 'SOPA DE MANI MEDIANA', '11', '2018-04-28', '4', 'POLLO', '6', '6'),
(65, 'fotoproducto/imagen_1349564195.jpg', 'PIPOCA DE POLLO', 'PIPOCA DE POLLO', '15', '2018-04-28', '12', 'POLLO', '6', '6'),
(66, 'fotoproducto/imagen_1349564365.jpg', 'Cuero', 'Cuero', '11', '2016-08-22', '1', 'POLLO', '6', '6'),
(67, 'fotoproducto/imagen_1349564382.jpg', 'Hueso', 'Hueso', '11', '2016-08-22', '1', 'POLLO', '6', '6'),
(68, 'fotoproducto/imagen_1349564425.jpg', 'Presa', 'Presa', '1', '2016-08-22', '7', 'POLLO', '6', '6'),
(69, 'fotoproducto/imagen_134951011.jpg', 'PORCION DE ARROZ', 'PORCION DE ARROZ', '10', '2018-04-28', '7', 'POLLO', '6', '6'),
(70, 'fotoproducto/imagen_134951110.jpg', 'PORCION DE PAPA', 'PORCION DE PAPA', '100', '2018-04-28', '7', 'POLLO', '6', '6'),
(71, 'fotoproducto/imagen_1385599810.jpg', 'SPIEDO PERSONAL', 'SPIEDO PERSONAL', '12', '2018-04-28', '13', 'POLLO', '6', '6'),
(72, 'fotoproducto/imagen_1349564053.jpg', 'CUEARTO SPIEDO ', 'CUEARTO SPIEDO ', '100', '2018-04-28', '18', 'POLLO', '6', '6'),
(73, 'fotoproducto/imagen_1349564463.jpg', 'SIMBA MANZANA', 'SIMBA MANZANA', '5', '2018-04-28', '10', 'REFRESCO', '6', '6'),
(74, 'fotoproducto/imagen_1349564498.jpg', 'Popular Sprite', 'Popular Sprite', '100', '2016-08-22', '5', 'REFRESCO', '6', '6'),
(75, 'fotoproducto/imagen_1349564524.jpg', 'Popular Coca Cola', 'Popular Coca Cola', '11', '2016-08-22', '5', 'REFRESCO', '6', '6'),
(76, 'fotoproducto/imagen_1349564587.jpg', 'DEL VALLE', 'DEL VALLE', '100', '2018-04-28', '10', 'REFRESCO', '6', '6'),
(77, 'fotoproducto/imagen_1349564926.jpg', 'COCA COLA 1 ltr', 'COCA COLA 1 ltr', '11', '2018-04-28', '8', 'REFRESCO', '6', '6'),
(78, 'fotoproducto/imagen_1349564902.jpg', 'COCA COLA 2 1/2 lts', 'COCA COLA 2 1/2 lts', '10', '2018-04-28', '13', 'REFRESCO', '6', '6'),
(79, 'fotoproducto/imagen_1349564958.jpg', 'COCA COLA DE 3 lts', 'COCA COLA DE 3 lts', '12', '2018-04-28', '14', 'REFRESCO', '6', '6'),
(80, 'fotoproducto/imagen_1349564989.jpg', 'Fanta 2 1/2 lts ', 'Fanta 2 1/2 lts ', '10', '2016-08-22', '10', 'REFRESCO', '6', '6'),
(81, 'fotoproducto/imagen_1349565020.jpg', 'Sprite ', 'Sprite ', '11', '2016-08-22', '11', 'REFRESCO', '6', '6'),
(82, 'fotoproducto/imagen_1349565068.jpg', 'SIMBA PIÃ‘A', 'SIMBA PIÃ‘A', '11', '2018-04-28', '10', 'REFRESCO', '6', '6'),
(83, 'fotoproducto/imagen_1349564618.jpg', 'Tostada', 'Tostada', '4', '2016-08-22', '4', 'HERVIDO', '6', '4'),
(84, 'fotoproducto/imagen_1349564663.jpg', 'Limonada 1/2', 'Limonada 1/2', '10', '2016-08-22', '10', 'HERVIDO', '6', '4'),
(85, 'fotoproducto/imagen_1349564697.jpg', 'Limonada 1 1/2 ', 'Limonada 1 1/2 ', '7', '2016-08-22', '7', 'HERVIDO', '6', '10'),
(86, 'fotoproducto/imagen_1349564726.jpg', 'Limonada 1lts', 'Limonada 1lts', '4', '2016-08-22', '4', 'HERVIDO', '6', '7'),
(87, 'fotoproducto/imagen_1349564784.jpg', 'Tostada 1lts', 'Tostada 1lts', '10', '2016-08-22', '10', 'HERVIDO', '6', '7'),
(88, 'fotoproducto/imagen_1349564824.jpg', 'Tostada 1 1/2 lts', 'Tostada 1 1/2 lts', '7', '2016-08-22', '10', 'HERVIDO', '6', '10'),
(89, 'fotoproducto/imagen_1349564545.jpg', 'POPULAR FANTA', 'POPULAR FANTA', '10', '2018-04-28', '5', 'REFRESCO', '6', '6'),
(90, 'fotoproducto/imagen_1349564587.jpg', 'TROPI FRUT', 'TROPI FRUT', '10', '2018-04-28', '6', 'REFRESCO', '6', '6'),
(91, 'fotoproducto/images (1).jpg', '003', 'Piernas Imba 3', '2', '2020-06-13', '', 'ARIEL SA', '', ''),
(92, 'fotoproducto/imagen_1349565020.jpg', 'Sprite 2 1/2 ', 'Sprite 2 1/2 ', '100', '2016-08-22', '13', 'REFRESCO', '6', '6'),
(93, 'fotoproducto/NoPicture.jpg', 'TOSTADA 2L', 'TOSTADA 2L', '12', '2018-04-28', '12', 'HERVIDO', '6', '6'),
(94, 'fotoproducto/NoPicture.jpg', 'tostada en vaso', 'tostada en vaso', '0', '2018-05-06', '3', 'HERVIDO', '6', '6'),
(134, 'fotoproducto/cocacola.jpg', '101', 'Coca Cola 500 gr', '', '2020-06-12', '7', 'POLLO', '5', '6');

-- --------------------------------------------------------

--
-- Table structure for table `productocambio`
--

CREATE TABLE `productocambio` (
  `idcambio` int(11) NOT NULL,
  `codigo` varchar(150) NOT NULL,
  `imagen` varchar(150) NOT NULL,
  `precio` varchar(100) NOT NULL,
  `cantidad` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productoe`
--

CREATE TABLE `productoe` (
  `idProductoE` int(11) NOT NULL,
  `productoElegido` varchar(50) NOT NULL,
  `idproducto` varchar(50) NOT NULL,
  `jaleas` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productoe`
--

INSERT INTO `productoe` (`idProductoE`, `productoElegido`, `idproducto`, `jaleas`) VALUES
(1, 'Yogurt  + JALEA', '1', ''),
(2, 'Yogurt  + JALEA', '1', ''),
(3, 'Yogurt  + JALEA', '1', ''),
(4, 'Yogurt  + JALEA', '1', ''),
(5, 'Yogurt  + JALEA', '1', ''),
(6, 'Yogurt ZERO 500 ml GZ', '3', ''),
(7, 'Yogurt ZERO 250 ml GZ', '5', ''),
(8, 'Yogurt Normal 500 ml GZ', '4', ''),
(9, 'Yogurt  ', '1', ''),
(10, 'Yogurt ZERO 500 ml', '3', ''),
(11, 'Yogurt ZERO 500 ml', '3', ''),
(12, 'Yogurt ZERO 500 ml', '3', ''),
(13, 'Yogurt  ', '1', ''),
(14, 'Yogurt  ', '1', ''),
(15, 'Yogurt  ', '1', ''),
(16, 'Yogurt  ', '1', ''),
(17, 'Yogurt  ', '1', ''),
(18, 'Yogurt  ', '1', ''),
(19, 'Yogurt Normal 500 ml', '4', ''),
(20, 'Yogurt ZERO 500 ml', '3', ''),
(21, 'Yogurt  ', '1', ''),
(22, 'Yogurt  ', '1', ''),
(23, 'Yogurt  ', '1', ''),
(24, 'Yogurt  ', '1', ''),
(25, 'Yogurt  ', '1', ''),
(26, 'Yogurt  ', '1', ''),
(27, 'Yogurt  ', '1', ''),
(28, 'Yogurt  ', '1', ''),
(29, 'Yogurt  ', '1', ''),
(30, 'Yogurt  ', '1', ''),
(31, 'Yogurt  ', '1', ''),
(32, 'Yogurt  ', '1', ''),
(33, 'Yogurt  ', '1', ''),
(34, 'Yogurt  ', '1', ''),
(35, 'Yogurt  ', '1', ''),
(36, 'Yogurt  ', '1', ''),
(37, 'Yogurt  ', '1', ''),
(38, 'Yogurt  ', '1', ''),
(39, 'Yogurt  ', '1', ''),
(40, 'Yogurt  ', '1', ''),
(41, 'Yogurt  ', '1', ''),
(42, 'Yogurt ZERO 250 ml', '5', ''),
(43, 'Yogurt ZERO 500 ml', '3', ''),
(44, 'Yogurt Normal 500 ml', '4', ''),
(45, 'Yogurt Normal 500 ml', '4', ''),
(46, 'Yogurt ZERO 500 ml', '3', ''),
(47, 'Yogurt  ', '1', ''),
(48, 'Yogurt  ', '1', ''),
(49, 'Yogurt  ', '1', ''),
(50, 'Yogurt  ', '1', ''),
(51, 'Yogurt ZERO 250 ml', '5', ''),
(52, 'Yogurt ZERO 250 ml', '5', ''),
(53, 'Yogurt ZERO 250 ml', '5', ''),
(54, 'Yogurt Normal 500 ml', '4', ''),
(55, 'Yogurt ZERO 250 ml', '5', ''),
(56, 'Yogurt Normal 500 ml', '4', ''),
(57, 'Yogurt ZERO 500 ml', '3', ''),
(58, 'Yogurt ZERO 500 ml', '3', ''),
(59, 'Yogurt ZERO 500 ml', '3', ''),
(60, 'Yogurt ZERO 500 ml', '3', ''),
(61, 'Yogurt Normal 500 ml', '4', ''),
(62, 'Yogurt ZERO 500 ml', '3', ''),
(63, 'Yogurt ZERO 500 ml', '3', ''),
(64, 'Yogurt  ', '1', ''),
(65, 'Yogurt  ', '1', ''),
(66, 'Yogurt  ', '1', ''),
(67, 'Yogurt  ', '1', ''),
(68, 'Yogurt  ', '1', ''),
(69, 'Yogurt  ', '1', ''),
(70, 'Yogurt  ', '1', ''),
(71, 'Yogurt  ', '1', ''),
(72, 'Yogurt  ', '1', ''),
(73, 'Yogurt  ', '1', ''),
(74, 'Yogurt  ', '1', ''),
(75, 'Yogurt  ', '1', ''),
(76, 'Yogurt  ', '1', ''),
(77, 'Yogurt  ', '1', ''),
(78, 'Yogurt  ', '1', ''),
(79, 'Yogurt  ', '1', ''),
(80, 'Yogurt  ', '1', ''),
(81, 'Yogurt  ', '1', ''),
(82, 'Yogurt  ', '1', ''),
(83, 'Yogurt  ', '1', ''),
(84, 'Yogurt ZERO 500 ml', '3', ''),
(85, 'Yogurt ZERO 500 ml', '3', ''),
(86, 'Yogurt  ', '1', ''),
(87, 'Yogurt  ', '1', ''),
(88, 'Yogurt  ', '1', ''),
(89, 'Yogurt  ', '1', ''),
(90, 'Yogurt ZERO 500 ml', '3', ''),
(91, 'Yogurt Normal 500 ml', '4', ''),
(92, 'Yogurt Normal 500 ml', '4', ''),
(93, 'Yogurt ZERO 500 ml', '3', ''),
(94, 'Yogurt Normal 500 ml', '4', ''),
(95, 'Yogurt Normal 500 ml', '4', ''),
(96, 'Yogurt ZERO 500 ml', '3', ''),
(97, 'Yogurt ZERO 500 ml', '3', ''),
(98, 'Yogurt  ', '1', ''),
(99, 'Yogurt  ', '1', ''),
(100, 'Yogurt  ', '1', ''),
(101, 'Yogurt  ', '1', ''),
(102, 'Yogurt  ', '1', ''),
(103, 'Yogurt  ', '1', ''),
(104, 'Yogurt  ', '1', ''),
(105, 'Yogurt  ', '1', ''),
(106, 'Yogurt  ', '1', ''),
(107, 'Yogurt  ', '1', ''),
(108, 'Yogurt  ', '1', ''),
(109, 'Yogurt  ', '1', ''),
(110, 'Yogurt  ', '1', ''),
(111, 'Yogurt  ', '1', ''),
(112, 'Yogurt  ', '1', ''),
(113, 'Yogurt  ', '1', ''),
(114, 'Yogurt  ', '1', ''),
(115, 'Yogurt  ', '1', ''),
(116, 'Yogurt  ', '1', ''),
(117, 'Yogurt  ', '1', ''),
(118, 'Yogurt  ', '1', ''),
(119, 'Yogurt  ', '1', ''),
(120, 'Yogurt  ', '1', ''),
(121, 'Yogurt  ', '1', ''),
(122, 'Yogurt  ', '1', ''),
(123, 'Yogurt  ', '1', ''),
(124, 'Yogurt  ', '1', ''),
(125, 'Yogurt  ', '1', ''),
(126, 'Yogurt  ', '1', ''),
(127, 'Yogurt  ', '1', ''),
(128, 'Yogurt  ', '1', ''),
(129, 'Yogurt  ', '1', ''),
(130, 'Yogurt  ', '1', ''),
(131, 'Yogurt  ', '1', ''),
(132, 'Yogurt  ', '1', ''),
(133, 'Yogurt  ', '1', ''),
(134, 'Yogurt  ', '1', ''),
(135, 'Yogurt  ', '1', ''),
(136, 'Yogurt  ', '1', ''),
(137, 'Yogurt  ', '1', ''),
(138, 'Yogurt  ', '1', ''),
(139, 'Yogurt  ', '1', ''),
(140, 'Yogurt  ', '1', ''),
(141, 'Yogurt  ', '1', ''),
(142, 'Yogurt  ', '1', ''),
(143, 'Yogurt  ', '1', ''),
(144, 'Yogurt  ', '1', ''),
(145, 'Yogurt  ', '1', ''),
(146, 'Yogurt  ', '1', ''),
(147, 'Yogurt  ', '1', ''),
(148, 'Yogurt  ', '1', ''),
(149, 'Yogurt  ', '1', ''),
(150, 'Yogurt ZERO 500 ml', '3', ''),
(151, 'Yogurt  ', '1', ''),
(152, 'Yogurt  ', '1', ''),
(153, 'Yogurt  ', '1', ''),
(154, 'Yogurt  ', '1', ''),
(155, 'Yogurt  ', '1', ''),
(156, 'Yogurt  ', '1', ''),
(157, 'Yogurt  ', '1', ''),
(158, 'Yogurt ZERO 500 ml', '3', ''),
(159, 'Yogurt ZERO 500 ml', '3', ''),
(160, 'Yogurt ZERO 500 ml', '3', ''),
(161, 'Yogurt ZERO 500 ml', '3', ''),
(162, 'Yogurt  ', '1', ''),
(163, 'Yogurt Normal 500 ml', '4', ''),
(164, 'Yogurt  ', '1', ''),
(165, 'Yogurt ZERO 500 ml', '3', ''),
(166, 'Yogurt ZERO 500 ml', '3', ''),
(167, 'Yogurt  ', '1', ''),
(168, 'Yogurt  ', '1', ''),
(169, 'Yogurt  ', '1', ''),
(170, 'Yogurt  ', '1', ''),
(171, 'Yogurt  ', '1', ''),
(172, 'Yogurt ZERO 500 ml', '3', ''),
(173, 'Yogurt  ', '1', ''),
(174, 'Yogurt  ', '1', ''),
(175, 'Yogurt  ', '1', ''),
(176, 'Yogurt  ', '1', ''),
(177, 'Yogurt  ', '1', ''),
(178, 'Yogurt  ', '1', ''),
(179, 'Yogurt  ', '1', ''),
(180, 'Yogurt  ', '1', ''),
(181, 'Yogurt  ', '1', ''),
(182, 'Yogurt  ', '1', ''),
(183, 'Yogurt Normal 500 ml', '4', ''),
(184, 'Yogurt  ', '1', ''),
(185, 'Yogurt ZERO 500 ml', '3', ''),
(186, 'Yogurt Normal 500 ml', '4', '');

-- --------------------------------------------------------

--
-- Table structure for table `productoelegido`
--

CREATE TABLE `productoelegido` (
  `idproductoElegido` int(11) NOT NULL,
  `productoElegido` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profesional`
--

CREATE TABLE `profesional` (
  `ced_prof` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `nombre_apellido` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `tipo_prof` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profesional`
--

INSERT INTO `profesional` (`ced_prof`, `nombre_apellido`, `tipo_prof`) VALUES
('6444685', 'HENRY CALANI A.', 'MEDICO'),
('6222345', 'SUSANA', 'MEDICO'),
('123456', 'LETI CALANI', 'MEDICO'),
('6446468', 'HENRY JOSEPH', 'COORDINADOR');

-- --------------------------------------------------------

--
-- Table structure for table `proveedor`
--

CREATE TABLE `proveedor` (
  `idproveedor` int(11) NOT NULL,
  `proveedor` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `responsable` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `fechaRegistro` date NOT NULL,
  `direccion` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `fechaAviso` date NOT NULL,
  `valor` double NOT NULL,
  `valorCobrado` double NOT NULL,
  `saldo` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `proveedor`
--

INSERT INTO `proveedor` (`idproveedor`, `proveedor`, `responsable`, `fechaRegistro`, `direccion`, `telefono`, `estado`, `fechaAviso`, `valor`, `valorCobrado`, `saldo`) VALUES
(12, 'Hypertrading ', 'Jose Lopez', '2016-04-15', 'Av. Radial 17', '3342345', 'CONTADO', '2016-08-11', 6000, 6000, '0'),
(11, 'SURE', 'juan perez', '2016-04-15', 'Av. siempre viva ', '77786774', 'CONTADO', '0000-00-00', 7500, 7500, '0'),
(30, 'Bolivia Sport', 'Monga', '2016-06-08', 'Av. specie', '44234423', 'REGISTRAR', '0000-00-00', 0, 0, '0'),
(29, 'ARIEL SA', 'ariel moranda', '2020-06-06', 'circuvalacion', '44433777', 'CONTADO', '0000-00-00', 7000, 7000, '0');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `idproducto` int(11) NOT NULL,
  `imagen` varchar(200) NOT NULL,
  `codigo` varchar(150) NOT NULL,
  `nombreProducto` varchar(150) NOT NULL,
  `cantidadStock` int(11) NOT NULL,
  `cantidadVendido` int(11) NOT NULL,
  `fechaRegistro` date NOT NULL,
  `proveedor` varchar(100) NOT NULL,
  `alarma` varchar(150) NOT NULL,
  `precioCompra` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`idproducto`, `imagen`, `codigo`, `nombreProducto`, `cantidadStock`, `cantidadVendido`, `fechaRegistro`, `proveedor`, `alarma`, `precioCompra`) VALUES
(2, 'fotoproducto/parfait.jpg', 'Parfait 200 ml', 'Parfait 200 ml', 350, 46, '2016-08-16', 'MANACO SA', '10', '2'),
(3, 'fotoproducto/zero.jpg', 'Yogurt ZERO 500 ml', 'Yogurt ZERO 500 ml', 350, 156, '2016-08-16', 'MANACO SA', '10', '2'),
(4, 'fotoproducto/yogurt.jpg', 'Yogurt Normal 500 ml', 'Yogurt Normal 500 ml', 350, 85, '2016-08-16', 'MANACO SA', '10', '2'),
(5, 'fotoproducto/zero.jpg', 'Yogurt ZERO 250 ml', 'Yogurt ZERO 250 ml', 350, 45, '2016-08-16', 'MANACO SA', '10', '2'),
(6, 'fotoproducto/yogurt.jpg', 'Yogurt Normal 250 ml ', 'Yogurt Normal 250 ml ', 350, 78, '2016-08-16', 'MANACO SA', '10', '2'),
(7, 'fotoproducto/NoPicture.jpg', 'bolsa peuqeña 7x20', 'bolsa peuqeña 7x20', 350, 30, '2016-08-16', 'MANACO SA', '10', '2'),
(8, 'fotoproducto/NoPicture.jpg', 'toalla papel ', 'toalla papel ', 350, 50, '2016-08-16', 'MANACO SA', '10', '2'),
(9, 'fotoproducto/NoPicture.jpg', 'scocht  ', 'scocht  ', 350, 100, '2016-08-16', 'MANACO SA', '10', '2'),
(10, 'fotoproducto/NoPicture.jpg', 'guantes Quirurgicos  ', 'guantes Quirurgicos  ', 350, 7800, '2016-08-16', 'MANACO SA', '10', '2'),
(11, 'fotoproducto/NoPicture.jpg', 'Trapos de Cocina ', 'Trapos de Cocina ', 350, 100, '2016-08-16', 'MANACO SA', '10', '2'),
(12, 'fotoproducto/NoPicture.jpg', 'Sapolio', 'Sapolio', 350, 100, '2016-08-16', 'MANACO SA', '10', '2'),
(13, 'fotoproducto/NoPicture.jpg', 'Limpia Todo', 'Limpia Todo ', 350, 50, '2016-08-16', 'La red', '10', '2'),
(14, 'fotoproducto/NoPicture.jpg', 'Sapolio', 'Sapolio', 350, 100, '2016-08-16', 'uchu.comp', '10', '2'),
(15, 'fotoproducto/NoPicture.jpg', 'Limpia Todo', 'Limpia Todo', 350, 100, '2016-08-16', 'Mac Solution', '10', '2'),
(16, 'fotoproducto/NoPicture.jpg', 'Bolsas Grande Basura blanca ', 'Bolsas Grande Basura blanca ', 350, 100, '2016-08-16', 'Sr. Diego  Rodriguez', '12', '2'),
(17, 'fotoproducto/NoPicture.jpg', 'Servilletas', 'Servilletas', 350, 123, '2016-08-16', 'Monaco', '12', '2'),
(18, 'fotoproducto/NoPicture.jpg', 'Cucharillas', 'Cucharillas', 350, 34, '2016-08-16', 'Hypertrading ', '', '2'),
(19, 'fotoproducto/NoPicture.jpg', 'BOLSAS PEQUEÑAS BLANCAS ', 'BOLSAS PEQUEÑAS BLANCAS ', 350, 50, '2016-08-16', 'Hypertrading ', '', '2'),
(20, 'fotoproducto/zero.jpg', 'Yogurt Zero 500ml', 'Yogurt Zero 500ml', 350, 100, '2016-08-16', 'Hypertrading ', '20', '2'),
(21, 'fotoproducto/yogurt.jpg', 'Yogurt Normal 500ml', 'Yogurt Normal 500ml', 350, 200, '2016-08-16', 'ARIEL SA', '25', '2'),
(22, 'fotoproducto/zero.jpg', 'Yogurt Zero 250ml', 'Yogurt Zero 250ml', 350, 68, '2016-08-16', 'Hypertrading ', '23', '2'),
(23, 'fotoproducto/yogurt.jpg', 'Yogurt Normal 250ml', 'Yogurt Normal 250ml', 350, 67, '2016-08-16', 'ARIEL SA', '25', '2'),
(24, 'fotoproducto/mixto.jpg', 'FRUTILLA / DURAZNO', 'FRUTILLA / DURAZNO', 350, 50, '2016-08-16', 'Hypertrading', '87', '2'),
(25, 'fotoproducto/durazno.jpg', 'DURAZNO ', 'DURAZNO ', 350, 100, '2016-08-16', 'Hypertrading ', '', '2'),
(26, 'fotoproducto/frutilla.jpg', 'FRUTILLA ', 'FRUTILLA ', 350, 100, '2016-08-16', 'Hypertrading ', '', '2'),
(27, 'fotoproducto/pera.jpg', 'PERA / MANZANA ', 'PERA / MANZANA ', 350, 25, '2016-08-16', 'MANACO SA', '30', '2'),
(28, 'fotoproducto/mocon.jpg', 'MOCOCHINCHI', 'MOCOCHINCHI', 350, 46, '2016-08-16', 'MANACO SA', '10', '2'),
(29, 'fotoproducto/marcuya.jpg', 'MARACUYA', 'MARACUYA', 350, 156, '2016-08-16', 'MANACO SA', '10', '2'),
(30, 'fotoproducto/remolacha.jpg', 'REMOLACHA / FRUTILLA', 'REMOLACHA / FRUTILLA', 350, 85, '2016-08-16', 'MANACO SA', '10', '2'),
(31, 'fotoproducto/zanahoria.jpg', 'ZANAHORIA / NARANJA ', 'ZANAHORIA / NARANJA ', 350, 45, '2016-08-16', 'MANACO SA', '10', '2'),
(32, 'fotoproducto/envase.jpg', 'envases y tapas 100 ml ', 'envases y tapas 100 ml ', 350, 78, '2016-08-16', 'MANACO SA', '10', '2'),
(34, 'fotoproducto/musli.jpg', 'musli', 'musli', 350, 11, '2016-08-16', 'Hypertrading ', '', '2'),
(35, 'fotoproducto/NoPicture.jpg', 'Carlitos', 'Carlitos', 350, 100, '2016-08-19', '1', '', ''),
(36, 'fotoproducto/NoPicture.jpg', 'carlta', 'carlta', 350, 100, '2016-08-19', '4', '', ''),
(37, 'fotoproducto/NoPicture.jpg', 'prodcuto', 'prodcuto', 350, 2, '2016-08-19', '4', '', ''),
(39, 'fotoproducto/NoPicture.jpg', 'carlitos', 'carlitos', 350, 100, '2016-08-19', '4', '', ''),
(40, 'fotoproducto/NoPicture.jpg', 'pollo', 'pollo', 350, 50, '2016-08-19', '4', '', ''),
(42, 'fotoproducto/NoPicture.jpg', 'pollo', 'pollo', 350, 50, '2016-08-19', '4', '', ''),
(44, 'fotoproducto/NoPicture.jpg', 'pollos', 'pollos', 350, 121, '2016-08-19', '1', '', ''),
(46, 'fotoproducto/imagen_1349563916.jpg', 'Carlitos', 'Carlitos', 350, 100, '2016-08-19', '1', '', ''),
(47, 'fotoproducto/imagen_1349563845.jpg', 'Un Cuarto', 'Un Cuarto', 350, 100, '2016-08-19', '6', '', ''),
(48, 'fotoproducto/imagen_1349563882.jpg', 'Carlitos', 'Carlitos', 350, 15, '2016-08-19', '6', '', ''),
(49, 'fotoproducto/imagen_1349563944.jpg', 'Carlita', 'Carlita', 350, 12, '2016-08-19', '6', '', ''),
(50, 'fotoproducto/NoPicture.jpg', 'Economico', 'Economico', 350, 8, '2016-08-19', '6', '', ''),
(51, 'fotoproducto/imagen_1349563944.jpg', 'Economico', 'Economico', 350, 8, '2016-08-19', '6', '', ''),
(52, 'fotoproducto/imagen_1349564147.jpg', 'Cabanita', 'Cabanita', 350, 100, '2016-08-19', '6', '', ''),
(53, 'fotoproducto/imagen_1349564147.jpg', 'Cabanita ', 'Cabanita ', 350, 11, '2016-08-19', '6', '', ''),
(54, 'fotoproducto/imagen_1349564229.jpg', 'Nuggets', 'Nuggets', 350, 7, '2016-08-19', '6', '', ''),
(55, 'fotoproducto/imagen_1349564269.jpg', 'Nuggets', 'Nuggets', 350, 15, '2016-08-19', '6', '', ''),
(56, 'fotoproducto/imagen_1349563882.jpg', 'Pollo Entero', 'Pollo Entero', 350, 12, '2016-08-19', '6', '', ''),
(57, 'fotoproducto/imagen_1349563882.jpg', 'Medio Pollos', 'Medio Pollos', 350, 25, '2016-08-19', '6', '', ''),
(58, 'fotoproducto/imagen_1349564000.jpg', 'Churrasco', 'Churrasco', 350, 13, '2016-08-19', '6', '', ''),
(59, 'fotoproducto/imagen_1349564103.jpg', 'Pollo a la Parrilla', 'Pollo a la Parrilla', 350, 10, '2016-08-19', '6', '', ''),
(60, 'fotoproducto/imagen_1349564195.jpg', 'Milanesa', 'Milanesa', 350, 11, '2016-08-19', '6', '', ''),
(61, 'fotoproducto/NoPicture.jpg', 'Milanesa ', 'Milanesa ', 350, 11, '2016-08-19', '6', '', ''),
(62, 'fotoproducto/imagen_1349564195.jpg', 'Milanesa', 'Milanesa', 350, 15, '2016-08-19', '6', '', ''),
(63, 'fotoproducto/imagen_1349564365.jpg', 'Cuero', 'Cuero', 350, 11, '2016-08-19', '6', '', ''),
(64, 'fotoproducto/imagen_1349564382.jpg', 'Hueso', 'Hueso', 350, 11, '2016-08-19', '6', '', ''),
(65, 'fotoproducto/imagen_1349564425.jpg', 'Presa', 'Presa', 350, 1, '2016-08-19', '6', '', ''),
(66, 'fotoproducto/imagen_134951011.jpg', 'Porcion Arroz', 'Porcion Arroz', 350, 10, '2016-08-19', '6', '', ''),
(67, 'fotoproducto/imagen_134951110.jpg', 'Porcion Papa', 'Porcion Papa', 350, 100, '2016-08-19', '6', '', ''),
(68, 'fotoproducto/imagen_1385599810.jpg', 'Spiedo ', 'Spiedo ', 350, 12, '2016-08-19', '6', '', ''),
(69, 'fotoproducto/imagen_1349564053.jpg', 'Spiedo', 'Spiedo', 350, 100, '2016-08-19', '6', '', ''),
(70, 'fotoproducto/imagen_1349564463.jpg', 'Gordita', 'Gordita', 350, 5, '2016-08-19', '6', '', ''),
(71, 'fotoproducto/imagen_1349564498.jpg', 'Popular Sprite', 'Popular Sprite', 350, 100, '2016-08-19', '6', '', ''),
(72, 'fotoproducto/imagen_1349564524.jpg', 'Popular Coca Cola', 'Popular Coca Cola', 350, 11, '2016-08-19', '6', '', ''),
(73, 'fotoproducto/imagen_1349564587.jpg', 'Tropical', 'Tropical', 350, 100, '2016-08-19', '6', '', ''),
(74, 'fotoproducto/imagen_1349564926.jpg', 'Coca Cola 2lts', 'Coca Cola 2lts', 350, 11, '2016-08-19', '6', '', ''),
(75, 'fotoproducto/imagen_1349564902.jpg', 'Coca Cola 2 1/5 lts', 'Coca Cola 2 1/5 lts', 350, 10, '2016-08-19', '6', '', ''),
(76, 'fotoproducto/imagen_1349564958.jpg', 'Fanta ', 'Fanta ', 350, 12, '2016-08-19', '6', '', ''),
(77, 'fotoproducto/imagen_1349564989.jpg', 'Fanta ', 'Fanta ', 350, 10, '2016-08-19', '6', '', ''),
(78, 'fotoproducto/imagen_1349565020.jpg', 'Sprite ', 'Sprite ', 350, 11, '2016-08-19', '6', '', ''),
(79, 'fotoproducto/imagen_1349565068.jpg', 'simba', 'simba', 350, 11, '2016-08-19', '6', '', ''),
(80, 'fotoproducto/imagen_1349564618.jpg', 'Tostada', 'Tostada', 350, 4, '2016-08-19', '6', '', ''),
(81, 'fotoproducto/imagen_1349564663.jpg', 'Limonada', 'Limonada', 350, 10, '2016-08-19', '6', '', ''),
(82, 'fotoproducto/imagen_1349564697.jpg', 'Limonada', 'Limonada', 350, 7, '2016-08-19', '6', '', ''),
(83, 'fotoproducto/imagen_1349564726.jpg', 'Limonada', 'Limonada', 350, 4, '2016-08-19', '6', '', ''),
(84, 'fotoproducto/imagen_1349564784.jpg', 'Tostada', 'Tostada', 350, 10, '2016-08-19', '6', '', ''),
(85, 'fotoproducto/imagen_1349564824.jpg', 'Tostada', 'Tostada', 350, 7, '2016-08-19', '6', '', ''),
(86, 'fotoproducto/imagen_1349564545.jpg', 'Popular Fanta', 'Popular Fanta', 350, 10, '2016-08-19', '6', '', ''),
(87, 'fotoproducto/imagen_1349564587.jpg', 'Tropi Frut  1lt', 'Tropi Frut  1lt', 350, 10, '2016-08-19', '6', '', ''),
(88, 'fotoproducto/imagen_1349564524.jpg', 'Coca Cola Mini', 'Coca Cola Mini', 350, 2, '2016-08-19', '6', '', ''),
(89, 'fotoproducto/imagen_1349565020.jpg', 'Sprite 2 1/2 ', 'Sprite 2 1/2 ', 350, 100, '2016-08-22', '6', '', ''),
(90, 'fotoproducto/pollo-broaster_700x466.jpg', '0013', 'Coca Cola 500 gr3', 350, 289, '2020-06-13', 'SURE', '290', '43');

-- --------------------------------------------------------

--
-- Table structure for table `sumatoria`
--

CREATE TABLE `sumatoria` (
  `idsumtoria` int(11) NOT NULL,
  `totalsuma` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sumatoria`
--

INSERT INTO `sumatoria` (`idsumtoria`, `totalsuma`) VALUES
(1, 98.9),
(2, 177.91),
(3, 867.57),
(4, 145.98),
(5, 19.87),
(6, 155.5),
(7, 905.5),
(8, 545.5),
(9, 525.5),
(10, 60.5),
(11, 740.5),
(12, 140.5),
(13, 10303),
(14, 451.4),
(15, 120.5),
(16, 174);

-- --------------------------------------------------------

--
-- Table structure for table `tipocambio`
--

CREATE TABLE `tipocambio` (
  `idcambio` int(11) NOT NULL,
  `cambio` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipocambio`
--

INSERT INTO `tipocambio` (`idcambio`, `cambio`) VALUES
(1, 6.96);

-- --------------------------------------------------------

--
-- Table structure for table `tipogasto`
--

CREATE TABLE `tipogasto` (
  `idgasto` int(11) NOT NULL,
  `tipo` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipogasto`
--

INSERT INTO `tipogasto` (`idgasto`, `tipo`) VALUES
(1, 'GAS'),
(2, 'PAN'),
(3, 'LECHE'),
(5, 'Tarjeta VIVA 20'),
(6, 'Envases de plastico'),
(7, 'GAS'),
(8, 'Taxi');

-- --------------------------------------------------------

--
-- Table structure for table `tipopago`
--

CREATE TABLE `tipopago` (
  `idtipopago` int(11) NOT NULL,
  `nombrepago` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipopago`
--

INSERT INTO `tipopago` (`idtipopago`, `nombrepago`) VALUES
(1, 'Combo'),
(2, 'Credito'),
(3, 'Saldo');

-- --------------------------------------------------------

--
-- Table structure for table `tipoproducto`
--

CREATE TABLE `tipoproducto` (
  `idtipoproducto` int(11) NOT NULL,
  `tipoproducto` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipoproducto`
--

INSERT INTO `tipoproducto` (`idtipoproducto`, `tipoproducto`) VALUES
(1, 'POLLO'),
(2, 'REFRESCO HERVIDO'),
(3, 'REFRESCO GASEOSA');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_prof`
--

CREATE TABLE `tipo_prof` (
  `cod_prof` int(11) NOT NULL,
  `denominacion_prof` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usu` int(11) NOT NULL,
  `login` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id_usu`, `login`, `tipo`, `nombre`, `password`, `foto`) VALUES
(39, 'ingrid', 'VENTAS', 'Ingrid', 'ingrid2016', 'fotoUsuario/user.png'),
(34, 'ariel', 'ADMINISTRADOR', 'Ariel Miranda', 'ariel2016', 'fotoUsuario/user.png'),
(44, 'henry', 'ADMINISTRADOR', 'Henry Calani', 'henry', 'fotoUsuario/user.png'),
(47, 'rossi', 'VENTAS', 'Rossi Bravo', 'bravo14346058', 'fotoUsuario/user.png');

-- --------------------------------------------------------

--
-- Table structure for table `utlidad`
--

CREATE TABLE `utlidad` (
  `idutilidad` int(11) NOT NULL,
  `ingresos` double NOT NULL,
  `gastos` double NOT NULL,
  `ventaTotal` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utlidad`
--

INSERT INTO `utlidad` (`idutilidad`, `ingresos`, `gastos`, `ventaTotal`) VALUES
(1, 192, 100, '92');

-- --------------------------------------------------------

--
-- Table structure for table `ventas`
--

CREATE TABLE `ventas` (
  `idVentas` int(11) NOT NULL,
  `imagenProducto` varchar(150) NOT NULL,
  `fotoCliente` varchar(150) NOT NULL,
  `nombreCliente` varchar(150) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `observaciones` text NOT NULL,
  `fechaRegistro` date NOT NULL,
  `ci` varchar(100) NOT NULL,
  `producto` varchar(100) NOT NULL,
  `precio` varchar(150) NOT NULL,
  `porPagar` double NOT NULL,
  `pventa` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ventas`
--

INSERT INTO `ventas` (`idVentas`, `imagenProducto`, `fotoCliente`, `nombreCliente`, `cantidad`, `observaciones`, `fechaRegistro`, `ci`, `producto`, `precio`, `porPagar`, `pventa`) VALUES
(1, 'fotoproducto/vidriotempladoparaipadair2.jpeg', 'fotopaciente/NoPicture.gif', 'BARRETO SUSANA', 1, 'No hay observacioes', '2016-04-17', '64446765', 'VIDRIO TEMPLADO PARA IPAD AIR 2   ', '100', 320, 150),
(2, 'fotoproducto/durotoshibade1Tb.jpeg', 'fotopaciente/NoPicture.gif', 'BARRETO SUSANA', 2, 'No hay observacioes', '2016-04-18', '64446765', ' DURO TOSHIBA DE 1 TB   ', '110', 320, 150),
(3, 'fotoproducto/vidriotempladoparaipadair2.jpeg', 'fotopaciente/NoPicture.gif', 'BARRETO LETICIA', 1, 'compro hoy en 19 de marzo 2016', '2016-04-19', '5444565', 'VIDRIO TEMPLADO PARA IPAD AIR 2   ', '100', 155, 1000),
(4, 'fotoproducto/durotoshibade1Tb.jpeg', 'fotopaciente/NoPicture.gif', 'BARRETO LETICIA', 1, 'compro hoy en 19 de marzo 2016', '2016-04-19', '5444565', ' DURO TOSHIBA DE 1 TB   ', '55', 155, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `ventasdetalle`
--

CREATE TABLE `ventasdetalle` (
  `ventasDetalleId` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `codTransaccion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `cantidad` int(11) NOT NULL,
  `producto` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `precio` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `total` double NOT NULL,
  `tipo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `estadoTransaccion` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `commentario` varchar(150) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `ventasdetalle`
--

INSERT INTO `ventasdetalle` (`ventasDetalleId`, `fecha`, `codTransaccion`, `nombre`, `cantidad`, `producto`, `precio`, `total`, `tipo`, `usuario`, `estadoTransaccion`, `commentario`) VALUES
(8, '2020-04-22', 'CD-DE-EE-RR-FF', 'Henry Calani', 2, 'Un Cuarto', '46', 92, 'llevar', '1', 'Consolidado', ''),
(2, '2020-04-21', 'CD-DE-EE-RR-FF', 'Henry Calani', 2, 'Un Cuarto', '46', 92, 'llevar', '1', 'Consolidado', 'La hoja si se encontro'),
(3, '2020-04-21', 'CD-DE-EE-RR-FF', 'Henry Calani', 1, 'DUOPOLLO', '23', 23, 'mesa', '1', 'Consolidado', ''),
(4, '2020-04-21', 'CD-DE-EE-RR-FF', 'Henry Calani', 1, 'LIMITO', '12', 12, 'mesa', '1', 'No Consolidado', 'No se encontro la ficha vero se vendio'),
(5, '2020-04-21', 'CD-DE-EE-RR-FF', 'Henry Calani', 2, 'Un Cuarto', '46', 92, 'llevar', '1', 'No Consolidado', ''),
(6, '2020-04-21', 'CD-DE-EE-RR-FF', 'Henry Calani', 1, 'DUOPOLLO', '23', 23, 'mesa', '1', 'No Consolidado', ''),
(7, '2020-04-21', 'CD-DE-EE-RR-FF', 'Henry Calani', 1, 'LIMITO', '12', 12, 'mesa', '1', 'No Consolidado', ''),
(9, '2020-04-22', 'CD-DE-EE-RR-FF', 'Henry Calani', 1, 'DUOPOLLO', '23', 23, 'mesa', '1', 'No Consolidado', ''),
(10, '2020-04-22', 'CD-DE-EE-RR-FF', 'Henry Calani', 1, 'LIMITO', '12', 12, 'mesa', '1', 'No Consolidado', ''),
(11, '2020-04-24', 'CD-DE-EE-RR-FF', 'Henry Calani', 1, 'SOPA DE MANI  GRANDE', '7', 7, 'Mesa', '1', 'No Consolidado', ''),
(12, '2020-04-24', 'CD-DE-EE-RR-FF', 'Henry Calani', 1, 'SILLPANCHO', '13', 13, 'Llevar', '1', 'No Consolidado', ''),
(13, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '1', 'No Consolidado', ''),
(14, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Llevar', '1', 'No Consolidado', ''),
(15, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 2, 'LIMITO', '26', 52, 'Mesa', '1', 'No Consolidado', ''),
(16, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Llevar', '1', 'No Consolidado', ''),
(17, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '1', 'No Consolidado', ''),
(18, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '1', 'No Consolidado', ''),
(19, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '1', 'No Consolidado', ''),
(20, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '1', 'No Consolidado', ''),
(21, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'HAMBURGUESA', '8', 8, 'Mesa', '1', 'No Consolidado', ''),
(22, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'ALMUERZO COMPLETO', '10', 10, 'Llevar', '1', 'No Consolidado', ''),
(23, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'ALMUERZO COMPLETO', '10', 10, 'Mesa', '1', 'No Consolidado', ''),
(24, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '1', 'No Consolidado', ''),
(25, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'SOPA DE MANI  GRANDE', '7', 7, 'Mesa', '1', 'No Consolidado', ''),
(26, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'PIPOCA DE POLLO', '12', 12, 'Mesa', '1', 'No Consolidado', ''),
(27, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'SOPA DE MANI  GRANDE', '7', 7, 'Mesa', '1', 'No Consolidado', ''),
(28, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'Hueso', '1', 1, 'Mesa', '1', 'No Consolidado', ''),
(29, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '1', 'No Consolidado', ''),
(30, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 1, 'SOPA DE MANI  GRANDE', '7', 7, 'Mesa', '1', 'No Consolidado', ''),
(31, '2020-04-25', '52-E9-EB-D4', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '1', 'No Consolidado', ''),
(32, '2020-04-25', '92-5E-17-65-18', 'Henry Calani', 1, 'SILLPANCHO', '13', 13, 'Mesa', '1', 'No Consolidado', ''),
(33, '2020-04-25', '92-5E-17-65-18', 'Henry Calani', 1, 'MEDIO POLLO', '30', 30, 'Mesa', '1', 'No Consolidado', ''),
(34, '2020-04-25', '52-E9-EB-D4', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '1', 'No Consolidado', ''),
(35, '2020-04-25', '22-F7-81-68-57', 'Henry Calani', 1, 'SOPA DE MANI MEDIANA', '4', 4, 'Mesa', '1', 'No Consolidado', ''),
(36, '2020-04-25', '59-AC-1B-EC', 'Henry Calani', 2, 'ECONOMICO', '16', 32, 'Llevar', '44', 'No Consolidado', ''),
(37, '2020-04-25', '59-AC-1B-EC', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '44', 'No Consolidado', ''),
(38, '2020-04-25', '66-3D-8E-9A', 'Henry Calani', 2, 'HAMBURGUESA', '16', 32, 'Llevar', '44', 'No Consolidado', ''),
(39, '2020-04-25', '52-E9-EB-D4', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Llevar', '44', 'No Consolidado', ''),
(40, '2020-04-25', '52-E9-EB-D4', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '44', 'No Consolidado', ''),
(41, '2020-04-25', '6F-1F-17-13', 'Henry Calani', 1, 'SILLPANCHO', '13', 13, 'Mesa', '44', 'No Consolidado', ''),
(42, '2020-04-25', '52-E9-EB-D4', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Llevar', '44', 'No Consolidado', ''),
(43, '2020-04-25', '52-E9-EB-D4', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Llevar', '44', 'No Consolidado', ''),
(44, '2020-04-25', '52-E9-EB-D4', 'Henry Calani', 1, 'HAMBURGUESA', '8', 8, 'Llevar', '44', 'No Consolidado', ''),
(45, '2020-04-26', 'EE-EF-79-C7-17', 'Henry Calani', 1, 'LIMITO', '13', 13, 'Mesa', '44', 'No Consolidado', ''),
(46, '2020-04-29', '83-97-25-C8', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '44', 'No Consolidado', ''),
(47, '2020-04-29', '4E-3A-F1-77-AD', 'Henry Calani', 1, 'SILLPANCHO', '13', 13, 'Mesa', '44', 'No Consolidado', ''),
(48, '2020-04-29', '83-97-25-C8', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '44', 'No Consolidado', ''),
(49, '2020-04-29', '83-97-25-C8', 'Henry Calani', 1, 'SEGUNDO', '8', 8, 'Mesa', '44', 'No Consolidado', ''),
(50, '2020-04-29', '4E-3A-F1-77-AD', 'Henry Calani', 1, 'SILLPANCHO', '13', 13, 'Mesa', '44', 'No Consolidado', ''),
(51, '2020-04-29', '4E-3A-F1-77-AD', 'Henry Calani', 1, 'LIMITO', '13', 13, 'Mesa', '44', 'No Consolidado', ''),
(52, '2020-04-29', '9E-4A-6B-01', 'Henry Calani', 1, 'DUOPOLLO', '16', 16, 'Mesa', '44', 'No Consolidado', ''),
(53, '2020-04-29', '83-97-25-C8', 'Henry Calani', 1, 'SEGUNDO', '8', 8, 'Mesa', '44', 'No Consolidado', ''),
(54, '2020-04-29', 'BB-90-2F-CC', 'Henry Calani', 2, 'LIMITO', '26', 52, 'Mesa', '44', 'No Consolidado', ''),
(55, '2020-04-29', '83-97-25-C8', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '44', 'No Consolidado', ''),
(56, '2020-04-29', '4E-3A-F1-77-AD', 'Henry Calani', 1, 'LIMITO', '13', 13, 'Mesa', '44', 'No Consolidado', ''),
(57, '2020-05-02', 'CF-FF-1E-52', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '44', 'No Consolidado', ''),
(58, '2020-05-02', 'EE-99-52-D5-6E', 'Henry Calani', 1, 'MEDIO POLLO', '30', 30, 'Mesa', '44', 'No Consolidado', ''),
(59, '2020-05-04', 'F6-00-7D-4F-BE', 'Henry Calani', 1, 'PIPOCA DE POLLO', '12', 12, 'Mesa', '44', 'No Consolidado', ''),
(60, '2020-05-04', '7B-CA-B5-B8-FC', 'Henry Calani', 2, 'SILLPANCHO', '26', 52, 'Mesa', '44', 'No Consolidado', ''),
(61, '2020-05-04', '91-5C-4C-14-C5', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '44', 'No Consolidado', ''),
(62, '2020-05-09', '83-1D-07-18', 'Henry Calani', 2, 'ECONOMICO', '16', 32, 'Mesa', '44', 'No Consolidado', ''),
(63, '2020-05-09', '61-B9-AD-A6-C9', 'Henry Calani', 1, 'LIMITO', '13', 13, 'Mesa', '44', 'No Consolidado', ''),
(64, '2020-05-09', '61-B9-AD-A6-C9', 'Henry Calani', 1, 'DUOPOLLO', '16', 16, 'Mesa', '44', 'No Consolidado', ''),
(65, '2020-05-09', '2D-6F-DF-70', 'Henry Calani', 1, 'LIMITO', '13', 13, 'Mesa', '44', 'No Consolidado', ''),
(66, '2020-05-09', '2A-B1-D0-38-67', 'Henry Calani', 1, 'ECONOMICO', '8', 8, 'Mesa', '44', 'No Consolidado', ''),
(67, '2020-05-09', '2D-6F-DF-70', 'Henry Calani', 1, 'LIMITO', '13', 13, 'Mesa', '44', 'No Consolidado', ''),
(68, '2020-05-09', '83-1D-07-18', 'Henry Calani', 1, 'DUOPOLLO', '16', 16, 'Mesa', '44', 'No Consolidado', ''),
(69, '2020-05-09', '2A-B1-D0-38-67', 'Henry Calani', 1, 'SEGUNDO', '8', 8, 'Mesa', '44', 'No Consolidado', ''),
(70, '2020-05-09', '83-1D-07-18', 'Henry Calani', 1, 'DUOPOLLO', '16', 16, 'Mesa', '44', 'No Consolidado', ''),
(71, '2020-05-09', '1B-6B-5A-5A-1F', 'Henry Calani', 1, 'SOPA DE MANI  GRANDE', '7', 7, 'Mesa', '44', 'No Consolidado', ''),
(72, '2020-05-09', '1B-6B-5A-5A-1F', 'Henry Calani', 1, 'SILLPANCHO', '13', 13, 'Llevar', '44', 'No Consolidado', ''),
(73, '2020-05-09', '1B-6B-5A-5A-1F', 'Henry Calani', 1, 'SEGUNDO', '8', 8, 'Mesa', '44', 'No Consolidado', ''),
(74, '2020-05-09', '2A-B1-D0-38-67', 'Henry Calani', 1, 'HAMBURGUESA', '8', 8, 'Mesa', '44', 'No Consolidado', ''),
(75, '2020-05-09', 'AE-73-63-E7-18', 'Henry Calani', 1, 'MEDIO POLLO', '30', 30, 'Mesa', '44', 'No Consolidado', ''),
(76, '2020-05-09', '73-C4-3A-7D', 'Henry Calani', 1, 'MEDIO POLLO', '30', 30, 'Mesa', '44', 'No Consolidado', ''),
(77, '2020-05-09', '73-C4-3A-7D', 'Henry Calani', 1, 'SOPA DE MANI MEDIANA', '4', 4, 'Mesa', '44', 'No Consolidado', ''),
(78, '2020-05-09', '2B-AB-97-09-66', 'Henry Calani', 1, 'PIPOCA DE POLLO', '12', 12, 'Mesa', '44', 'No Consolidado', ''),
(79, '2020-05-09', '2D-6F-DF-70', 'Henry Calani', 1, 'SILLPANCHO', '13', 13, 'Mesa', '44', 'No Consolidado', ''),
(80, '2020-05-09', 'A4-8E-54-EA', 'Henry Calani', 1, 'ALMUERZO COMPLETO', '10', 10, 'Mesa', '44', 'No Consolidado', ''),
(81, '2020-05-09', 'A4-8E-54-EA', 'Henry Calani', 1, 'MEDIO POLLO', '30', 30, 'Mesa', '44', 'No Consolidado', ''),
(82, '2020-05-09', '2A-B1-D0-38-67', 'Henry Calani', 1, 'HAMBURGUESA', '8', 8, 'Mesa', '44', 'No Consolidado', ''),
(83, '2020-05-09', '5A-C5-A0-8C-2D', 'Henry Calani', 1, 'LIMITO', '13', 13, 'Mesa', '44', 'No Consolidado', ''),
(84, '2020-05-09', '5A-C5-A0-8C-2D', 'Henry Calani', 1, 'Un Cuarto', '23', 23, 'Llevar', '44', 'No Consolidado', ''),
(85, '2020-05-09', '2D-6F-DF-70', 'Henry Calani', 1, 'LIMITO', '13', 13, 'Mesa', '44', 'No Consolidado', '');

-- --------------------------------------------------------

--
-- Table structure for table `ventasdetalletotal`
--

CREATE TABLE `ventasdetalletotal` (
  `ventasDetalleId` int(11) NOT NULL,
  `fecha` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `codTransaccion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `total` double NOT NULL,
  `usuario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `estadoTransaccion` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `ventasdetalletotal`
--

INSERT INTO `ventasdetalletotal` (`ventasDetalleId`, `fecha`, `codTransaccion`, `nombre`, `total`, `usuario`, `estadoTransaccion`) VALUES
(3, '2020-04-22', 'CD-DE-EE-RR-FF', 'Henry Calani', 81, '1', 'No Consolidado'),
(2, '2020-04-21', 'CD-DE-EE-RR-FF', 'Henry Calani', 81, '1', 'No Consolidado'),
(4, '2020-04-24', 'CD-DE-EE-RR-FF', 'Henry Calani', 20, '1', 'No Consolidado'),
(5, '2020-04-25', 'Array', 'Henry Calani', 16, '1', 'No Consolidado'),
(6, '2020-04-25', 'Array', 'Henry Calani', 42, '1', 'No Consolidado'),
(7, '2020-04-25', 'Array', 'Henry Calani', 8, '1', 'No Consolidado'),
(8, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 8, '1', 'No Consolidado'),
(9, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 16, '1', 'No Consolidado'),
(10, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 20, '1', 'No Consolidado'),
(11, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 15, '1', 'No Consolidado'),
(12, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 20, '1', 'No Consolidado'),
(13, '2020-04-25', 'BF-17-9B-A5', 'Henry Calani', 15, '1', 'No Consolidado'),
(14, '2020-04-25', '52-E9-EB-D4', 'Henry Calani', 8, '1', 'No Consolidado'),
(15, '2020-04-25', '92-5E-17-65-18', 'Henry Calani', 43, '1', 'No Consolidado'),
(16, '2020-04-25', '52-E9-EB-D4', 'Henry Calani', 8, '1', 'No Consolidado'),
(17, '2020-04-25', '22-F7-81-68-57', 'Henry Calani', 4, '1', 'No Consolidado'),
(18, '2020-04-25', '59-AC-1B-EC', 'Henry Calani', 24, '44', 'No Consolidado'),
(19, '2020-04-25', '66-3D-8E-9A', 'Henry Calani', 16, '44', 'No Consolidado'),
(20, '2020-04-25', '52-E9-EB-D4', 'Henry Calani', 8, '44', 'No Consolidado'),
(21, '2020-04-25', '52-E9-EB-D4', 'Henry Calani', 8, '44', 'No Consolidado'),
(22, '2020-04-25', '6F-1F-17-13', 'Henry Calani', 13, '44', 'No Consolidado'),
(23, '2020-04-25', '52-E9-EB-D4', 'Henry Calani', 8, '44', 'No Consolidado'),
(24, '2020-04-25', '52-E9-EB-D4', 'Henry Calani', 8, '44', 'No Consolidado'),
(25, '2020-04-25', '52-E9-EB-D4', 'Henry Calani', 8, '44', 'No Consolidado'),
(26, '2020-04-26', 'EE-EF-79-C7-17', 'Henry Calani', 13, '44', 'No Consolidado'),
(27, '2020-04-29', '83-97-25-C8', 'Henry Calani', 8, '44', 'No Consolidado'),
(28, '2020-04-29', '4E-3A-F1-77-AD', 'Henry Calani', 13, '44', 'No Consolidado'),
(29, '2020-04-29', '83-97-25-C8', 'Henry Calani', 8, '44', 'No Consolidado'),
(30, '2020-04-29', '83-97-25-C8', 'Henry Calani', 8, '44', 'No Consolidado'),
(31, '2020-04-29', '4E-3A-F1-77-AD', 'Henry Calani', 13, '44', 'No Consolidado'),
(32, '2020-04-29', '4E-3A-F1-77-AD', 'Henry Calani', 13, '44', 'No Consolidado'),
(33, '2020-04-29', '9E-4A-6B-01', 'Henry Calani', 16, '44', 'No Consolidado'),
(34, '2020-04-29', '83-97-25-C8', 'Henry Calani', 8, '44', 'No Consolidado'),
(35, '2020-04-29', 'BB-90-2F-CC', 'Henry Calani', 26, '44', 'No Consolidado'),
(36, '2020-04-29', '83-97-25-C8', 'Henry Calani', 8, '44', 'No Consolidado'),
(37, '2020-04-29', '4E-3A-F1-77-AD', 'Henry Calani', 13, '44', 'No Consolidado'),
(38, '2020-05-02', 'CF-FF-1E-52', 'Henry Calani', 8, '44', 'No Consolidado'),
(39, '2020-05-02', 'EE-99-52-D5-6E', 'Henry Calani', 30, '44', 'No Consolidado'),
(40, '2020-05-04', 'F6-00-7D-4F-BE', 'Henry Calani', 12, '44', 'No Consolidado'),
(41, '2020-05-04', '7B-CA-B5-B8-FC', 'Henry Calani', 26, '44', 'No Consolidado'),
(42, '2020-05-04', '91-5C-4C-14-C5', 'Henry Calani', 8, '44', 'No Consolidado'),
(43, '2020-05-09', '83-1D-07-18', 'Henry Calani', 16, '44', 'No Consolidado'),
(44, '2020-05-09', '61-B9-AD-A6-C9', 'Henry Calani', 29, '44', 'No Consolidado'),
(45, '2020-05-09', '2D-6F-DF-70', 'Henry Calani', 13, '44', 'No Consolidado'),
(46, '2020-05-09', '2A-B1-D0-38-67', 'Henry Calani', 8, '44', 'No Consolidado'),
(47, '2020-05-09', '2D-6F-DF-70', 'Henry Calani', 13, '44', 'No Consolidado'),
(48, '2020-05-09', '83-1D-07-18', 'Henry Calani', 16, '44', 'No Consolidado'),
(49, '2020-05-09', '2A-B1-D0-38-67', 'Henry Calani', 8, '44', 'No Consolidado'),
(50, '2020-05-09', '83-1D-07-18', 'Henry Calani', 16, '44', 'No Consolidado'),
(51, '2020-05-09', '1B-6B-5A-5A-1F', 'Henry Calani', 28, '44', 'No Consolidado'),
(52, '2020-05-09', '2A-B1-D0-38-67', 'Henry Calani', 8, '44', 'No Consolidado'),
(53, '2020-05-09', 'AE-73-63-E7-18', 'Henry Calani', 30, '44', 'No Consolidado'),
(54, '2020-05-09', '73-C4-3A-7D', 'Henry Calani', 34, '44', 'No Consolidado'),
(55, '2020-05-09', '2B-AB-97-09-66', 'Henry Calani', 12, '44', 'No Consolidado'),
(56, '2020-05-09', '2D-6F-DF-70', 'Henry Calani', 13, '44', 'No Consolidado'),
(57, '2020-05-09', 'A4-8E-54-EA', 'Henry Calani', 40, '44', 'No Consolidado'),
(58, '2020-05-09', '2A-B1-D0-38-67', 'Henry Calani', 8, '44', 'No Consolidado'),
(59, '2020-05-09', '5A-C5-A0-8C-2D', 'Henry Calani', 36, '44', 'No Consolidado'),
(60, '2020-05-09', '2D-6F-DF-70', 'Henry Calani', 13, '44', 'No Consolidado');

-- --------------------------------------------------------

--
-- Table structure for table `ventasdia`
--

CREATE TABLE `ventasdia` (
  `idventas` int(11) NOT NULL,
  `producto` varchar(900) NOT NULL,
  `cantidad` double NOT NULL,
  `total` double NOT NULL,
  `cliente` varchar(150) NOT NULL,
  `ci` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `usuario` varchar(150) NOT NULL,
  `idClientei` varchar(150) NOT NULL,
  `precio` double NOT NULL,
  `imagen` varchar(150) NOT NULL,
  `idProducto` varchar(150) NOT NULL,
  `tipoProducto` varchar(150) NOT NULL,
  `idjalea` varchar(150) NOT NULL,
  `tipoVentaEC` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ventasdiac`
--

CREATE TABLE `ventasdiac` (
  `idventas` int(11) NOT NULL,
  `producto` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `cantidad` double NOT NULL,
  `total` double NOT NULL,
  `cliente` varchar(150) NOT NULL,
  `ci` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `usuario` varchar(150) NOT NULL,
  `idClientei` varchar(150) NOT NULL,
  `precio` varchar(100) NOT NULL,
  `imagen` varchar(150) NOT NULL,
  `idProducto` varchar(150) NOT NULL,
  `tipocliente` varchar(150) NOT NULL,
  `idjalea` varchar(150) NOT NULL,
  `tipoVentaEC` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ventasdiac`
--

INSERT INTO `ventasdiac` (`idventas`, `producto`, `cantidad`, `total`, `cliente`, `ci`, `fecha`, `usuario`, `idClientei`, `precio`, `imagen`, `idProducto`, `tipocliente`, `idjalea`, `tipoVentaEC`) VALUES
(1, 'Economico', 3, 24, 'S/N', 0, '2020-01-11 18:00:07', 'USUARIO', '33', '8', 'fotoproducto/imagen_1349563944.jpg', '54', 'POLLO', 'Mesa', 'efectivo'),
(2, 'Economico', 2, 16, 'S/N', 0, '2020-01-11 18:00:07', 'USUARIO', '33', '8', 'fotoproducto/imagen_1349563944.jpg', '54', 'POLLO', 'Llevar', 'efectivo'),
(3, 'Medio Pollos', 1, 25, 'S/N', 0, '2020-02-11 18:00:07', 'USUARIO', '33', '25', 'fotoproducto/imagen_1349563882.jpg', '60', 'POLLO', 'Mesa', 'efectivo'),
(4, 'CabaÃ±ita', 2, 10, 'S/N', 0, '2020-02-11 18:00:07', 'USUARIO', '33', '10', 'fotoproducto/imagen_1349564147.jpg', '55', 'POLLO', 'Mesa', 'efectivo'),
(5, 'Carlita', 1, 12, 'S/N', 0, '2020-03-11 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(6, 'Carlita', 1, 12, 'S/N', 0, '2020-03-11 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Llevar', 'efectivo'),
(7, 'Carlitos', 1, 15, 'S/N', 0, '2020-04-11 18:00:07', 'USUARIO', '33', '15', 'fotoproducto/imagen_1349563882.jpg', '51', 'POLLO', 'Mesa', 'efectivo'),
(8, 'Milanesa', 1, 6, 'S/N', 0, '2020-05-11 18:00:07', 'USUARIO', '33', '6', 'fotoproducto/imagen_1349564195.jpg', '65', 'POLLO', 'Llevar', 'efectivo'),
(9, 'Pollo Entero', 1, 50, 'S/N', 0, '2020-05-11 18:00:07', 'USUARIO', '33', '50', 'fotoproducto/imagen_1349563882.jpg', '59', 'POLLO', 'Mesa', 'efectivo'),
(10, 'Carlita', 1, 12, 'S/N', 0, '2020-06-11 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(11, 'CabaÃ±ita', 1, 10, 'S/N', 0, '2020-06-11 18:00:07', 'USUARIO', '33', '10', 'fotoproducto/imagen_1349564147.jpg', '55', 'POLLO', 'Mesa', 'efectivo'),
(12, 'CabaÃ±ita', 1, 10, 'S/N', 0, '2016-07-11 18:00:07', 'USUARIO', '33', '10', 'fotoproducto/imagen_1349564147.jpg', '55', 'POLLO', 'Llevar', 'efectivo'),
(13, 'Popular Sprite', 1, 5, 'S/N', 0, '2016-07-11 18:00:07', 'USUARIO', '33', '5', 'fotoproducto/imagen_1349564498.jpg', '74', 'REFRESCO', 'Mesa', 'efectivo'),
(14, 'Un Cuarto', 1, 22, 'S/N', 0, '2016-07-11 18:00:07', 'USUARIO', '33', '22', 'fotoproducto/imagen_1349563845.jpg', '50', 'POLLO', 'Llevar', 'efectivo'),
(15, 'Nuggets', 1, 6, 'S/N', 0, '2020-08-11 18:00:07', 'USUARIO', '33', '6', 'fotoproducto/imagen_1349564229.jpg', '57', 'POLLO', 'Mesa', 'efectivo'),
(16, 'Medio Pollos', 1, 25, 'S/N', 0, '2020-08-11 18:00:07', 'USUARIO', '33', '25', 'fotoproducto/imagen_1349563882.jpg', '60', 'POLLO', 'Llevar', 'efectivo'),
(17, 'Carlitos', 1, 15, 'S/N', 0, '2020-08-11 18:00:07', 'USUARIO', '33', '15', 'fotoproducto/imagen_1349563882.jpg', '51', 'POLLO', 'Mesa', 'efectivo'),
(18, 'Economico', 1, 8, 'S/N', 0, '2020-08-11 18:00:07', 'USUARIO', '33', '8', 'fotoproducto/imagen_1349563944.jpg', '54', 'POLLO', 'Mesa', 'efectivo'),
(19, 'Medio Pollos', 1, 25, 'S/N', 0, '2020-09-11 18:00:07', 'USUARIO', '33', '25', 'fotoproducto/imagen_1349563882.jpg', '60', 'POLLO', 'Llevar', 'efectivo'),
(20, 'Carlita', 1, 12, 'S/N', 0, '2020-09-11 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(21, 'Carlita', 1, 12, 'S/N', 0, '2020-10-11 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(22, 'Pollo a la Parrilla', 1, 6, 'S/N', 0, '2020-10-11 18:00:07', 'USUARIO', '33', '6', 'fotoproducto/imagen_1349564103.jpg', '62', 'POLLO', 'Mesa', 'efectivo'),
(23, 'Carlita', 1, 12, 'S/N', 0, '2020-11-11 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(24, 'Medio Pollos', 1, 25, 'S/N', 0, '2020-11-29 18:07:11', 'USUARIO', '33', '25', 'fotoproducto/imagen_1349563882.jpg', '60', 'POLLO', 'Llevar', 'efectivo'),
(25, 'Economico', 1, 8, 'S/N', 0, '2020-12-29 18:07:11', 'USUARIO', '33', '8', 'fotoproducto/imagen_1349563944.jpg', '54', 'POLLO', 'Llevar', 'efectivo'),
(26, 'Carlita', 1, 12, 'S/N', 0, '2020-12-29 18:07:55', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(27, 'Carlitos', 1, 15, 'S/N', 0, '2020-12-29 18:08:11', 'USUARIO', '33', '15', 'fotoproducto/imagen_1349563882.jpg', '51', 'POLLO', 'Mesa', 'efectivo'),
(28, 'Economico', 3, 24, 'S/N', 0, '2020-01-01 18:00:07', 'USUARIO', '33', '8', 'fotoproducto/imagen_1349563944.jpg', '54', 'POLLO', 'Mesa', 'efectivo'),
(29, 'Economico', 2, 16, 'S/N', 0, '2020-01-01 18:00:07', 'USUARIO', '33', '8', 'fotoproducto/imagen_1349563944.jpg', '54', 'POLLO', 'Llevar', 'efectivo'),
(30, 'Medio Pollos', 1, 25, 'S/N', 0, '2020-01-02 18:00:07', 'USUARIO', '33', '25', 'fotoproducto/imagen_1349563882.jpg', '60', 'POLLO', 'Mesa', 'efectivo'),
(31, 'CabaÃ±ita', 1, 10, 'S/N', 0, '2020-01-02 18:00:07', 'USUARIO', '33', '10', 'fotoproducto/imagen_1349564147.jpg', '55', 'POLLO', 'Mesa', 'efectivo'),
(32, 'Carlita', 1, 12, 'S/N', 0, '2020-01-03 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(33, 'Carlita', 1, 12, 'S/N', 0, '2020-01-03 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Llevar', 'efectivo'),
(34, 'Carlitos', 1, 15, 'S/N', 0, '2020-01-04 18:00:07', 'USUARIO', '33', '15', 'fotoproducto/imagen_1349563882.jpg', '51', 'POLLO', 'Mesa', 'efectivo'),
(35, 'Milanesa', 1, 6, 'S/N', 0, '2020-01-05 18:00:07', 'USUARIO', '33', '6', 'fotoproducto/imagen_1349564195.jpg', '65', 'POLLO', 'Llevar', 'efectivo'),
(36, 'Pollo Entero', 1, 50, 'S/N', 0, '2020-02-05 18:00:07', 'USUARIO', '33', '50', 'fotoproducto/imagen_1349563882.jpg', '59', 'POLLO', 'Mesa', 'efectivo'),
(37, 'Carlita', 1, 12, 'S/N', 0, '2020-01-05 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(38, 'CabaÃ±ita', 1, 10, 'S/N', 0, '2020-02-06 18:00:07', 'USUARIO', '33', '10', 'fotoproducto/imagen_1349564147.jpg', '55', 'POLLO', 'Mesa', 'efectivo'),
(39, 'CabaÃ±ita', 1, 10, 'S/N', 0, '2016-01-06 18:00:07', 'USUARIO', '33', '10', 'fotoproducto/imagen_1349564147.jpg', '55', 'POLLO', 'Llevar', 'efectivo'),
(40, 'Popular Sprite', 1, 5, 'S/N', 0, '2016-01-07 18:00:07', 'USUARIO', '33', '5', 'fotoproducto/imagen_1349564498.jpg', '74', 'REFRESCO', 'Mesa', 'efectivo'),
(41, 'Un Cuarto', 1, 22, 'S/N', 0, '2016-01-08 18:00:07', 'USUARIO', '33', '22', 'fotoproducto/imagen_1349563845.jpg', '50', 'POLLO', 'Llevar', 'efectivo'),
(42, 'Nuggets', 1, 6, 'S/N', 0, '2020-01-09 18:00:07', 'USUARIO', '33', '6', 'fotoproducto/imagen_1349564229.jpg', '57', 'POLLO', 'Mesa', 'efectivo'),
(43, 'Medio Pollos', 1, 25, 'S/N', 0, '2020-01-10 18:00:07', 'USUARIO', '33', '25', 'fotoproducto/imagen_1349563882.jpg', '60', 'POLLO', 'Llevar', 'efectivo'),
(44, 'Carlitos', 1, 15, 'S/N', 0, '2020-01-11 18:00:07', 'USUARIO', '33', '15', 'fotoproducto/imagen_1349563882.jpg', '51', 'POLLO', 'Mesa', 'efectivo'),
(45, 'Economico', 1, 8, 'S/N', 0, '2020-01-12 18:00:07', 'USUARIO', '33', '8', 'fotoproducto/imagen_1349563944.jpg', '54', 'POLLO', 'Mesa', 'efectivo'),
(46, 'Medio Pollos', 1, 25, 'S/N', 0, '2020-01-13 18:00:07', 'USUARIO', '33', '25', 'fotoproducto/imagen_1349563882.jpg', '60', 'POLLO', 'Llevar', 'efectivo'),
(47, 'Carlita', 1, 12, 'S/N', 0, '2020-01-14 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(48, 'Carlita', 1, 12, 'S/N', 0, '2020-01-15 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(49, 'Pollo a la Parrilla', 1, 6, 'S/N', 0, '2020-01-16 18:00:07', 'USUARIO', '33', '6', 'fotoproducto/imagen_1349564103.jpg', '62', 'POLLO', 'Mesa', 'efectivo'),
(50, 'Carlita', 1, 12, 'S/N', 0, '2020-01-17 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(51, 'Medio Pollos', 1, 25, 'S/N', 0, '2020-01-18 18:07:11', 'USUARIO', '33', '25', 'fotoproducto/imagen_1349563882.jpg', '60', 'POLLO', 'Llevar', 'efectivo'),
(52, 'Economico', 1, 8, 'S/N', 0, '2020-01-19 18:07:11', 'USUARIO', '33', '8', 'fotoproducto/imagen_1349563944.jpg', '54', 'POLLO', 'Llevar', 'efectivo'),
(53, 'Carlita', 1, 12, 'S/N', 0, '2020-01-20 18:07:55', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(54, 'Economico', 1, 8, 'S/N', 0, '2020-01-21 18:00:07', 'USUARIO', '33', '8', 'fotoproducto/imagen_1349563944.jpg', '54', 'POLLO', 'Mesa', 'efectivo'),
(55, 'Medio Pollos', 1, 25, 'S/N', 0, '2020-01-22 18:00:07', 'USUARIO', '33', '25', 'fotoproducto/imagen_1349563882.jpg', '60', 'POLLO', 'Llevar', 'efectivo'),
(56, 'Carlita', 1, 12, 'S/N', 0, '2020-01-23 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(57, 'Carlita', 1, 12, 'S/N', 0, '2020-01-24 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(58, 'Pollo a la Parrilla', 1, 6, 'S/N', 0, '2020-01-25 18:00:07', 'USUARIO', '33', '6', 'fotoproducto/imagen_1349564103.jpg', '62', 'POLLO', 'Mesa', 'efectivo'),
(59, 'Carlita', 1, 12, 'S/N', 0, '2020-01-26 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(60, 'Medio Pollos', 1, 25, 'S/N', 0, '2020-01-27 18:07:11', 'USUARIO', '33', '25', 'fotoproducto/imagen_1349563882.jpg', '60', 'POLLO', 'Llevar', 'efectivo'),
(61, 'Economico', 1, 8, 'S/N', 0, '2020-01-28 18:07:11', 'USUARIO', '33', '8', 'fotoproducto/imagen_1349563944.jpg', '54', 'POLLO', 'Llevar', 'efectivo'),
(62, 'Carlita', 1, 12, 'S/N', 0, '2020-01-29 18:07:55', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(63, 'Medio Pollos', 1, 25, 'S/N', 0, '2020-01-30 18:07:11', 'USUARIO', '33', '25', 'fotoproducto/imagen_1349563882.jpg', '60', 'POLLO', 'Llevar', 'efectivo'),
(64, 'Economico', 1, 8, 'S/N', 0, '2020-01-30 18:07:11', 'USUARIO', '33', '8', 'fotoproducto/imagen_1349563944.jpg', '54', 'POLLO', 'Llevar', 'efectivo'),
(65, 'Carlita', 1, 12, 'S/N', 0, '2020-02-10 10:00:00', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(66, 'Carlitos', 1, 15, 'S/N', 0, '2020-01-31 18:08:11', 'USUARIO', '33', '15', 'fotoproducto/imagen_1349563882.jpg', '51', 'POLLO', 'Mesa', 'efectivo'),
(67, 'Carlita', 1, 12, 'S/N', 0, '2020-06-23 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(68, 'Medio Pollos', 1, 25, 'S/N', 0, '2020-06-23 18:07:11', 'USUARIO', '33', '25', 'fotoproducto/imagen_1349563882.jpg', '60', 'POLLO', 'Llevar', 'efectivo'),
(69, 'Economico', 1, 8, 'S/N', 0, '2020-06-23 18:07:11', 'USUARIO', '33', '8', 'fotoproducto/imagen_1349563944.jpg', '54', 'POLLO', 'Llevar', 'efectivo'),
(70, 'Carlita', 1, 12, 'S/N', 0, '2020-06-23 18:07:55', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(71, 'Medio Pollos', 1, 25, 'S/N', 0, '2020-06-23 18:07:11', 'USUARIO', '33', '25', 'fotoproducto/imagen_1349563882.jpg', '60', 'POLLO', 'Llevar', 'efectivo'),
(72, 'Economico', 1, 8, 'S/N', 0, '2020-06-23 18:07:11', 'USUARIO', '33', '8', 'fotoproducto/imagen_1349563944.jpg', '54', 'POLLO', 'Llevar', 'efectivo'),
(73, 'Carlita', 1, 12, 'S/N', 0, '2020-06-23 18:07:55', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(74, 'Carlita', 1, 12, 'S/N', 0, '2020-06-23 18:00:07', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(75, 'Medio Pollos', 1, 25, 'S/N', 0, '2020-06-24 18:07:11', 'USUARIO', '33', '25', 'fotoproducto/imagen_1349563882.jpg', '60', 'POLLO', 'Llevar', 'efectivo'),
(76, 'Economico', 1, 8, 'S/N', 0, '2020-06-24 18:07:11', 'USUARIO', '33', '8', 'fotoproducto/imagen_1349563944.jpg', '54', 'POLLO', 'Llevar', 'efectivo'),
(77, 'Carlita', 1, 12, 'S/N', 0, '2020-06-24 18:07:55', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(78, 'Medio Pollos', 1, 25, 'S/N', 0, '2020-06-24 18:07:11', 'USUARIO', '33', '25', 'fotoproducto/imagen_1349563882.jpg', '60', 'POLLO', 'Llevar', 'efectivo'),
(79, 'Economico', 1, 8, 'S/N', 0, '2020-06-24 18:07:11', 'USUARIO', '33', '8', 'fotoproducto/imagen_1349563944.jpg', '54', 'POLLO', 'Llevar', 'efectivo'),
(80, 'Carlita', 1, 12, 'S/N', 0, '2020-06-24 18:07:55', 'USUARIO', '33', '12', 'fotoproducto/imagen_1349563944.jpg', '52', 'POLLO', 'Mesa', 'efectivo'),
(81, 'Carlitos', 1, 15, 'S/N', 0, '2020-06-23 18:08:11', 'USUARIO', '33', '15', 'fotoproducto/imagen_1349563882.jpg', '51', 'POLLO', 'Mesa', 'efectivo');

-- --------------------------------------------------------

--
-- Table structure for table `ventashervido`
--

CREATE TABLE `ventashervido` (
  `idVentasP` int(11) NOT NULL,
  `producto` varchar(150) NOT NULL,
  `cantidad` varchar(150) NOT NULL,
  `costo` varchar(150) NOT NULL,
  `idventas` varchar(150) NOT NULL,
  `fecha` date NOT NULL,
  `tipoVentaEC` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ventaspollo`
--

CREATE TABLE `ventaspollo` (
  `idVentasP` int(11) NOT NULL,
  `producto` varchar(150) NOT NULL,
  `cantidad` varchar(150) NOT NULL,
  `costo` varchar(150) NOT NULL,
  `idventas` varchar(150) NOT NULL,
  `fecha` date NOT NULL,
  `tipoVentaEC` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ventaspollo`
--

INSERT INTO `ventaspollo` (`idVentasP`, `producto`, `cantidad`, `costo`, `idventas`, `fecha`, `tipoVentaEC`) VALUES
(1, 'Economico', '2', '16', '2', '2016-08-27', 'efectivo'),
(2, 'Economico', '3', '24', '1', '2016-08-27', 'efectivo'),
(3, 'CabaÃ±ita', '1', '10', '4', '2016-08-29', 'efectivo'),
(4, 'Medio Pollos', '1', '25', '3', '2016-08-29', 'efectivo'),
(5, 'Carlita', '1', '12', '6', '2016-08-29', 'efectivo'),
(6, 'Carlita', '1', '12', '5', '2016-08-29', 'efectivo'),
(7, 'Carlitos', '1', '15', '7', '2016-08-29', 'efectivo'),
(8, 'Carlita', '1', '12', '10', '2016-08-29', 'efectivo'),
(9, 'Pollo Entero', '1', '50', '9', '2016-08-29', 'efectivo'),
(10, 'Milanesa', '1', '6', '8', '2016-08-29', 'efectivo'),
(11, 'CabaÃ±ita', '1', '10', '12', '2016-08-29', 'efectivo'),
(12, 'CabaÃ±ita', '1', '10', '11', '2016-08-29', 'efectivo'),
(13, 'Nuggets', '1', '6', '15', '2016-08-29', 'efectivo'),
(14, 'Un Cuarto', '1', '22', '14', '2016-08-29', 'efectivo'),
(15, 'Carlitos', '1', '15', '17', '2016-08-29', 'efectivo'),
(16, 'Medio Pollos', '1', '25', '16', '2016-08-29', 'efectivo'),
(17, 'Economico', '1', '8', '18', '2016-08-29', 'efectivo'),
(18, 'Carlita', '1', '12', '20', '2016-08-29', 'efectivo'),
(19, 'Medio Pollos', '1', '25', '19', '2016-08-29', 'efectivo'),
(20, 'Carlita', '1', '12', '21', '2016-08-29', 'efectivo'),
(21, 'Carlita', '1', '12', '23', '2016-08-29', 'efectivo'),
(22, 'Pollo a la Parrilla', '1', '6', '22', '2016-08-29', 'efectivo'),
(23, 'Economico', '1', '8', '25', '2016-08-29', 'efectivo'),
(24, 'Medio Pollos', '1', '25', '24', '2016-08-29', 'efectivo'),
(25, 'Carlita', '1', '12', '26', '2016-08-29', 'efectivo'),
(26, 'Carlitos', '1', '15', '27', '2016-08-29', 'efectivo');

-- --------------------------------------------------------

--
-- Table structure for table `ventasrefresco`
--

CREATE TABLE `ventasrefresco` (
  `idVentasP` int(11) NOT NULL,
  `producto` varchar(150) NOT NULL,
  `cantidad` varchar(150) NOT NULL,
  `costo` varchar(150) NOT NULL,
  `idventas` varchar(150) NOT NULL,
  `fecha` date NOT NULL,
  `tipoVentaEC` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ventasrefresco`
--

INSERT INTO `ventasrefresco` (`idVentasP`, `producto`, `cantidad`, `costo`, `idventas`, `fecha`, `tipoVentaEC`) VALUES
(1, 'Popular Sprite', '1', '5', '13', '2016-08-29', 'efectivo');

-- --------------------------------------------------------

--
-- Table structure for table `yogurtelegido`
--

CREATE TABLE `yogurtelegido` (
  `idyogurt` int(11) NOT NULL,
  `elegido` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `yogurtelegido`
--

INSERT INTO `yogurtelegido` (`idyogurt`, `elegido`, `tipo`) VALUES
(1, '1 ', 'publico'),
(2, '1 ', 'publico'),
(3, '1 ', 'cortesia'),
(4, '1 ', 'publico'),
(5, '1 ', 'publico');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alerta`
--
ALTER TABLE `alerta`
  ADD PRIMARY KEY (`alertaId`);

--
-- Indexes for table `cantidadyogurt`
--
ALTER TABLE `cantidadyogurt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clase`
--
ALTER TABLE `clase`
  ADD PRIMARY KEY (`idclase`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indexes for table `cliente2`
--
ALTER TABLE `cliente2`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indexes for table `clienteb`
--
ALTER TABLE `clienteb`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indexes for table `clientedato`
--
ALTER TABLE `clientedato`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indexes for table `clientetotal`
--
ALTER TABLE `clientetotal`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indexes for table `codigocontrol`
--
ALTER TABLE `codigocontrol`
  ADD PRIMARY KEY (`idcodigo`);

--
-- Indexes for table `codigotransaccion`
--
ALTER TABLE `codigotransaccion`
  ADD PRIMARY KEY (`idCodigo`);

--
-- Indexes for table `confirmarpedido`
--
ALTER TABLE `confirmarpedido`
  ADD PRIMARY KEY (`idConfirmar`);

--
-- Indexes for table `control`
--
ALTER TABLE `control`
  ADD PRIMARY KEY (`idcontrol`);

--
-- Indexes for table `controljalea`
--
ALTER TABLE `controljalea`
  ADD PRIMARY KEY (`idcontrol`);

--
-- Indexes for table `cuentaporpagar`
--
ALTER TABLE `cuentaporpagar`
  ADD PRIMARY KEY (`idPorPagar`);

--
-- Indexes for table `datos`
--
ALTER TABLE `datos`
  ADD PRIMARY KEY (`iddatos`);

--
-- Indexes for table `datosfactura`
--
ALTER TABLE `datosfactura`
  ADD PRIMARY KEY (`datosFacturaId`);

--
-- Indexes for table `datosusuariosesion`
--
ALTER TABLE `datosusuariosesion`
  ADD PRIMARY KEY (`datosUsuarioId`);

--
-- Indexes for table `estadocobro`
--
ALTER TABLE `estadocobro`
  ADD PRIMARY KEY (`idestado`);

--
-- Indexes for table `fechareportes`
--
ALTER TABLE `fechareportes`
  ADD PRIMARY KEY (`idfecha`);

--
-- Indexes for table `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`idgastos`);

--
-- Indexes for table `jalea`
--
ALTER TABLE `jalea`
  ADD PRIMARY KEY (`idjalea`);

--
-- Indexes for table `jalea2`
--
ALTER TABLE `jalea2`
  ADD PRIMARY KEY (`idjalea`);

--
-- Indexes for table `librov`
--
ALTER TABLE `librov`
  ADD PRIMARY KEY (`idlibro`);

--
-- Indexes for table `mensajealerta`
--
ALTER TABLE `mensajealerta`
  ADD PRIMARY KEY (`mensajeAlertaId`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`idmenu`);

--
-- Indexes for table `nota`
--
ALTER TABLE `nota`
  ADD PRIMARY KEY (`idnota`);

--
-- Indexes for table `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`idPedido`);

--
-- Indexes for table `prductofactory`
--
ALTER TABLE `prductofactory`
  ADD PRIMARY KEY (`idproducto`);

--
-- Indexes for table `prductoiogo`
--
ALTER TABLE `prductoiogo`
  ADD PRIMARY KEY (`idproducto`);

--
-- Indexes for table `prductoprueba`
--
ALTER TABLE `prductoprueba`
  ADD PRIMARY KEY (`idproducto`);

--
-- Indexes for table `prductozoe`
--
ALTER TABLE `prductozoe`
  ADD PRIMARY KEY (`idproducto`);

--
-- Indexes for table `preventa`
--
ALTER TABLE `preventa`
  ADD PRIMARY KEY (`idPreventa`);

--
-- Indexes for table `preventa2`
--
ALTER TABLE `preventa2`
  ADD PRIMARY KEY (`idPreventa`);

--
-- Indexes for table `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idproducto`);

--
-- Indexes for table `productocambio`
--
ALTER TABLE `productocambio`
  ADD PRIMARY KEY (`idcambio`);

--
-- Indexes for table `productoe`
--
ALTER TABLE `productoe`
  ADD PRIMARY KEY (`idProductoE`);

--
-- Indexes for table `productoelegido`
--
ALTER TABLE `productoelegido`
  ADD PRIMARY KEY (`idproductoElegido`);

--
-- Indexes for table `profesional`
--
ALTER TABLE `profesional`
  ADD PRIMARY KEY (`ced_prof`);

--
-- Indexes for table `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idproveedor`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`idproducto`);

--
-- Indexes for table `sumatoria`
--
ALTER TABLE `sumatoria`
  ADD PRIMARY KEY (`idsumtoria`);

--
-- Indexes for table `tipocambio`
--
ALTER TABLE `tipocambio`
  ADD PRIMARY KEY (`idcambio`);

--
-- Indexes for table `tipogasto`
--
ALTER TABLE `tipogasto`
  ADD PRIMARY KEY (`idgasto`);

--
-- Indexes for table `tipopago`
--
ALTER TABLE `tipopago`
  ADD PRIMARY KEY (`idtipopago`);

--
-- Indexes for table `tipoproducto`
--
ALTER TABLE `tipoproducto`
  ADD PRIMARY KEY (`idtipoproducto`);

--
-- Indexes for table `tipo_prof`
--
ALTER TABLE `tipo_prof`
  ADD PRIMARY KEY (`cod_prof`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usu`);

--
-- Indexes for table `utlidad`
--
ALTER TABLE `utlidad`
  ADD PRIMARY KEY (`idutilidad`);

--
-- Indexes for table `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`idVentas`);

--
-- Indexes for table `ventasdetalle`
--
ALTER TABLE `ventasdetalle`
  ADD PRIMARY KEY (`ventasDetalleId`);

--
-- Indexes for table `ventasdetalletotal`
--
ALTER TABLE `ventasdetalletotal`
  ADD PRIMARY KEY (`ventasDetalleId`);

--
-- Indexes for table `ventasdia`
--
ALTER TABLE `ventasdia`
  ADD PRIMARY KEY (`idventas`);

--
-- Indexes for table `ventasdiac`
--
ALTER TABLE `ventasdiac`
  ADD PRIMARY KEY (`idventas`);

--
-- Indexes for table `ventashervido`
--
ALTER TABLE `ventashervido`
  ADD PRIMARY KEY (`idVentasP`);

--
-- Indexes for table `ventaspollo`
--
ALTER TABLE `ventaspollo`
  ADD PRIMARY KEY (`idVentasP`);

--
-- Indexes for table `ventasrefresco`
--
ALTER TABLE `ventasrefresco`
  ADD PRIMARY KEY (`idVentasP`);

--
-- Indexes for table `yogurtelegido`
--
ALTER TABLE `yogurtelegido`
  ADD PRIMARY KEY (`idyogurt`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alerta`
--
ALTER TABLE `alerta`
  MODIFY `alertaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cantidadyogurt`
--
ALTER TABLE `cantidadyogurt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `clase`
--
ALTER TABLE `clase`
  MODIFY `idclase` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `cliente2`
--
ALTER TABLE `cliente2`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `clienteb`
--
ALTER TABLE `clienteb`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clientedato`
--
ALTER TABLE `clientedato`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clientetotal`
--
ALTER TABLE `clientetotal`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `codigocontrol`
--
ALTER TABLE `codigocontrol`
  MODIFY `idcodigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `codigotransaccion`
--
ALTER TABLE `codigotransaccion`
  MODIFY `idCodigo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `confirmarpedido`
--
ALTER TABLE `confirmarpedido`
  MODIFY `idConfirmar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `control`
--
ALTER TABLE `control`
  MODIFY `idcontrol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1473;

--
-- AUTO_INCREMENT for table `controljalea`
--
ALTER TABLE `controljalea`
  MODIFY `idcontrol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=836;

--
-- AUTO_INCREMENT for table `cuentaporpagar`
--
ALTER TABLE `cuentaporpagar`
  MODIFY `idPorPagar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `datos`
--
ALTER TABLE `datos`
  MODIFY `iddatos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `datosfactura`
--
ALTER TABLE `datosfactura`
  MODIFY `datosFacturaId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `datosusuariosesion`
--
ALTER TABLE `datosusuariosesion`
  MODIFY `datosUsuarioId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `estadocobro`
--
ALTER TABLE `estadocobro`
  MODIFY `idestado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `fechareportes`
--
ALTER TABLE `fechareportes`
  MODIFY `idfecha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gastos`
--
ALTER TABLE `gastos`
  MODIFY `idgastos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `jalea`
--
ALTER TABLE `jalea`
  MODIFY `idjalea` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jalea2`
--
ALTER TABLE `jalea2`
  MODIFY `idjalea` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `librov`
--
ALTER TABLE `librov`
  MODIFY `idlibro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13637;

--
-- AUTO_INCREMENT for table `mensajealerta`
--
ALTER TABLE `mensajealerta`
  MODIFY `mensajeAlertaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `idmenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `nota`
--
ALTER TABLE `nota`
  MODIFY `idnota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pedido`
--
ALTER TABLE `pedido`
  MODIFY `idPedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `prductofactory`
--
ALTER TABLE `prductofactory`
  MODIFY `idproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `prductoiogo`
--
ALTER TABLE `prductoiogo`
  MODIFY `idproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `prductoprueba`
--
ALTER TABLE `prductoprueba`
  MODIFY `idproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `preventa`
--
ALTER TABLE `preventa`
  MODIFY `idPreventa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `preventa2`
--
ALTER TABLE `preventa2`
  MODIFY `idPreventa` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `producto`
--
ALTER TABLE `producto`
  MODIFY `idproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT for table `productocambio`
--
ALTER TABLE `productocambio`
  MODIFY `idcambio` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productoe`
--
ALTER TABLE `productoe`
  MODIFY `idProductoE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=187;

--
-- AUTO_INCREMENT for table `productoelegido`
--
ALTER TABLE `productoelegido`
  MODIFY `idproductoElegido` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idproveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `idproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `sumatoria`
--
ALTER TABLE `sumatoria`
  MODIFY `idsumtoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tipocambio`
--
ALTER TABLE `tipocambio`
  MODIFY `idcambio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tipogasto`
--
ALTER TABLE `tipogasto`
  MODIFY `idgasto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tipopago`
--
ALTER TABLE `tipopago`
  MODIFY `idtipopago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tipoproducto`
--
ALTER TABLE `tipoproducto`
  MODIFY `idtipoproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `utlidad`
--
ALTER TABLE `utlidad`
  MODIFY `idutilidad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ventas`
--
ALTER TABLE `ventas`
  MODIFY `idVentas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ventasdetalle`
--
ALTER TABLE `ventasdetalle`
  MODIFY `ventasDetalleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `ventasdetalletotal`
--
ALTER TABLE `ventasdetalletotal`
  MODIFY `ventasDetalleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `ventasdia`
--
ALTER TABLE `ventasdia`
  MODIFY `idventas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ventasdiac`
--
ALTER TABLE `ventasdiac`
  MODIFY `idventas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `ventashervido`
--
ALTER TABLE `ventashervido`
  MODIFY `idVentasP` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ventaspollo`
--
ALTER TABLE `ventaspollo`
  MODIFY `idVentasP` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `ventasrefresco`
--
ALTER TABLE `ventasrefresco`
  MODIFY `idVentasP` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `yogurtelegido`
--
ALTER TABLE `yogurtelegido`
  MODIFY `idyogurt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
