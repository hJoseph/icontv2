<?php
require('../Model/conexion.php');

if (!isset($_SESSION)) {
    session_start();
}
$userId=$_GET['userId'];
$con = new conexion();
$userRegister = $con->getUserById($userId);

foreach ($userRegister as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $foto = $user['foto'];
}

if ($tipo == 'ADMINISTRADOR') {

    $getUserSession = $con->getUserSession();

    foreach ($getUserSession as $userSession) {
        $usuario= $userSession['usuario'];
        $password = $userSession['password'];
    }


    $tipo = $tipo;
    $showStatus = $con->getStatus();
    foreach ($showStatus as $status) {
        $estado = $status['estado'];
    }

    $menuMain = $con->getMenu();
    $_SESSION['id_usuario'] = $id_usuario;
    $_SESSION['nombres'] = $nombres;

    $cantidadActual=$_GET["cantidad"];
    $idproducto=$_GET["idproducto"];
    $productoUpdate=$_GET["producto"];
    $imagen=$_GET["imagen"];
    $precio=$_GET["precio"];
    $pventa=$_GET["pventa"];
    $tipoPedido=$_GET["tipoPedido"];
    $userId=$_GET["userId"];


    $getCantidad = $con->getCantidad($idproducto,$tipoPedido);

    foreach ($getCantidad as $getCantidadProducto) {
        $cantidadAnterior=$getCantidadProducto['cantidad'];
    }

    if($cantidadActual > $cantidadAnterior){

        $cantidadActualizada=$cantidadActual-$cantidadAnterior;
        for ( $i = 0; $i <$cantidadActualizada; $i++ ){
            $insertNewPreventa = $con->insertNewPreventa($imagen,$productoUpdate,$precio,$idproducto,$pventa,$userId,$tipoPedido);
        }
    }


   if(($cantidadActual < $cantidadAnterior) and ($cantidadActual!=1)){

       $cantidadActualizada=$cantidadAnterior-($cantidadAnterior-$cantidadActual);
        $deleteCantidadActual = $con->deleteCantidadActual($idproducto,$tipoPedido);
        for ( $i = 0; $i <$cantidadActualizada; $i++ ){
            $insertNewPreventa = $con->insertNewPreventa($imagen,$productoUpdate,$precio,$idproducto,$pventa,$userId,$tipoPedido);
        }
    }

    if(($cantidadActual==1)){

        $deleteCantidadActual = $con->deleteCantidadActual($idproducto,$tipoPedido);
        for ( $i = 0; $i <$cantidadActual; $i++ ){
            $insertNewPreventa = $con->insertNewPreventa($imagen,$productoUpdate,$precio,$idproducto,$pventa,$userId,$tipoPedido);
        }
    }

    $showAllProduct = $con->getProduct();

    $showPreVenta = $con->getPreVenta();

    $totalPreVenta = $con->getTotalPreVenta();

    header("Location: Ventas.php?usuario=$usuario&password=$password");

}


?>