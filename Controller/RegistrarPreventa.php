<?php
require('../Model/conexion.php');
require_once('codigo_control.class.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuario = $_POST['usuario'];
$contrasena = $_POST['password'];

$totalApagar=$_POST['ingreso1'];
$efectivo=$_POST['ingreso2'];
$cambio=$_POST['resultado'];

$nitci=$_POST['ci'];
$fecha=date("Y-m-d H:i:s");

$con = new conexion();


$registerClienteData= $con->registerDatosCliente($usuario,$nitci,$fecha,$totalApagar,$efectivo,$cambio,'1','Local');


$userPreventa = $con->getUserPreventa($nitci);
foreach ($userPreventa as $userData) {
    $nitci = $userData['ci'];
    $idClientei = $userData['idcliente'];
    $nombre = $userData['nombre'];
}

if (isset($nitci)) {
    $registerPreventa = $con->registerPreventa($nombre, $nitci, $fecha, $totalApagar, $efectivo, $cambio, $idClientei);
}

if (!isset($nitci)) {

    $registerPreventa  =$con->registerNewCi($nombre, $nitci);

    $userPreventa = $con->getUserPreventa($nitci);
    foreach ($userPreventa as $userData) {
        $nitci = $userData['ci'];
        $idClientei = $userData['idcliente'];
        $nombre = $userData['nombre'];
    }
    $registerPreventa = $con->registerPreventa($nombre, $nitci, $fecha, $totalApagar, $efectivo, $cambio, $idClientei);
}


$userRegister = $con->getUser($usuario, $contrasena);

foreach ($userRegister as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $foto = $user['foto'];
}

$dataFactura = $con->getDataFactura();
foreach ($dataFactura as $factura) {
    $propietarioA=$factura['propietario'];
    $razonA=$factura['razon'];
    $direccionA=$factura['direccion'];
    $nroA=$factura['nro'];
    $telefonoA=$factura['telefono'];
}


$dataCodigoControl = $con->getDataCodigoControl();
foreach ($dataCodigoControl as $codigoControl) {
    $nitNegocioA=$codigoControl['nit'];
    $autorizacionA=$codigoControl['autorizacion'];
    $facturaA=$codigoControl['factura'];
    $llaveA=$codigoControl['llave'];
    $fechaLA=$codigoControl['fechaL'];
}

$dataLibroVenta = $con->getDataLibroDeVenta();
foreach ($dataLibroVenta as $libro) {
    $facturalv=$libro['factura'];
}


$dataClienteFactura= $con->getClientDataFactura();
$numfilas = mysqli_num_rows($dataClienteFactura);



$codigoControlFactura = $con->getDataCodigoControl();
foreach ($codigoControlFactura as $dataCodigoControlFactura) {
    $autorizacion=$dataCodigoControlFactura['autorizacion'];
    $factura=$dataCodigoControlFactura['factura'];
    $llave=$dataCodigoControlFactura['llave'];
    $nit=$dataCodigoControlFactura['nit'];
    $fechaL=$dataCodigoControlFactura['fechaL'];
}


$ventaTotalForFactura= $con->getVentaTotalForFactura();
foreach ($ventaTotalForFactura as $ventaTotal) {
    $atendido=$ventaTotal['atendido'];
}
$fechaa=date("Ymd");

$pedidoTotalForFactura= $con->getPedidoTotalForFactura();
$pedido = mysqli_num_rows($pedidoTotalForFactura);


$ventaTotalPedido= $con->getTotalVentaForFactura();
foreach ($ventaTotalPedido as $ventaPreventaTotal) {
    $monto=$ventaPreventaTotal['total'];
}
$totalNroVentaPedido = mysqli_num_rows($ventaTotalPedido);


$ventasTotalDiaCurrent= $con->getVentasDiaCurrent();
$totalNroVentaPedidoDia = mysqli_num_rows($ventasTotalDiaCurrent);


$codigoControl = new CodigoControl($autorizacion, $factura, $nit, $fechaa, $monto, $llave);
$codigo = $codigoControl->generar();
$registerCodigoControl = $con->registerCodigoControl($codigo);

$codTransaccion=$con->getCodigoControl();
foreach ($codTransaccion as $codigoControlValue) {
    $codigoControl=$codigoControlValue['codigoTransaccion'];
}

$registerDatosFactura = $con->registerDatosFactura($nitci, $cambio, $efectivo, $totalApagar, $usuario, $contrasena);
$dataMoneda = $con->getMoneda();
$menuMain = $con->getMenu();

require('../Views/ShowFactura.php');

?>