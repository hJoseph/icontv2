<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuario = $_GET['usuarioLogin'];
$contrasena = $_GET['passwordLogin'];


$con = new conexion();
$userRegister = $con->getUser($usuario, $contrasena);

foreach ($userRegister as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $usuario = $user['login'];
    $password = $user['password'];
    $foto = $user['foto'];
}



$menuMain = $con->getMenu();

if(isset($_GET['reporte_dia'])){

    $fechaVentas = $_GET['fechaVentas'];
    $fechaVentasI = $fechaVentas . ' ' . '06:00:00';

    date_default_timezone_set("America/Caracas");
    $tiempo = getdate(time());
    $fecha = date_create($fechaVentas);
    date_add($fecha, date_interval_create_from_date_string('1 days'));
    $fechaVentasU = date_format($fecha, 'Y-m-d');

    $fechaVentasF = $fechaVentasU . ' ' . '04:00:00';


    $ventasByDia = $con->getVentasByDia($fechaVentasI,$fechaVentasF);
    $ventasTotalByDia = $con->getVentasTotalByDia($fechaVentasI,$fechaVentasF);

    require('../Views/ReporteVentasPorDia.php');
}

if(isset($_GET['rango_fecha'])){

    $fechaInicial = $_GET['fechaInicialVentas'];
    $fechaInicialVentas =  $fechaInicial.' '. '06:00:00';
    $fechaFinal = $_GET['fechaFinalVentas'];

    date_default_timezone_set("America/Caracas" ) ;
    $tiempo = getdate(time());
    $fecha = date_create($fechaFinal);
    date_add($fecha, date_interval_create_from_date_string('1 days'));
    $fechaVentasU = date_format($fecha, 'Y-m-d');

    $fechaFinalVentas = $fechaVentasU.' '. '04:00:00';


    $ventasByDia = $con->getVentasByDia($fechaInicialVentas,$fechaFinalVentas);
    $ventasTotalByDia = $con->getVentasTotalByDia($fechaInicialVentas,$fechaFinalVentas);

    require('../Views/ReporteVentasPorDia.php');
}

if(isset($_GET['reporte_producto'])){

    $fechaInicial = $_GET['fechaInicialVentas'];
    $fechaInicialVentas =  $fechaInicial.' '. '06:00:00';
    $fechaFinal = $_GET['fechaFinalVentas'];

    date_default_timezone_set("America/Caracas" ) ;
    $tiempo = getdate(time());
    $fecha = date_create($fechaFinal);
    date_add($fecha, date_interval_create_from_date_string('1 days'));
    $fechaVentasU = date_format($fecha, 'Y-m-d');

    $fechaFinalVentas = $fechaVentasU.' '. '04:00:00';

    $ventasByDia = $con->getVentasByDia($fechaInicialVentas,$fechaFinalVentas);
    $ventasTotalByDia = $con->getVentasTotalByDia($fechaInicialVentas,$fechaFinalVentas);

    require('../Views/ReporteVentasPorProducto.php');
}

if(isset($_GET['gastos'])){

    $fechaInicial = $_GET['fechaInicialVentas'];
    $fechaInicialVentas =  $fechaInicial.' '. '06:00:00';
    $fechaFinal = $_GET['fechaFinalVentas'];

    date_default_timezone_set("America/Caracas" ) ;
    $tiempo = getdate(time());
    $fecha = date_create($fechaFinal);
    date_add($fecha, date_interval_create_from_date_string('1 days'));
    $fechaVentasU = date_format($fecha, 'Y-m-d');

    $fechaFinalVentas = $fechaVentasU.' '. '04:00:00';

    $gastosEmpresa = $con->getGatosDeLaEmpresa($fechaInicialVentas,$fechaFinalVentas);
    $gastosTotales = $con->getTotalGatosDeLaEmpresa($fechaInicialVentas,$fechaFinalVentas);
    $entradaTotal = $con->getEntradasDeLaEmpresa($fechaInicialVentas,$fechaFinalVentas);

    require('../Views/ReporteGastosDeLaEmpresa.php');
}

if(isset($_GET['utilidad'])){

    $fechaInicial = $_GET['fechaInicialVentas'];
    $fechaInicialVentas =  $fechaInicial.' '. '06:00:00';
    $fechaFinal = $_GET['fechaFinalVentas'];

    date_default_timezone_set("America/Caracas" ) ;
    $tiempo = getdate(time());
    $fecha = date_create($fechaFinal);
    date_add($fecha, date_interval_create_from_date_string('1 days'));
    $fechaVentasU = date_format($fecha, 'Y-m-d');

    $fechaFinalVentas = $fechaVentasU.' '. '04:00:00';

    $gastosEmpresa = $con->getGatosDeLaEmpresa($fechaInicialVentas,$fechaFinalVentas);
    $gastosTotales = $con->getTotalGatosDeLaEmpresa($fechaInicialVentas,$fechaFinalVentas);
    $entradaTotal = $con->getEntradasDeLaEmpresa($fechaInicialVentas,$fechaFinalVentas);
    $utilidad = $con->getUtilidadDeLaEmpresa($fechaInicialVentas,$fechaFinalVentas);
    require('../Views/UtilidadView.php');
}

if(isset($_GET['reporte_mes'])){

    $anio = $_GET['anio'];
    $mes = $_GET['mes'];

    $menuMain = $con->getMenu();
    $ventasMensuales = $con->getVentasMensuales();
    $sumVentasByMes = $con->getSumaTotalVentasByMes($mes,$anio);
    $totalVentasMensual = $con->getTotalVentasByMes($mes,$anio);

    require('../Views/ReporteVentasPorMes.php');
}

if(isset($_GET['reporte_anual'])){

    $anio = $_GET['anio'];


    $menuMain = $con->getMenu();
    $ventasMensuales = $con->getVentasMensuales();
    $sumVentasByMes = $con->getTotalVentasByYear($anio);
    $totalVentasMensual = $con->getTotalVentasByAnio($anio);

    require('../Views/ReporteVentasPorAnio.php');
}

if(isset($_GET['reporte_6meses'])){

    $menuMain = $con->getMenu();
    $ventasMensuales = $con->getVentasMensuales();
    $VentasByMes = $con->getTotalVentas6Meses();
    $totalVentasMensual = $con->getGrandTotalVentas6Meses();

    require('../Views/ReporteVentasPor6Meses.php');
}

?>