<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];

$con = new conexion();

$allUsuarios = $con->getAllUsersData();
$menuMain = $con->getMenu();

if(isset($_POST['a_nuevo'])){
    $usuario=$_POST['login'];
    $tipo=$_POST['tipo'];
    $nombre=$_POST['nombre'];
    $password=$_POST['password'];
    //$destino ='fotoUsuario/user.png';

    $mensaje="Se Añadio un nuevo Usuario";
    $alerta="alert alert-success";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);


    if($_FILES['userfile']['name']!=""){

        $ruta = "fotoproducto/";
        opendir($ruta);
        $destino = $ruta.$_FILES['userfile']['name'];


        $nombre_archivo = "C:/xampp/htdocs/icontv2/Views/fotoproducto/" . $_FILES['userfile']['name'];
        $tipo_archivo = $_FILES['userfile']['type'];
        $tamano_archivo = $_FILES['userfile']['size'];


        $nuevo_archivo= "fotoproducto/" . substr($tipo_archivo,6,4);


        if (!((strpos($tipo_archivo, "gif") || strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo < 5000000))) {
            cuadro_error("La extensión o el tamaño de los archivos no es correcta, Se permiten archivos .gif o .jpg de 5 Mb máximo");

        }else{
            if (move_uploaded_file($_FILES['userfile']['tmp_name'], $nombre_archivo)){
                rename($nombre_archivo,$nuevo_archivo);
                //  cuadro_mensaje("El archivo ha sido cargado correctamente");
            }else{
                cuadro_error("Ocurrió algún error al subir el archivo. No pudo guardarse");
            }
        }
    }

    else{
        $destino = "fotoUsuario/user.png";
    }


    $registerNewUser = $con->registerNewUser($nombre, $tipo, $usuario, $password, $destino);

}

if(isset($_GET['idborrar'])){
    $idUsuario=$_GET['idborrar'];
    $usuarioLogin=$_GET['usuarioLogin'];
    $passwordLogin=$_GET['passwordLogin'];

    $mensaje="Se Elimino un Usuario";
    $alerta="alert alert-danger";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $deleteUser = $con->deleteUsuario($idUsuario);

}

if(isset($_POST['update_data'])){
    $idUsuarioData=$_POST['idUsuario'];
    $login=$_POST['login'];
    $tipo=$_POST['tipo'];
    $nombre=$_POST['nombre'];
    $password=$_POST['password'];
    $imagen = $_POST['imagen'];

    $usuarioLogin=$_POST['usuarioLogin'];
    $passwordLogin=$_POST['passwordLogin'];

    $mensaje="Se Actualizo los datos del Usuario";
    $alerta="alert alert-info";

    if($_FILES['userfileEdit']['name']!=""){

        $ruta = "fotoproducto/";
        opendir($ruta);
        $destino = $ruta.$_FILES['userfileEdit']['name'];


        $nombre_archivo = "C:/xampp/htdocs/icontv2/Views/fotoproducto/" . $_FILES['userfileEdit']['name'];
        $tipo_archivo = $_FILES['userfileEdit']['type'];
        $tamano_archivo = $_FILES['userfileEdit']['size'];


        $nuevo_archivo= "fotoproducto/" . substr($tipo_archivo,6,4);


        if (!((strpos($tipo_archivo, "gif") || strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo < 5000000))) {
            cuadro_error("La extensión o el tamaño de los archivos no es correcta, Se permiten archivos .gif o .jpg de 5 Mb máximo");

        }else{
            if (move_uploaded_file($_FILES['userfileEdit']['tmp_name'], $nombre_archivo)){
                rename($nombre_archivo,$nuevo_archivo);
            }else{
                cuadro_error("Ocurrió algún error al subir el archivo. No pudo guardarse");
            }
        }
    }

    else{
        $destino = $imagen;
    }



    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $updateUser = $con->updateUsuario($idUsuarioData, $nombre, $tipo, $login, $password,$destino);

}

header("Location: Usuario.php?usuario=$usuarioLogin&password=$passwordLogin&estado='Activo'");

?>