<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];

$con = new conexion();

$allUsuarios = $con->getAllUsersData();
$menuMain = $con->getMenu();

if (isset($_POST['nuevo_Cuenta'])) {
    $tipo = $_POST['tipo'];
    $descripcion = $_POST['descripcion'];
    $total = $_POST['total'];
    $fechaRegistro = $_POST['fechaRegistro'];

    $mensaje = "Se creo un nuevo registro en la hoja de cuenta correctamente !!";
    $alerta = "alert alert-success";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    if ($tipo=="Entrada") {
        $newAccount = $con->registerNewAccount("E", $descripcion, $total, $fechaRegistro, $usuarioLogin,0);
    }
    else {
        $newAccount = $con->registerNewAccount("S", $descripcion, 0, $fechaRegistro, $usuarioLogin,$total);
    }
}

if (isset($_GET['idborrar'])) {
    $idAccount= $_GET['idborrar'];
    $usuarioLogin = $_GET['usuarioLogin'];
    $passwordLogin = $_GET['passwordLogin'];

    $mensaje = "Se Elimino un la hoja de Cuenta correctamente !!";
    $alerta = "alert alert-danger";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $deleteAccount= $con->deleteAccount($idAccount);

}

if (isset($_POST['update'])) {
    $idgastos = $_POST['idgastos'];
    $tipo = $_POST['tipo'];
    $descripcion = $_POST['descripcion'];
    $total = $_POST['total'];
    $fechaRegistro = $_POST['fechaRegistro'];

    $mensaje = "Se Actualizo la hoja de cuenta correctamente !!";
    $alerta="alert alert-info";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);


    if ($tipo=="Entrada") {
        $newAccount = $con->updateAccount("E", $descripcion, $total, $fechaRegistro, $usuarioLogin,0,$idgastos);
    }
    else {
        $newAccount = $con->updateAccount("S", $descripcion, 0, $fechaRegistro, $usuarioLogin,$total,$idgastos);
    }

}

header("Location: Cuenta.php?usuario=$usuarioLogin&password=$passwordLogin&estado=Activo");

?>