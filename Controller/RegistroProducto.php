<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];

$con = new conexion();

$allUsuarios = $con->getAllUsersData();
$menuMain = $con->getMenu();

if (isset($_POST['nuevo_Producto'])) {
    $codigo = $_POST['codigo'];
    $nombreProducto = $_POST['descripcion'];
    $cantidad = null;
    $fechaRegistro = date('Y-m-d');
    $cantidad = $_POST['cantidad'];
    $precioCompra = $_POST['pcompra'];
    $tipoproducto = $_POST['tipoproducto'];
    $proveedor = null;
    $precioVenta = $_POST['pventa'];

    $mensaje = "Se Añadio un nuevo producto";
    $alerta = "alert alert-success";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    if($_FILES['userfile']['name']!=""){

        $ruta = "fotoproducto/";
        opendir($ruta);
        $destino = $ruta.$_FILES['userfile']['name'];


        $nombre_archivo = "C:/xampp/htdocs/icontv2/Views/fotoproducto/" . $_FILES['userfile']['name'];
        $tipo_archivo = $_FILES['userfile']['type'];
        $tamano_archivo = $_FILES['userfile']['size'];


        $nuevo_archivo= "fotoproducto/" . substr($tipo_archivo,6,4);


        if (!((strpos($tipo_archivo, "gif") || strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo < 5000000))) {
            cuadro_error("La extensión o el tamaño de los archivos no es correcta, Se permiten archivos .gif o .jpg de 5 Mb máximo");

        }else{
            if (move_uploaded_file($_FILES['userfile']['tmp_name'], $nombre_archivo)){
                 rename($nombre_archivo,$nuevo_archivo);
                //  cuadro_mensaje("El archivo ha sido cargado correctamente");
            }else{
                cuadro_error("Ocurrió algún error al subir el archivo. No pudo guardarse");
            }
        }
    }

    else{
        $destino = "fotoproducto/NoPicture.jpg";
    }

    $registerNewProducto = $con->registerNewProducto($destino, $codigo, $nombreProducto, $cantidad, $fechaRegistro, $precioVenta, $proveedor,$tipoproducto, $precioCompra);

}

if (isset($_GET['idborrar'])) {
    $idProducto = $_GET['idborrar'];
    $usuarioLogin = $_GET['usuarioLogin'];
    $passwordLogin = $_GET['passwordLogin'];

    $mensaje = "Se Elimino un Producto";
    $alerta = "alert alert-danger";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $deleteProducto= $con->deleteProducto($idProducto);

}

if (isset($_POST['update_producto'])) {
    $imagen = $_POST['imagen'];
    $idproducto = $_POST['idproducto'];
    $codigo = $_POST['codigo'];
    $nombreProducto = $_POST['descripcion'];
    $cantidad = $_POST['cantidad'];
    $fechaRegistro = date('Y-m-d');
    $proveedor = null;
    $precioCompra = $_POST['pcompra'];
    $tipoproducto = $_POST['tipoproducto'];

    $precioVenta = $_POST['pventa'];

    $mensaje = "Se Edito el producto";
    $alerta="alert alert-info";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    if($_FILES['userfileEdit']['name']!=""){

        $ruta = "fotoproducto/";
        opendir($ruta);
        $destino = $ruta.$_FILES['userfileEdit']['name'];


        $nombre_archivo = "C:/xampp/htdocs/icontv2/Views/fotoproducto/" . $_FILES['userfileEdit']['name'];
        $tipo_archivo = $_FILES['userfileEdit']['type'];
        $tamano_archivo = $_FILES['userfileEdit']['size'];


        $nuevo_archivo= "fotoproducto/" . substr($tipo_archivo,6,4);


        if (!((strpos($tipo_archivo, "gif") || strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo < 5000000))) {
            cuadro_error("La extensión o el tamaño de los archivos no es correcta, Se permiten archivos .gif o .jpg de 5 Mb máximo");

        }else{
            if (move_uploaded_file($_FILES['userfileEdit']['tmp_name'], $nombre_archivo)){
                rename($nombre_archivo,$nuevo_archivo);
            }else{
                cuadro_error("Ocurrió algún error al subir el archivo. No pudo guardarse");
            }
        }
    }

    else{
        $destino = $imagen;
    }

    echo "UPDATE `producto` SET `codigo` = '$codigo', 
                                                   `nombreProducto` = '$nombreProducto', 
                                                   `fechaRegistro` = '$fechaRegistro', 
                                                   `precioVenta` = '$precioVenta', 
                                                   `proveedor` = '$proveedor', 
                                                   `cantidad` = '$cantidad', 
                                                   `tipo` = '$tipoproducto', 
                                                   `precioCompra` = '$precioCompra',
                                                   `imagen` = '$destino'
                                                   WHERE `idproducto` = $idProducto";

    $updateProducto = $con->updateProducto($destino, $codigo, $nombreProducto, $cantidad, $fechaRegistro, $precioVenta, $proveedor,$tipoproducto, $precioCompra,$idproducto);

}

header("Location: Producto.php?usuario=$usuarioLogin&password=$passwordLogin&estado=Activo");

?>