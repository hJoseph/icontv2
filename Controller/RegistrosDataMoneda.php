<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];


$con = new conexion();

$allUsuarios = $con->getAllUsersData();
$menuMain = $con->getMenu();

if(isset($_POST['moneda'])){

    $moneda = $_POST['moneda'];
    $idMoneda = $_POST['idMoneda'];



    if($moneda == "Agentina"){
        $pais="Agentina";
        $tipoMoneda="$";
        $contexto="peso argentino";
    }

    if($moneda == "EUA"){
        $pais="Estados Unidos";
        $tipoMoneda="$";
        $contexto="dólar estadounidense";
    }
    if($moneda == "Bolivia"){
        $pais="Bolivia";
        $tipoMoneda="Bs.";
        $contexto="bolivianos";
    }
    if($moneda == "Ecuador"){
        $pais="Ecuador";
        $tipoMoneda="$";
        $contexto="dólar estadounidense";
    }
    if($moneda == "Colombia"){
        $pais="Colombia";
        $tipoMoneda="$";
        $contexto="peso colombiano";
    }
    if($moneda == "Peru"){
        $pais="Peru";
        $tipoMoneda="S/";
        $contexto="sol";
    }
    if($moneda == "Brasil"){
        $pais="Brasil";
        $tipoMoneda="R$";
        $contexto="real brasileño";
    }
    if($moneda == "Chile"){
        $pais="Chile";
        $tipoMoneda="$";
        $contexto="peso chileno	";
    }
    if($moneda == "Venezuela"){
        $pais="Venezuela";
        $tipoMoneda="Bs F";
        $contexto="bolívar fuerte";
    }
    if($moneda == "Mexico"){
        $pais="Mexico";
        $tipoMoneda="$";
        $contexto="peso mexicano";
    }
    if($moneda == "Espania"){
        $pais="Espania";
        $tipoMoneda="€";
        $contexto="euro";
    }
    if($moneda == "Paraguay"){
        $pais="Paraguay";
        $tipoMoneda="₲";
        $contexto="guaraní paraguayo";
    }
    if($moneda == "Uruguay"){
        $pais="Uruguay";
        $tipoMoneda="$";
        $contexto="peso uruguayo";
    }


    $mensaje="Se Actualizo los datos de la moneda";
    $alerta="alert alert-info";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $updateUser = $con->updateMoneda($idMoneda, $pais, $tipoMoneda, $contexto);

}



header("Location: Moneda.php?usuario=$usuarioLogin&password=$passwordLogin&estado='Activo'");

?>