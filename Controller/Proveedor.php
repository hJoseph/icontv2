<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuario = $_GET['usuario'];
$contrasena = $_GET['password'];

$con = new conexion();
$userRegister = $con->getUser($usuario, $contrasena);

foreach ($userRegister as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $usuario = $user['login'];
    $password = $user['password'];
    $foto = $user['foto'];
 }

$color="#4e4e4e";
$colorAll="#0061c2";
$idmenu="3";
$updateMenu =$con->getUpdateMenu($idmenu, $color);
$updateAllMenu =$con->getAllUpdateMenu($idmenu, $colorAll);

$getPreventaTotal = $con->getPreventaTotal();
$allProveedor = $con->getAllProveedor();

if(!isset($_GET['estado'])){
    $mensaje="";
    $alerta="";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);
}

$tipoOfAlerta = $con->getTipoMensajeAlerta();
foreach ($tipoOfAlerta as $tipoAlerta) {
    $alerta = $tipoAlerta['tipoAlerta'];
}

$tipoMessajeAlerta = $con->getMensajeAlerta();
foreach ($tipoMessajeAlerta as $tipoMensaje) {
    $messageAlerta = $tipoMensaje['mensaje'];
}



$menuMain = $con->getMenu();

 require('../Views/ProveedorViews.php');

?>