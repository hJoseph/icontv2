<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$idproducto = $_GET['idproducto'];
$iduserPedido = $_GET['iduser'];

$con = new conexion();

$getUserSession = $con->getUserSession();

foreach ($getUserSession as $userSession) {
    $usuario= $userSession['usuario'];
    $password = $userSession['password'];
}

$productoData = $con->getProductoData($idproducto);
foreach ($productoData as $product) {
    $idproducto=$product['idproducto'];
    $imagen= $product["imagen"];
    $producto=$product['codigo'];
    $nombreProducto=$product['nombreProducto'];
    $cantidad = $product["cantidad"];
    $fechaRegistro = date('Y-m-d H:i:s');
    $precioVenta= $product["precioVenta"];
    $tipoProducto=$product['tipo'];
    $proveedor=$product['proveedor'];
    $precioCompra=$product['precioCompra'];
}
$tipoMesa = "Mesa";
$idUser=$iduserPedido;

 $registerPreventa = $con->registerPreventaProducto($imagen, $nombreProducto, $precioVenta, $idproducto, $precioVenta, $idUser, $tipoMesa);

$mensaje = "Se creo un nuevo pedido para la Mesa";
$alerta="alert alert-success";

$updateMensaje = $con->updateMensajeAlerta($mensaje);
$updateAlerta = $con->updateAlerta($alerta);

require('../Views/RefreshPedido.php');

?>