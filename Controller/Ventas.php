<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuario = $_GET['usuario'];
$contrasena = $_GET['password'];

$con = new conexion();
$userRegister = $con->getUser($usuario, $contrasena);

foreach ($userRegister as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $foto = $user['foto'];
}

$color="#4e4e4e";
$colorAll="#0061c2";
$idmenu="7";
$updateMenu =$con->getUpdateMenu($idmenu, $color);
$updateAllMenu =$con->getAllUpdateMenu($idmenu, $colorAll);

$tipoOfAlerta = $con->getTipoMensajeAlerta();
foreach ($tipoOfAlerta as $tipoAlerta) {
    $alerta = $tipoAlerta['tipoAlerta'];
}

$tipoMessajeAlerta = $con->getMensajeAlerta();
foreach ($tipoMessajeAlerta as $tipoMensaje) {
    $messageAlerta = $tipoMensaje['mensaje'];
}


if ($tipo == 'ADMINISTRADOR') {

    $tipoUsuario = $tipo;
    $showStatus = $con->getStatus();
    foreach ($showStatus as $status) {
        $estado = $status['estado'];
    }

    $menuMain = $con->getMenu();
    $_SESSION['id_usuario'] = $id_usuario;
    $_SESSION['nombres'] = $nombres;

    $showAllProduct = $con->getProduct();

    $showPreVenta = $con->getPreVenta();

    $totalPreVenta = $con->getTotalPreVenta();

    require('../Views/VentasPos.php');



}


?>