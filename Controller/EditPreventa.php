<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}
$idproductoPreventa = $_POST['idproducto'];
$tipo = $_POST['tipo'];
$con = new conexion();
$editPreVenta = $con->editPreVenta($idproductoPreventa,$tipo);

foreach ($editPreVenta as $preVenta) {
    $idpreventa=$preVenta['idPreventa'];
    $producto=$preVenta['producto'];
    $precio=$preVenta['precio'];
    $idproducto=$preVenta['idProducto'];
    $imagen=$preVenta['imagen'];
    $pventa=$preVenta['pventa'];
    $userId=$preVenta['idUser'];
    $tipoPedido=$preVenta['tipo'];
}

$getCantidad = $con->getCantidad($idproducto,$tipoPedido);

foreach ($getCantidad as $getCantidadProducto) {
    $cantidadActual=$getCantidadProducto['cantidad'];
}

$mensaje = "Se actualizo la nueva cantidad correctamente";
$alerta="alert alert-success";

$updateMensaje = $con->updateMensajeAlerta($mensaje);
$updateAlerta = $con->updateAlerta($alerta);

require('../Views/EditPreventaForm.php');

?>