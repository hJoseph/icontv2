<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];

$con = new conexion();

$allUsuarios = $con->getAllUsersData();
$menuMain = $con->getMenu();

if (isset($_POST['nuevo_Pedido'])) {
    $proveedor = $_POST['proveedor'];
    $descripcion = $_POST['descripcion'];
    $total = $_POST['total'];
    $fechaRegistro = $_POST['fechaRegistro'];

    $mensaje = "Se creo un nuevo registro un nuevo correctamente !!";
    $alerta = "alert alert-success";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $newPedido = $con->registerNewPedido($descripcion, $total, $proveedor, $usuarioLogin,$fechaRegistro);


}

if (isset($_GET['idborrar'])) {
    $idAccount= $_GET['idborrar'];
    $usuarioLogin = $_GET['usuarioLogin'];
    $passwordLogin = $_GET['passwordLogin'];

    $mensaje = "Se Elimino un registro correctamente !!";
    $alerta = "alert alert-danger";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $deletePedido= $con->deletePedido($idAccount);

}

if (isset($_POST['update'])) {
    $idPedido = $_POST['idPedido'];
    $proveedor = $_POST['proveedor'];
    $descripcion = $_POST['descripcion'];
    $total = $_POST['total'];
    $fechaRegistro = $_POST['fechaRegistro'];

    $mensaje = "Se Actualizo el pedido correctamente !!";
    $alerta="alert alert-info";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $editPedido = $con->updatePedido($descripcion, $total, $proveedor, $usuarioLogin,$fechaRegistro,$idPedido);
}

header("Location: Pedido.php?usuario=$usuarioLogin&password=$passwordLogin&estado=Activo");

?>