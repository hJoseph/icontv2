<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];


$con = new conexion();

$allUsuarios = $con->getAllUsersData();
$menuMain = $con->getMenu();

if(isset($_POST['idioma'])){

    $idioma = $_POST['idioma'];
    $idIdioma = $_POST['idIdioma'];

    $mensaje="Se Actualizo el Idioma ";
    $alerta="alert alert-info";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);
    $updateIdioma = $con->updateIdioma($idioma, $idIdioma);


    $updateIdiomaMenu = $con->updateMenuIdioma();


}


header("Location: Languaje.php?usuario=$usuarioLogin&password=$passwordLogin&estado='Activo'");

?>