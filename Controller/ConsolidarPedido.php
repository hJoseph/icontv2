<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];

$con = new conexion();

$allUsuarios = $con->getAllUsersData();
$menuMain = $con->getMenu();


if (isset($_GET['idConsolidar'])) {
    $idPedido= $_GET['idConsolidar'];
    $usuarioLogin = $_GET['usuarioLogin'];
    $passwordLogin = $_GET['passwordLogin'];

    $mensaje = "Se Consolido Consolido Correctamente la transaccion !!";
    $alerta = "alert alert-info";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $deletePedido= $con->consolidarPedido($idPedido);

}



if (isset($_POST['consolidar'])) {
    $idConsolidar = $_POST['idConsolidar'];
    $comentario = $_POST['comentario'];

    $mensaje = "Se Inserto  el detalle de la Venta  !!";
    $alerta="alert alert-info";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $editPedido = $con->updateDetalleVenta($idConsolidar, $comentario);
}

header("Location: Consolidar.php?usuario=$usuarioLogin&password=$passwordLogin&estado=Activo");

?>