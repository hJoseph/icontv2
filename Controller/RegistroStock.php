<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];

$con = new conexion();

$allUsuarios = $con->getAllUsersData();
$menuMain = $con->getMenu();

if (isset($_POST['nuevo_Producto'])) {
    $proveedor = $_POST['proveedor'];
    $codigo = $_POST['codigo'];
    $nombreProducto = $_POST['descripcion'];
    $precioDeCompra = $_POST['precioDeCompra'];
    $cantidad = $_POST['stock'];
    $alarma = $_POST['alarma'];
    $fechaRegistro = date('Y-m-d');



    $mensaje = "Se Añadio un nuevo producto al almacen";
    $alerta = "alert alert-success";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    if($_FILES['userfile']['name']!=""){

        $ruta = "fotoproducto/";
        opendir($ruta);
        $destino = $ruta.$_FILES['userfile']['name'];


        $nombre_archivo = "C:/xampp/htdocs/icontv2/Views/fotoproducto/" . $_FILES['userfile']['name'];
        $tipo_archivo = $_FILES['userfile']['type'];
        $tamano_archivo = $_FILES['userfile']['size'];


        $nuevo_archivo= "fotoproducto/" . substr($tipo_archivo,6,4);


        if (!((strpos($tipo_archivo, "gif") || strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo < 5000000))) {
            cuadro_error("La extensión o el tamaño de los archivos no es correcta, Se permiten archivos .gif o .jpg de 5 Mb máximo");

        }else{
            if (move_uploaded_file($_FILES['userfile']['tmp_name'], $nombre_archivo)){
                 rename($nombre_archivo,$nuevo_archivo);
                //  cuadro_mensaje("El archivo ha sido cargado correctamente");
            }else{
                cuadro_error("Ocurrió algún error al subir el archivo. No pudo guardarse");
            }
        }
    }

    else{
        $destino = "fotoproducto/NoPicture.jpg";
    }


    $registerNewProducto = $con->registerNewProductoStock($destino, $proveedor, $codigo, $nombreProducto, $precioDeCompra, $cantidad, $alarma,$fechaRegistro);

}

if (isset($_GET['idborrar'])) {
    $idProducto = $_GET['idborrar'];
    $usuarioLogin = $_GET['usuarioLogin'];
    $passwordLogin = $_GET['passwordLogin'];

    $mensaje = "Se Elimino un Producto del Stock";
    $alerta = "alert alert-danger";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $deleteStockProducto = $con->deleteStockProducto($idProducto);

}

if (isset($_POST['update_producto'])) {
    $imagen = $_POST['imagen'];
    $idProducto = $_POST['idproducto'];
    $proveedor = $_POST['proveedor'];
    $codigo = $_POST['codigo'];
    $nombreProducto = $_POST['descripcion'];
    $precioDeCompra = $_POST['precioDeCompra'];
    $cantidad = $_POST['stock'];
    $alarma = $_POST['alarma'];
    $fechaRegistro = date('Y-m-d');

    $mensaje = "Se Edito el producto del stock";
    $alerta="alert alert-info";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    if($_FILES['userfileEdit']['name']!=""){

        $ruta = "fotoproducto/";
        opendir($ruta);
        $destino = $ruta.$_FILES['userfileEdit']['name'];


        $nombre_archivo = "C:/xampp/htdocs/icontv2/Views/fotoproducto/" . $_FILES['userfileEdit']['name'];
        $tipo_archivo = $_FILES['userfileEdit']['type'];
        $tamano_archivo = $_FILES['userfileEdit']['size'];


        $nuevo_archivo= "fotoproducto/" . substr($tipo_archivo,6,4);


        if (!((strpos($tipo_archivo, "gif") || strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo < 5000000))) {
            cuadro_error("La extensión o el tamaño de los archivos no es correcta, Se permiten archivos .gif o .jpg de 5 Mb máximo");

        }else{
            if (move_uploaded_file($_FILES['userfileEdit']['tmp_name'], $nombre_archivo)){
                rename($nombre_archivo,$nuevo_archivo);
            }else{
                cuadro_error("Ocurrió algún error al subir el archivo. No pudo guardarse");
            }
        }
    }

    else{
        $destino = $imagen;
    }

//echo "UPDATE `stock` SET `codigo` = '$codigo',
//                                                   `nombreProducto` = '$nombreProducto',
//                                                   `fechaRegistro` = '$fechaRegistro',
//                                                   `cantidadStock` = '$cantidad',
//                                                   `proveedor` = '$proveedor',
//                                                   `alarma` = '$alarma',
//                                                   `precioCompra` = '$precioDeCompra',
//                                                   `imagen` = '$destino'
//                                                   WHERE `idproducto` = '$idProducto'";

    $updateProductoStock = $con->updateProductoStock($destino, $proveedor, $codigo, $nombreProducto, $precioDeCompra, $cantidad, $alarma,$fechaRegistro,$idProducto);



}

header("Location: Inventario.php?usuario=$usuarioLogin&password=$passwordLogin&estado=Activo");

?>