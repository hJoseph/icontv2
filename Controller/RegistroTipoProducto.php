<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];

$con = new conexion();

$allUsuarios = $con->getAllUsersData();
$menuMain = $con->getMenu();

if (isset($_POST['nuevo_Tipo'])) {
    $tipoProducto = $_POST['tipoProducto'];

    $newTipoProducto= $con->registerNewTipo($tipoProducto);

    $mensaje = "Se creo un nuevo tipo de Producto correctamente !!";
    $alerta = "alert alert-success";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);
}

if (isset($_GET['idborrar'])) {
    $idTipoProducto= $_GET['idborrar'];
    $usuarioLogin = $_GET['usuarioLogin'];
    $passwordLogin = $_GET['passwordLogin'];

    $mensaje = "Se Elimino un tipo de proucto correctamente !!";
    $alerta = "alert alert-danger";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $deleteTipoProducto= $con->deleteTipoProducto($idTipoProducto);

}

if (isset($_POST['update_tipo'])) {
    $idTipoProducto= $_POST['idtipoproducto'];
    $tipoProducto = $_POST['tipoProducto'];

    $mensaje = "Se Actualizo el tipo de producto correctamente !!";
    $alerta="alert alert-info";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $updateTipo= $con->updateTipoProducto($idTipoProducto,$tipoProducto);

    echo "UPDATE `tipoproducto` SET `tipoproducto` = $tipoProducto  WHERE `idtipoproducto` = $idTipoProducto";

}

header("Location: Tipo.php?usuario=$usuarioLogin&password=$passwordLogin&estado=Activo");

?>