<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];

$con = new conexion();

$allUsuarios = $con->getAllUsersData();
$menuMain = $con->getMenu();

if(isset($_POST['nuevo_cliente'])){
    $nombre=$_POST['nombre'];
    $apellido=$_POST['apellido'];
    $direccion=$_POST['direccion'];
    $telefonoFijo=$_POST['telefonoFijo'];
    $telefonoCelular=$_POST['telefonoCelular'];
    $email=$_POST['email'];
    $contactoReferencia=null;
    $telefonoReferencia=null;
    $observaciones=null;
    $fechaRegistro= date('Y-m-d');
    $ci=$_POST['ci'];

    $mensaje="Se Añadio un nuevo proveedor";
    $alerta="alert alert-success";


    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);



    if($_FILES['userfile']['name']!=""){

        $ruta = "fotoproducto/";
        opendir($ruta);
        $destino = $ruta.$_FILES['userfile']['name'];


        $nombre_archivo = "C:/xampp/htdocs/icontv2/Views/fotoproducto/" . $_FILES['userfile']['name'];
        $tipo_archivo = $_FILES['userfile']['type'];
        $tamano_archivo = $_FILES['userfile']['size'];


        $nuevo_archivo= "fotoproducto/" . substr($tipo_archivo,6,4);


        if (!((strpos($tipo_archivo, "gif") || strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo < 5000000))) {
            cuadro_error("La extensión o el tamaño de los archivos no es correcta, Se permiten archivos .gif o .jpg de 5 Mb máximo");

        }else{
            if (move_uploaded_file($_FILES['userfile']['tmp_name'], $nombre_archivo)){
                rename($nombre_archivo,$nuevo_archivo);
                //  cuadro_mensaje("El archivo ha sido cargado correctamente");
            }else{
                cuadro_error("Ocurrió algún error al subir el archivo. No pudo guardarse");
            }
        }
    }

    else{
        $destino = "fotoUsuario/user.png";
    }

    $registerNewProveedor = $con->registerNewCliente($nuevo_archivo, $nombre, $apellido, $direccion, $telefonoFijo,$telefonoCelular,$email,$contactoReferencia,$telefonoReferencia,$observaciones,$fechaRegistro,$ci,$destino);

}

if(isset($_GET['idborrar'])){
    $idCliente=$_GET['idborrar'];
    $usuarioLogin=$_GET['usuarioLogin'];
    $passwordLogin=$_GET['passwordLogin'];

    $mensaje="Se Elimino un Cliente";
    $alerta="alert alert-danger";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $deleteCliente = $con->deleteClient($idCliente);

}

if(isset($_POST['update_cliente'])){
    $nombre=$_POST['nombre'];
    $apellido=$_POST['apellido'];
    $direccion=$_POST['direccion'];
    $telefonoFijo=$_POST['telefonoFijo'];
    $telefonoCelular=$_POST['telefonoCelular'];
    $email=$_POST['email'];
    $contactoReferencia=null;
    $telefonoReferencia=null;
    $observaciones=null;
    $fechaRegistro= date('Y-m-d');
    $ci=$_POST['ci'];
    $idcliente=$_POST['idcliente'];
    $imagen = $_POST['imagen'];

    $mensaje="Se Actualizo los datos del Cliente";
    $alerta="alert alert-info";

    if($_FILES['userfileEdit']['name']!=""){

        $ruta = "fotoproducto/";
        opendir($ruta);
        $destino = $ruta.$_FILES['userfileEdit']['name'];


        $nombre_archivo = "C:/xampp/htdocs/icontv2/Views/fotoproducto/" . $_FILES['userfileEdit']['name'];
        $tipo_archivo = $_FILES['userfileEdit']['type'];
        $tamano_archivo = $_FILES['userfileEdit']['size'];


        $nuevo_archivo= "fotoproducto/" . substr($tipo_archivo,6,4);


        if (!((strpos($tipo_archivo, "gif") || strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "png")) && ($tamano_archivo < 5000000))) {
            cuadro_error("La extensión o el tamaño de los archivos no es correcta, Se permiten archivos .gif o .jpg de 5 Mb máximo");

        }else{
            if (move_uploaded_file($_FILES['userfileEdit']['tmp_name'], $nombre_archivo)){
                rename($nombre_archivo,$nuevo_archivo);
            }else{
                cuadro_error("Ocurrió algún error al subir el archivo. No pudo guardarse");
            }
        }
    }

    else{
        $destino = $imagen;
    }




    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $updateUser = $con->editClient($nombre,$apellido, $direccion, $telefonoFijo, $telefonoCelular, $fechaRegistro,$ci,$idcliente,$email,$destino);

}

header("Location: Cliente.php?usuario=$usuarioLogin&password=$passwordLogin&estado=Activo");

?>