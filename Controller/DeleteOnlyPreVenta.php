<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$idproducto = $_GET['idproducto'];
$tipo = $_GET['tipo'];

$con = new conexion();

$getUserSession = $con->getUserSession();

foreach ($getUserSession as $userSession) {
    $usuario= $userSession['usuario'];
    $password = $userSession['password'];
}

$deleteOnlyPreventa = $con->deleteUnaPreVenta($idproducto,$tipo);

$showPreVenta = $con->getPreVenta();
$totalPreVenta = $con->getTotalPreVenta();

$mensaje = "Se Elimino un pedido correctamente";
$alerta="alert alert-danger";

$updateMensaje = $con->updateMensajeAlerta($mensaje);
$updateAlerta = $con->updateAlerta($alerta);

require('../Views/RefreshPedido.php');
?>