<?php
require('../Model/conexion.php');

if (!isset($_SESSION)) {
    session_start();
}

$usuario = $_GET['usuario'];
$contrasena = $_GET['password'];

$con = new conexion();
$userRegister = $con->getUser($usuario, $contrasena);

foreach ($userRegister as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombres = $user['nombre'];
    $foto = $user['foto'];
}

if ($tipo == 'ADMINISTRADOR') {

    $tipo = $tipo;
    $showStatus = $con->getStatus();
    foreach ($showStatus as $status) {
        $estado = $status['estado'];
    }

    $menuMain = $con->getMenu();
    $_SESSION['id_usuario'] = $id_usuario;
    $_SESSION['nombres'] = $nombres;

    $cantidadActual=$_GET["cantidad"];
    $idproducto=$_GET["idproducto"];
    $productoUpdate=$_GET["producto"];
    $imagen=$_GET["imagen"];
    $precio=$_GET["precio"];
    $pventa=$_GET["pventa"];

    $getCantidad = $con->getCantidad($idproducto);

    foreach ($getCantidad as $getCantidadProducto) {
        $cantidadAnterior=$getCantidadProducto['cantidad'];
    }

    if($cantidadActual > $cantidadAnterior){

        $cantidadActualizada=$cantidadActual-$cantidadAnterior;
        for ( $i = 0; $i <$cantidadActualizada; $i++ ){
            $insertNewPreventa = $con->insertNewPreventa($imagen,$productoUpdate,$precio,$idproducto,$pventa);
        }
    }

    else{
        $cantidadActualizada=$cantidadAnterior-$cantidadActual;
        $deleteCantidadActual = $con->deleteCantidadActual($idproducto);
        for ( $i = 0; $i <$cantidadActualizada; $i++ ){
            $insertNewPreventa = $con->insertNewPreventa($imagen,$productoUpdate,$precio,$idproducto,$pventa);
        }
    }

    $showAllProduct = $con->getProduct();

    $showPreVenta = $con->getPreVenta();

    $totalPreVenta = $con->getTotalPreVenta();


    require('../Views/VentasPosOld.php');

}


?>