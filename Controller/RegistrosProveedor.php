<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];

$con = new conexion();

$allUsuarios = $con->getAllUsersData();
$menuMain = $con->getMenu();

if(isset($_POST['nuevo_proveedor'])){
    $proveedor=$_POST['proveedor'];
    $responsable=$_POST['responsable'];
    $direccion=$_POST['direccion'];
    $telefono=$_POST['telefono'];
    $fechaRegistro=$_POST['fechaRegistro'];

    $mensaje="Se Añadio un nuevo proveedor";
    $alerta="alert alert-success";


    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $registerNewProveedor = $con->registerNewProveedor($proveedor, $responsable, $direccion, $telefono, $fechaRegistro);

}

if(isset($_GET['idborrar'])){
    $idProveedor=$_GET['idborrar'];
    $usuarioLogin=$_GET['usuarioLogin'];
    $passwordLogin=$_GET['passwordLogin'];

    $mensaje="Se Elimino un Proveedor";
    $alerta="alert alert-danger";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $deleteProveedor = $con->deleteProveedor($idProveedor);

}

if(isset($_POST['update_proveedor'])){
    $proveedorId=$_POST['idproveedor'];
    $proveedor=$_POST['proveedor'];
    $responsable=$_POST['responsable'];
    $direccion=$_POST['direccion'];
    $telefono=$_POST['telefono'];
    $fechaRegistro=$_POST['fechaRegistro'];
    $usuarioLogin=$_POST['usuarioLogin'];
    $passwordLogin=$_POST['passwordLogin'];

    $mensaje="Se Actualizo los datos del Proveedor";
    $alerta="alert alert-info";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $updateUser = $con->editProveedor($proveedorId,$proveedor, $responsable, $direccion, $telefono, $fechaRegistro);

}

header("Location: Proveedor.php?usuario=$usuarioLogin&password=$passwordLogin&estado='Activo'");

?>