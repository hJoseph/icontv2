<?php
require "../Model/ModelUrl.php";
?>

<!DOCTYPE html>
<html lang="en">
<?php
include("head.php");
?>


<!---<link href=<php echo url(); ?>/Views/popup/shadowbox.css" rel="stylesheet" type="text/css"/>-->

<script type="text/javascript" src="<?php echo url(); ?>/Views/popup/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/Views/popup/shadowbox.js"></script>
<script type="text/javascript"> Shadowbox.init({
        language: "es",
        players: ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv']
    }); </script>


<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () {
            Shadowbox.open({
                content: '<div><img src="<?php echo url() . "/Views/popup/welcome.jpg"?>" ></div>',
                player: "html",
                title: "Hola !!! ",
                width: 450,
                height: 201
            });
        }, 50);
    });
</script>
<style type="text/css">

    .html, body {
        font-family: Verdana, Geneva, sans-serif;
        font-size: 12px;
    }

    .ejemplo {
        float: left;
        width: 100%;
        padding: 0px;
        margin: 0px;
    }

    .ejemplo img {
        float: left;
        padding: 2px;
        border: 1px solid #999;
        margin-right: 10px;
        margin-bottom: 10px;
    }

</style>
<body>
<!-- container section start -->
<section id="container" class="">


    <header class="header dark-bg">
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i
                        class="icon_menu"></i></div>
        </div>

        <?PHP include("logo.php"); ?>

        <div class="nav search-row" id="top_menu">
            <!--  search form start -->
            <ul class="nav top-menu">
                <li>
                    <form class="navbar-form">
<!--                        <input class="form-control" placeholder="Search" type="text">-->
                    </form>
                </li>
            </ul>
            <!--  search form end -->
        </div>


        <?PHP include("DropDown.php"); ?>


    </header>
    <!--header end-->

    <?PHP include("menu.php"); ?>


    </div>
    </aside>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!--overview start-->
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><i class="fa fa-laptop"></i> PRINCIPAL</h3>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="principal.php">Inicio</a></li>
                        <li><i class="fa fa-laptop"></i> Principal</li>
                    </ol>
                </div>
            </div>

            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <td align="center"><a href="LogoutUser.php"> <img
                                    src="<?php echo url(); ?>/Views/img/salirSistema.png"></a></td>
                    <td align="center"><a href="LogoutUser.php"> <img
                                    src="<?php echo url(); ?>/Views/img/nube.png"></a></td>

                </tr>
                </thead>


            </table>

        </section>
    </section>
    <!--main content end-->
</section>
<!-- container section start -->

<!-- javascripts -->

<script src="<?php echo url(); ?>/Views/js/jquery.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery-ui-1.10.4.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/Views/js/jquery-ui-1.9.2.custom.min.js"></script>
<!-- bootstrap -->
<script src="<?php echo url(); ?>/Views/js/bootstrap.min.js"></script>
<!-- nice scroll -->
<script src="<?php echo url(); ?>/Views/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery.nicescroll.js" type="text/javascript"></script>
<!-- charts scripts -->

<script src="<?php echo url(); ?>/Views/js/jquery.sparkline.js" type="text/javascript"></script>
<script src="<?php echo url(); ?>/Views/js/owl.carousel.js"></script>

<!--script for this page only-->
<!-----------------------------------<script src="js/calendar-custom.js"></script>----->
<script src="<?php echo url(); ?>/Views/js/jquery.rateit.min.js"></script>
<!-- custom select -->
<script src="<?php echo url(); ?>/Views/js/jquery.customSelect.min.js"></script>


<!--custome script for all page-->
<script src="<?php echo url(); ?>/Views/js/scripts.js"></script>
<!-- custom script for this page-->
<script src="<?php echo url(); ?>/Views/js/sparkline-chart.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?php echo url(); ?>/Views/js/xcharts.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery.autosize.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery.placeholder.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/gdp-data.js"></script>
<script src="<?php echo url(); ?>/Views/js/morris.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/sparklines.js"></script>
<script src="<?php echo url(); ?>/Views/js/charts.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery.slimscroll.min.js"></script>


<script src="<?php echo url(); ?>/Views/js/zabuto_calendar.js"></script>


<script type="application/javascript">
    $(document).ready(function () {
        $("#my-calendar").zabuto_calendar({
            language: "es",
            today: true,
            nav_icon: {
                prev: '<i class="fa fa-chevron-circle-left"></i>',
                next: '<i class="fa fa-chevron-circle-right"></i>'
            }
        });
    });
</script>

<!-- DataTables JavaScript -->
<script src="<?php echo url(); ?>/Views/js/jquery.dataTables.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/dataTables.bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>

</body>
</html>