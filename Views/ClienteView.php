<?php
require "../Model/ModelUrl.php";
?>

<!DOCTYPE html>
<html lang="en">
<?php
include("head.php");
?>

<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () {
            Shadowbox.open({
                content: '<div><img src="<?php echo url() . "/Views/popup/welcome.jpg"?>" ></div>',
                player: "html",
                title: "Hola !!! ",
                width: 450,
                height: 201
            });
        }, 50);
    });
</script>
<style type="text/css">

    .html, body {
        font-family: Verdana, Geneva, sans-serif;
        font-size: 12px;
    }

    .ejemplo {
        float: left;
        width: 100%;
        padding: 0px;
        margin: 0px;
    }

    .ejemplo img {
        float: left;
        padding: 2px;
        border: 1px solid #999;
        margin-right: 10px;
        margin-bottom: 10px;
    }

</style>
<body>
<!-- container section start -->
<section id="container" class="">


    <header class="header dark-bg">
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i
                    class="icon_menu"></i></div>
        </div>

        <?PHP include("logo.php"); ?>

        <div class="nav search-row" id="top_menu">
            <!--  search form start -->
            <ul class="nav top-menu">
                <li>
                    <form class="navbar-form">
<!--                        <input class="form-control" placeholder="Search" type="text">-->
                    </form>
                </li>
            </ul>
            <!--  search form end -->
        </div>
        <?PHP include("DropDown.php"); ?>
    </header>
    <?PHP include("menu.php"); ?>
    </div>
    </aside>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><i class="fa fa-group"></i><strong> Registros de Clientes</strong></h3>
                    <div class="<?php echo $alerta; ?>" role="alert">
                        <b><?php echo $messageAlerta; ?> </b>
                    </div>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="principal.php">Inicio</a></li>
                        <li><i class="fa fa-group"></i>Clientes</li>

                    </ol>
                </div>
            </div>
            <!--<div align="right">
              <td><a class="btn btn-primary" href="#add" title=""><span class="icon_lightbulb_alt" data-original-title="Tooltip on left"></span> Agregar Nuevo</a></td>
                <button href="#add" title="" data-placement="left" data-toggle="tooltip" class="btn btn-primary tooltips" type="button" data-original-title="Nuvo Proveedor"><span class="icon_document_alt"></span> AGREGAR NUEVO PROVEEDOR</button>
            </div>--->

            <!--modal start-->

            <header class="panel-heading">
                <div class="panel-body">
                    <div align="right">
                        <button href="#add" title="" data-placement="left" data-toggle="modal"
                                class="btn btn-primary tooltips" type="button" data-original-title="Nuvo Cliente"><span
                                    class="icon_profile"></span> REGISTRAR NUEVO CLIENTE
                        </button>
                    </div>
                    <div id="add" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                         aria-hidden="true">
                        <form class="form-validate form-horizontal" name="form2" action="RegistroCliente.php"
                              method="post"
                              enctype="multipart/form-data">
                            <input name="usuarioLogin" value="<?php echo $usuario; ?>" type="hidden">
                            <input name="passwordLogin" value="<?php echo $password; ?>" type="hidden">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                        </button>
                                        <h3 id="myModalLabel" align="center">Registrar Informacion del Cliente</h3>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div><strong>Agregar Imagen</strong></div>
                                                <br>
                                                <input id="files" name="userfile" type="file"/>
                                                <output id="list-miniatura"></output>
                                                <output id="list-datos"></output>
                                            </div>
                                            <div class="col-lg-8">
                                                <br><br><br><br>
                                                <label for="nombre" class="control-label col-lg-3">Nombre:</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control input-lg m-bot15" id="nombre"
                                                           name="nombre" minlength="5" type="text" required/>
                                                </div>
                                                <br><br>

                                                <label for="apellido" class="control-label col-lg-3">Apellido:</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control input-lg m-bot15" id="apellido"
                                                           name="apellido" minlength="5" type="text" required/>
                                                </div>
                                                <br><br>

                                                <label for="ci" class="control-label col-lg-3">CI:</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control input-lg m-bot15" id="ci" name="ci"
                                                           minlength="5" type="text" required/>
                                                </div>
                                                <br><br>

                                                <label for="direccion" class="control-label col-lg-3">Direccion:</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control input-lg m-bot15" id="direccion"
                                                           name="direccion" minlength="5" type="text" required/>
                                                </div>
                                                <br><br>

                                                <label for="telefonoFijo"
                                                       class="control-label col-lg-3">Telefono:</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control input-lg m-bot15" id="telefonoFijo"
                                                           name="telefonoFijo" minlength="5" type="text" required/>
                                                </div>
                                                <br><br>

                                                <label for="telefonoCelular"
                                                       class="control-label col-lg-3">Celular:</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control input-lg m-bot15" id="telefonoCelular"
                                                           name="telefonoCelular" minlength="5" type="text" required/>
                                                </div>
                                                <br><br>

                                                <label for="email" class="control-label col-lg-3">Email:</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control input-lg m-bot15" id="email" name="email"
                                                           minlength="5" type="text" required/>
                                                </div>
                                                <br><br>

                                                <label for="fechaRegistro" class="control-label col-lg-3">Fecha:</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control input-lg m-bot15" type="date" readonly
                                                           name="fechaRegistro" autocomplete="off"
                                                           value="<?php echo date('Y-m-d'); ?>">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><strong>Cerrar</strong>
                                        </button>
                                        <button name="nuevo_cliente" type="submit" class="btn btn-primary">
                                            <strong>Registrar</strong></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </header>


            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>FOTO</th>
                            <th>NOMBRE</th>
                            <th>CI</th>
                            <th>DIRECCION</th>
                            <th>TELEFONO FIJO</th>
                            <th>TELEFONO CELULAR</th>
                            <th>EMAIL</th>
                            <th>FECHA DE REGISTRO</th>
                            <th>ACCIONES</th>
                        </tr>
                        </thead>
                        <?php while ($client = mysqli_fetch_array($allClient)) { ?>
                            <tr>
                                <td><img src="<?PHP echo url();
                                    echo '/Views/';
                                    echo $client['foto']; ?>" width="50" height="50"></td>
                                <td><?php echo $client['apellido'] . ' ' . $client['nombre']; ?></td>
                                <td><?php echo $client['ci']; ?></td>
                                <td><?php echo $client['direccion']; ?></td>
                                <td><?php echo $client['telefonoFijo']; ?></td>
                                <td><?php echo $client['telefonoCelular']; ?></td>
                                <td><?php echo $client['email']; ?></td>
                                <td><?php echo $client['fechaRegistro']; ?></td>
                                <td>
                                    <a href="#a<?php echo $client[0]; ?>" role="button" class="btn btn-success"
                                       data-toggle="modal"><i class="icon_check_alt2"></i></a>
                                    <a href="RegistroCliente.php?idborrar=<?php echo $client[0]; ?>&usuarioLogin=<?php echo $usuario; ?>&passwordLogin=<?php echo $password; ?>"
                                       class="btn btn-danger"><i class="icon_close_alt2"></i></a>
                                </td>
                            </tr>

                            <div id="a<?php echo $client[0]; ?>" class="modal fade" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true">
                                <form class="form-validate form-horizontal" name="form2" action="RegistroCliente.php"
                                      method="post" enctype="multipart/form-data" >
                                    <input type="hidden" name="idcliente" value="<?php echo $client['idcliente']; ?>">
                                    <input name="usuarioLogin" value="<?php echo $usuario; ?>" type="hidden">
                                    <input name="passwordLogin" value="<?php echo $password; ?>" type="hidden">
                                    <input type="hidden" name="imagen" value="<?php echo $client['foto']; ?>">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">×
                                                </button>
                                                <h3 id="myModalLabel" align="center">Cambiar Informacion del
                                                    Cliente</h3>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div><strong> Imagen</strong></div>
                                                        <img src="<?PHP echo url();
                                                        echo '/Views/';
                                                        echo $client['foto']; ?>" width="250" height="250">
                                                        <br><br>
                                                        <section class="panel" class="col-lg-6">
                                                            <div><strong>Cambiar Imagen de Cliente</strong></div>
                                                            <?php  include("UploadViewImage.php");  ?>
                                                        </section>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <br><br><br><br>
                                                        <label for="nombre" class="control-label col-lg-3">Nombre:</label>
                                                        <div class="col-lg-9">
                                                            <input class="form-control input-lg m-bot15" id="nombre"
                                                                   name="nombre" minlength="3" type="text" value="<?php echo $client['nombre']; ?>"/>
                                                        </div>
                                                        <br><br>

                                                        <label for="apellido" class="control-label col-lg-3">Apellido:</label>
                                                        <div class="col-lg-9">
                                                            <input class="form-control input-lg m-bot15" id="apellido"
                                                                   name="apellido" minlength="3" type="text" value="<?php echo $client['apellido']; ?>"/>
                                                        </div>
                                                        <br><br>

                                                        <label for="ci" class="control-label col-lg-3">CI:</label>
                                                        <div class="col-lg-9">
                                                            <input class="form-control input-lg m-bot15" id="ci" name="ci"
                                                                   minlength="5" type="text" value="<?php echo $client['ci']; ?>"/>
                                                        </div>
                                                        <br><br>

                                                        <label for="direccion" class="control-label col-lg-3">Direccion:</label>
                                                        <div class="col-lg-9">
                                                            <input class="form-control input-lg m-bot15" id="direccion"
                                                                   name="direccion" minlength="5" type="text" value="<?php echo $client['direccion']; ?>"/>
                                                        </div>
                                                        <br><br>

                                                        <label for="telefonoFijo"
                                                               class="control-label col-lg-3">Telefono:</label>
                                                        <div class="col-lg-9">
                                                            <input class="form-control input-lg m-bot15" id="telefonoFijo"
                                                                   name="telefonoFijo" minlength="5" type="text" value="<?php echo $client['telefonoFijo']; ?>"/>
                                                        </div>
                                                        <br><br>

                                                        <label for="telefonoCelular"
                                                               class="control-label col-lg-3">Celular:</label>
                                                        <div class="col-lg-9">
                                                            <input class="form-control input-lg m-bot15" id="telefonoCelular"
                                                                   name="telefonoCelular" minlength="5" type="text" value="<?php echo $client['telefonoCelular']; ?>"/>
                                                        </div>
                                                        <br><br>

                                                        <label for="email" class="control-label col-lg-3">Email:</label>
                                                        <div class="col-lg-9">
                                                            <input class="form-control input-lg m-bot15" id="email" name="email"
                                                                   minlength="5" type="text" value="<?php echo $client['email']; ?>"/>
                                                        </div>
                                                        <br><br>

                                                        <label for="fechaRegistro" class="control-label col-lg-3">Fecha:</label>
                                                        <div class="col-lg-9">
                                                            <input class="form-control input-lg m-bot15" type="date"
                                                                   name="fechaRegistro" autocomplete="off"
                                                                   value="<?php echo $client['fechaRegistro']; ?>">
                                                        </div>
                                                    </div>
                                                </div>




                                                <div class="modal-footer">
                                                    <button class="btn btn-default" data-dismiss="modal"
                                                            aria-hidden="true"><strong>Cerrar</strong></button>
                                                    <button name="update_cliente" type="submit" class="btn btn-primary">
                                                        <strong>Actualizar Datos</strong>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        <?php } ?>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>

        </section>
    </section>
    <!--main content end-->
</section>
<!-- container section end -->
<!-- javascripts -->
<script src="<?php echo url(); ?>/Views/js/jquery.js"></script>
<script src="<?php echo url(); ?>/Views/js/bootstrap.min.js"></script>
<!-- nicescroll -->
<script src="<?php echo url(); ?>/Views/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery.nicescroll.js" type="text/javascript"></script>
<!--custome script for all page-->
<script src="<?php echo url(); ?>/Views/js/scripts.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo url(); ?>/Views/js/jquery.dataTables.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/dataTables.bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script>
    function handleFileSelect(evt) {
        evt.stopPropagation();
        evt.preventDefault();

        var files = evt.dataTransfer.files; // FileList object.
        // files is a FileList of File objects. List some properties.
        var output = [];
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['Nombre: ', escape(theFile.name), ' || Tamanio: ', escape(theFile.size), ' bytes || type: ', escape(theFile.type), '<br /><img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '"style="width:100%;"/><br />'].join('');
                    document.getElementById('list-miniatura').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
        document.getElementById('list-datos').innerHTML = '<ul>' + output.join('') + '</ul>';
    }

    function handleDragOver(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    }

    // Setup the dnd listeners.
    // var dropZone = document.getElementById('drop_zone');
    // dropZone.addEventListener('dragover', handleDragOver, false);
    // dropZone.addEventListener('drop', handleFileSelect, false);
</script>

<script>
    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<br /><img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '" style="width:100%;"/><br />'].join('');
                    document.getElementById('list-miniatura').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>

</body>
</html>