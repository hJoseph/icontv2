<?php
require "../Model/ModelUrl.php";

?>

<!DOCTYPE html>
<html lang="en">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Sistema para el Menejo de Empresas - contabilidad , ventas , ingresos , egresos">
<meta name="author" content="GeeksLabs">
<meta name="keyword" content="Sistema modo tactil , para escritorio tabletas ">
<link rel="shortcut icon" href="img/favicon.png">

<title>iCONT - Sistema Profesional Para Empresas</title>
<!-- Bootstrap CSS -->
<link href="<?php echo url(); ?>/Views/css/bootstrap.min.css" rel="stylesheet">
<!-- bootstrap theme -->
<link href="<?php echo url(); ?>/Views/css/bootstrap-theme.css" rel="stylesheet">
<!--external css-->
<!-- font icon -->
<link href="<?php echo url(); ?>/Views/css/elegant-icons-style.css" rel="stylesheet"/>
<link href="<?php echo url(); ?>/Views/css/font-awesome.min.css" rel="stylesheet"/>
<!-- Custom styles -->
<link href="<?php echo url(); ?>/Views/css/style2.css" rel="stylesheet">
<link href="<?php echo url(); ?>/Views/css/style-responsive.css" rel="stylesheet"/>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
<!--[if lt IE 9]>
      <script src="<?php echo url(); ?>/Views/js/html5shiv.js"></script>
      <script src="<?php echo url(); ?>/Views/js/respond.min.js"></script>
      <script src="<?php echo url(); ?>/Views/js/lte-ie7.js"></script>
    <![endif]-->

<!----------- IMPRESION -------------->

<!--<script language="javascript" type="text/javascript" src="--><?php //echo url(); ?><!--/Views/jsf/funciones.js"></script>-->
<script src="<?php echo url(); ?>/Views/jsf/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="<?php echo url(); ?>/Views/jsf/jquery.printPage.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $(".btnPrint").printPage();
    });
</script>


</head>

<body>
<!-- container section start -->
<section id="container" class="">
    <!--header start-->
    <header class="header dark-bg">
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i
                        class="icon_menu"></i></div>
        </div>

        <?PHP include("logo.php"); ?>

        <div class="nav search-row" id="top_menu">
            <!--  search form start -->
            <ul class="nav top-menu">
                <li>
                    <form class="navbar-form">
<!--                        <input class="form-control" placeholder="Search" type="text">-->
                    </form>
                </li>
            </ul>
            <!--  search form end -->
        </div>

        <div class="top-nav notification-row">
            <!-- notificatoin dropdown start-->
            <ul class="nav pull-right top-menu">

                <!-- task notificatoin start -->
                <li id="task_notificatoin_bar" class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="icon-task-l"></i>
                                <span class="badge bg-important">5</span>
                    </a>
                    <ul class="dropdown-menu extended tasks-bar">
                        <div class="notify-arrow notify-arrow-blue"></div>
                        <li>
                            <p class="blue">You have 5 pending tasks</p>
                        </li>
                        <li>
                            <a href="#">
                                <div class="task-info">
                                    <div class="desc">Design PSD</div>
                                    <div class="percent">90%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="90"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                                        <span class="sr-only">90% Complete (success)</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="task-info">
                                    <div class="desc">
                                        Project 1
                                    </div>
                                    <div class="percent">30%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="30"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 30%">
                                        <span class="sr-only">30% Complete (warning)</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="task-info">
                                    <div class="desc">Digital Marketing</div>
                                    <div class="percent">80%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                        <span class="sr-only">80% Complete</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="task-info">
                                    <div class="desc">Logo Designing</div>
                                    <div class="percent">78%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="78"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 78%">
                                        <span class="sr-only">78% Complete (danger)</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="task-info">
                                    <div class="desc">Mobile App</div>
                                    <div class="percent">50%</div>
                                </div>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0"
                                         aria-valuemax="100" style="width: 50%">
                                        <span class="sr-only">50% Complete</span>
                                    </div>
                                </div>

                            </a>
                        </li>
                        <li class="external">
                            <a href="#">See All Tasks</a>
                        </li>
                    </ul>
                </li>
                <!-- task notificatoin end -->
                <!-- inbox notificatoin start-->
                <li id="mail_notificatoin_bar" class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon-envelope-l"></i>
                        <span class="badge bg-important">5</span>
                    </a>
                    <ul class="dropdown-menu extended inbox">
                        <div class="notify-arrow notify-arrow-blue"></div>
                        <li>
                            <p class="blue">You have 5 new messages</p>
                        </li>
                        <li>
                            <a href="#">
                                <span class="photo"><img alt="avatar" src="./img/avatar-mini.jpg"></span>
                                <span class="subject">
                                    <span class="from">Greg  Martin</span>
                                    <span class="time">1 min</span>
                                    </span>
                                <span class="message">
                                        I really like this admin panel.
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="photo"><img alt="avatar" src="./img/avatar-mini2.jpg"></span>
                                <span class="subject">
                                    <span class="from">Bob   Mckenzie</span>
                                    <span class="time">5 mins</span>
                                    </span>
                                <span class="message">
                                     Hi, What is next project plan?
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="photo"><img alt="avatar" src="./img/avatar-mini3.jpg"></span>
                                <span class="subject">
                                    <span class="from">Phillip   Park</span>
                                    <span class="time">2 hrs</span>
                                    </span>
                                <span class="message">
                                        I am like to buy this Admin Template.
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="photo"><img alt="avatar" src="./img/avatar-mini4.jpg"></span>
                                <span class="subject">
                                    <span class="from">Ray   Munoz</span>
                                    <span class="time">1 day</span>
                                    </span>
                                <span class="message">
                                        Icon fonts are great.
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">See all messages</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="profile-ava">
                                <img src="<?php echo url(); ?>/Views/<?PHP echo $foto; ?>" alt="Usuario" height="35"
                                     width="35">
                            </span>
                        <span class="username"><?PHP echo $nombres; ?> </span>
                        <b class="caret"></b>
                    </a>
                    <?PHP include("menuSalida.php"); ?>
        </div>
    </header>
    <?PHP include("menu.php"); ?>
    </div>
    </aside>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><i class="fa fa-group"></i><strong> FACTURA</strong></h3>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="index.php">Inicio</a></li>
                        <li><i class="fa fa-group"></i>Vista Previa Facturacion</li>
                        <li><i class="fa fa-home"></i><a href="NuevoPedidoController.php?usuario=<?PHP echo $usuario; ?>&password=<?PHP echo $contrasena;?>">Nuevo Pedido</a></li>
                    </ol>
                    <table>
                        <tr>
                            <td width="150" align="center">
                                <a class="btnPrint" href='ConFactura.php?usuario=<?PHP echo $usuario; ?>&password=<?PHP echo $contrasena;?>&ci=<?PHP echo $nitci;?>&ingreso1=<?PHP echo $totalApagar;?>&ingreso2=<?PHP echo $efectivo;?>&resultado=<?PHP echo $cambio;?>'>
                                <img src="<?php echo url(); ?>/Views/ticket/images/impresora.png" alt="FACTURA"/><br>FACTURA </a></td>

                             <td width="150" align="center">
                                <a class="btnPrint" href='SinFactura.php?usuario=<?PHP echo $usuario; ?>&password=<?PHP echo $contrasena;?>&ci=<?PHP echo $nitci;?>&ingreso1=<?PHP echo $totalApagar;?>&ingreso2=<?PHP echo $efectivo;?>&resultado=<?PHP echo $cambio;?>'>
                                 <img src="<?php echo url(); ?>/Views/ticket/images/impresora.png" alt="FACTURA"/><br>SIN FACTURA </a></td>
                             <td width="150" align="center"><a href="NuevoPedidoController.php?usuario=<?PHP echo $usuario; ?>&password=<?PHP echo $contrasena;?>"><img src="<?php echo url(); ?>/Views/ticket/images/factura.png" alt="FACTURA"/><br> NUEVO PEDIDO </a></td>
                        </tr>
                    </table>

                    <table align="center" bgcolor="#ffffff" class="breadcrumb">

                        <tr>
                            <td>
                                <?PHP

                                echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  
         <font size="2"> <center> <b>' . $razonA . '</b>  </center></font>';
          echo '<center>De: ' . $propietarioA . '</center> ';
          echo '<center>Casa Matriz: ' . $direccionA . '</center> ';
          echo '<center>   N&ordm; ' . $nroA . ' - Telefono : ' . $telefonoA . '  </center> ';
          echo '<center>  COCHABAMBA</center> ';
          echo '<center>  FACTURA ORIGINAL</center> ';
          echo '------------------------------------------------------------------------------';
          echo '<br>';
          $facturaFinal = $facturalv + 1;
          ?>

                                <table>
                                    <tr>
                                        <td width="25"></td>
                                        <td><b>NIT : </b></td>
                                        <td><?PHP echo $nitNegocioA; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="25"></td>
                                        <td><b>N&ordm; FACTURA : </b></td>
                                        <td><?PHP echo $facturaFinal; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="25"></td>
                                        <td><b>N&ordm; AUTORIZACION : </b></td>
                                        <td><?PHP echo $autorizacionA; ?></td>
                                    </tr>
                                    <tr>
                                        <td width="25"></td>
                                        <td colspan="2" align="center"> Otros servicios de comidas</td>
                                    </tr>
                                </table>
                                <?PHP

                                echo '------------------------------------------------------------------------------';
                                echo '<br>';
                                ?>


                                <?PHP



                                date_default_timezone_set("America/Caracas");
                                $tiempo = getdate(time());
                                $dia = $tiempo['wday'];
                                $dia_mes = $tiempo['mday'];
                                $mes = $tiempo['mon'];
                                $year = $tiempo['year'];
                                $hora = $tiempo['hours'];
                                $minutos = $tiempo['minutes'];
                                $segundos = $tiempo['seconds'];


                                switch ($dia) {
                                    case "1":
                                        $dia_nombre = "Lunes";
                                        break;
                                    case "2":
                                        $dia_nombre = "Martes";
                                        break;
                                    case "3":
                                        $dia_nombre = "Miercoles";
                                        break;
                                    case "4":
                                        $dia_nombre = "Jueves";
                                        break;
                                    case "5":
                                        $dia_nombre = "Viernes";
                                        break;
                                    case "6":
                                        $dia_nombre = "Sabado";
                                        break;
                                    case "0":
                                        $dia_nombre = "Domingo";
                                        break;
                                }
                                switch ($mes) {
                                    case "1":
                                        $mes_nombre = "Enero";
                                        break;
                                    case "2":
                                        $mes_nombre = "Febrero";
                                        break;
                                    case "3":
                                        $mes_nombre = "Marzo";
                                        break;
                                    case "4":
                                        $mes_nombre = "Abril";
                                        break;
                                    case "5":
                                        $mes_nombre = "Mayo";
                                        break;
                                    case "6":
                                        $mes_nombre = "Junio";
                                        break;
                                    case "7":
                                        $mes_nombre = "Julio";
                                        break;
                                    case "8":
                                        $mes_nombre = "Agosto";
                                        break;
                                    case "9":
                                        $mes_nombre = "Septiembre";
                                        break;
                                    case "10":
                                        $mes_nombre = "Octubre";
                                        break;
                                    case "11":
                                        $mes_nombre = "Noviembre";
                                        break;
                                    case "12":
                                        $mes_nombre = "Diciembre";
                                        break;
                                }

                                ?>

                                <?PHP
                                $fechaH = date("d/m/Y");
                                $hora = " " . $hora . ": " . $minutos . ":" . $segundos;
                                echo '<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha  de Venta: </b>';
                                echo $fechaH;
                                $anioActual = date("Y");
                                $mesActual = date("m");
                                $diaActual = date("d");

                                echo '&nbsp;&nbsp;&nbsp;';
                                echo $hora;
                                echo '<br>';
                                ?>



                                <?php

                                echo '<table cellpadding="0"  cellspacing="0" width="260">';

                                for ($i = 0; $i < $numfilas; $i++) {
                                    $fila = mysqli_fetch_array($dataClienteFactura);

                                    echo '<tr><td><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nit/CI :</b>' . $fila['ci'] . '</td></tr>';
                                    echo '<tr><td><b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Se&ntilde;or(es):</b>' . $fila['nombre'] . '</td></tr>';
                                    $nit = $fila['ci'];
                                    $efectivo = $fila['totalApagar'];
                                    $efectivoT = $fila['efectivo'];

                                }
                                echo "</table>";
                                ?>

                                <?php

                                echo '<table cellpadding="0"  cellspacing="0" >';

                                echo '------------------------------------------------------------------------------';
                                echo ' <br>';
                                echo '<thead>
                                         <tr>
                                            <td width="40"></td>
                                            <td width="40"><b>Cant.</b></td>
                                            <td width="100"><b>Descripcion</b></td>
                                            <td width="70"><b>Precio</b></td>
                                             <td width="70"><b>Total</b></td>
                                             <td width="70"><b>Tipo</b></td>
                                          </tr>
                                        </thead>';
                                for ($i = 0; $i < $pedido; $i++) {
                                    $detallePedido = mysqli_fetch_array($pedidoTotalForFactura);
                                    echo '<td >' . '</td>';
                                    echo '<td>' . $detallePedido['cantidad'] . '</td>';
                                    echo '<td >' . $detallePedido['producto'] . '</td>';
                                    echo '<td>' . $detallePedido['precio'] . '</td>';
                                    echo '<td>' . $detallePedido['precio'] * $detallePedido['cantidad'] . '</td>';
                                    echo '<td>'.$detallePedido['tipo'].'</td>';

                                    echo ' </tr>';


                                }
                                echo "</table>";
                                ?>

                                <?php
                                            while ($datosUsuarioMoneda = mysqli_fetch_array($dataMoneda)) {
                                                $contexto= $datosUsuarioMoneda['contexto'];
                                                $tipoMoneda= $datosUsuarioMoneda['tipoMoneda'];
                                            }
                                ?>
                                <?PHP
                                echo '<table cellpadding="0"  cellspacing="0" width="260">';

                                for ($i = 0; $i < $totalNroVentaPedido; $i++) {
                                    $detalleTotalPedido = mysqli_fetch_array($ventaTotalPedido);

                                    echo '------------------------------------------------------------------------------';
                                    echo ' <br>';
                                    echo '<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total a Pagar: '.$tipoMoneda.'</b> ';
                                    $totalVenta =$monto;
                                    echo $totalVenta;
                                    echo '<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b> ';
                                    echo ' <br>';
                                    echo '<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Efectivo :'.$tipoMoneda.'</b> ';
                                    echo $efectivoT;
                                    echo '<b>&nbsp;&nbsp;&nbsp;</b> ';
                                    echo ' <br>';
                                    echo '<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cambio : '.$tipoMoneda.'</b> ';

                                    $cambio = $efectivoT - $totalVenta;
                                    echo $cambio;
                                    echo ' <br>';

                                }

                                echo '</tr>';
                                echo "</table>";
                                ?>


                                <?php

                                class EnLetras
                                {
                                    var $Void = "";
                                    var $SP = " ";
                                    var $Dot = ".";
                                    var $Zero = "0";
                                    var $Neg = "Menos";

                                    function ValorEnLetras($x, $Moneda)
                                    {
                                        $s = "";
                                        $Ent = "";
                                        $Frc = "";
                                        $Signo = "";

                                        if (floatVal($x) < 0)
                                            $Signo = $this->Neg . " ";
                                        else
                                            $Signo = "";

                                        if (intval(number_format($x, 2, '.', '')) != $x) //<- averiguar si tiene decimales
                                            $s = number_format($x, 2, '.', '');
                                        else
                                            $s = number_format($x, 2, '.', '');

                                        $Pto = strpos($s, $this->Dot);

                                        if ($Pto === false) {
                                            $Ent = $s;
                                            $Frc = $this->Void;
                                        } else {
                                            $Ent = substr($s, 0, $Pto);
                                            $Frc = substr($s, $Pto + 1);
                                        }

                                        if ($Ent == $this->Zero || $Ent == $this->Void)
                                            $s = "Cero ";
                                        elseif (strlen($Ent) > 7) {
                                            $s = $this->SubValLetra(intval(substr($Ent, 0, strlen($Ent) - 6))) .
                                                "Millones " . $this->SubValLetra(intval(substr($Ent, -6, 6)));
                                        } else {
                                            $s = $this->SubValLetra(intval($Ent));
                                        }

                                        if (substr($s, -9, 9) == "Millones " || substr($s, -7, 7) == "Millón ")
                                            $s = $s . "de ";

                                        $s = $s . $Moneda;

                                        if ($Frc != $this->Void) {
                                            $s = $s . " " . $Frc . "/100";
                                            //$s = $s . " " . $Frc . "/100";
                                        }
                                        $letrass = $Signo . $s . " ";
                                        return ($Signo . $s . " ");

                                    }


                                    function SubValLetra($numero)
                                    {
                                        $Ptr = "";
                                        $n = 0;
                                        $i = 0;
                                        $x = "";
                                        $Rtn = "";
                                        $Tem = "";

                                        $x = trim("$numero");
                                        $n = strlen($x);

                                        $Tem = $this->Void;
                                        $i = $n;

                                        while ($i > 0) {
                                            $Tem = $this->Parte(intval(substr($x, $n - $i, 1) .
                                                str_repeat($this->Zero, $i - 1)));
                                            If ($Tem != "Cero")
                                                $Rtn .= $Tem . $this->SP;
                                            $i = $i - 1;
                                        }


                                        //--------------------- GoSub FiltroMil ------------------------------
                                        $Rtn = str_replace(" Mil Mil", " Un Mil", $Rtn);
                                        while (1) {
                                            $Ptr = strpos($Rtn, "Mil ");
                                            If (!($Ptr === false)) {
                                                If (!(strpos($Rtn, "Mil ", $Ptr + 1) === false))
                                                    $this->ReplaceStringFrom($Rtn, "Mil ", "", $Ptr);
                                                Else
                                                    break;
                                            } else break;
                                        }

                                        //--------------------- GoSub FiltroCiento ------------------------------
                                        $Ptr = -1;
                                        do {
                                            $Ptr = strpos($Rtn, "Cien ", $Ptr + 1);
                                            if (!($Ptr === false)) {
                                                $Tem = substr($Rtn, $Ptr + 5, 1);
                                                if ($Tem == "M" || $Tem == $this->Void)
                                                    ;
                                                else
                                                    $this->ReplaceStringFrom($Rtn, "Cien", "Ciento", $Ptr);
                                            }
                                        } while (!($Ptr === false));

                                        //--------------------- FiltroEspeciales ------------------------------
                                        $Rtn = str_replace("Diez Un", "Once", $Rtn);
                                        $Rtn = str_replace("Diez Dos", "Doce", $Rtn);
                                        $Rtn = str_replace("Diez Tres", "Trece", $Rtn);
                                        $Rtn = str_replace("Diez Cuatro", "Catorce", $Rtn);
                                        $Rtn = str_replace("Diez Cinco", "Quince", $Rtn);
                                        $Rtn = str_replace("Diez Seis", "Dieciseis", $Rtn);
                                        $Rtn = str_replace("Diez Siete", "Diecisiete", $Rtn);
                                        $Rtn = str_replace("Diez Ocho", "Dieciocho", $Rtn);
                                        $Rtn = str_replace("Diez Nueve", "Diecinueve", $Rtn);
                                        $Rtn = str_replace("Veinte Un", "Veintiun", $Rtn);
                                        $Rtn = str_replace("Veinte Dos", "Veintidos", $Rtn);
                                        $Rtn = str_replace("Veinte Tres", "Veintitres", $Rtn);
                                        $Rtn = str_replace("Veinte Cuatro", "Veinticuatro", $Rtn);
                                        $Rtn = str_replace("Veinte Cinco", "Veinticinco", $Rtn);
                                        $Rtn = str_replace("Veinte Seis", "Veintiseís", $Rtn);
                                        $Rtn = str_replace("Veinte Siete", "Veintisiete", $Rtn);
                                        $Rtn = str_replace("Veinte Ocho", "Veintiocho", $Rtn);
                                        $Rtn = str_replace("Veinte Nueve", "Veintinueve", $Rtn);

                                        //--------------------- FiltroUn ------------------------------
                                        If (substr($Rtn, 0, 1) == "M") $Rtn = "Un " . $Rtn;
                                        //--------------------- Adicionar Y ------------------------------
                                        for ($i = 65; $i <= 88; $i++) {
                                            If ($i != 77)
                                                $Rtn = str_replace("a " . Chr($i), "* y " . Chr($i), $Rtn);
                                        }
                                        $Rtn = str_replace("*", "a", $Rtn);
                                        return ($Rtn);
                                    }


                                    function ReplaceStringFrom(&$x, $OldWrd, $NewWrd, $Ptr)
                                    {
                                        $x = substr($x, 0, $Ptr) . $NewWrd . substr($x, strlen($OldWrd) + $Ptr);
                                    }


                                    function Parte($x)
                                    {
                                        $Rtn = '';
                                        $t = '';
                                        $i = '';
                                        Do {
                                            switch ($x) {
                                                Case 0:
                                                    $t = "Cero";
                                                    break;
                                                Case 1:
                                                    $t = "Un";
                                                    break;
                                                Case 2:
                                                    $t = "Dos";
                                                    break;
                                                Case 3:
                                                    $t = "Tres";
                                                    break;
                                                Case 4:
                                                    $t = "Cuatro";
                                                    break;
                                                Case 5:
                                                    $t = "Cinco";
                                                    break;
                                                Case 6:
                                                    $t = "Seis";
                                                    break;
                                                Case 7:
                                                    $t = "Siete";
                                                    break;
                                                Case 8:
                                                    $t = "Ocho";
                                                    break;
                                                Case 9:
                                                    $t = "Nueve";
                                                    break;
                                                Case 10:
                                                    $t = "Diez";
                                                    break;
                                                Case 20:
                                                    $t = "Veinte";
                                                    break;
                                                Case 30:
                                                    $t = "Treinta";
                                                    break;
                                                Case 40:
                                                    $t = "Cuarenta";
                                                    break;
                                                Case 50:
                                                    $t = "Cincuenta";
                                                    break;
                                                Case 60:
                                                    $t = "Sesenta";
                                                    break;
                                                Case 70:
                                                    $t = "Setenta";
                                                    break;
                                                Case 80:
                                                    $t = "Ochenta";
                                                    break;
                                                Case 90:
                                                    $t = "Noventa";
                                                    break;
                                                Case 100:
                                                    $t = "Cien";
                                                    break;
                                                Case 200:
                                                    $t = "Doscientos";
                                                    break;
                                                Case 300:
                                                    $t = "Trescientos";
                                                    break;
                                                Case 400:
                                                    $t = "Cuatrocientos";
                                                    break;
                                                Case 500:
                                                    $t = "Quinientos";
                                                    break;
                                                Case 600:
                                                    $t = "Seiscientos";
                                                    break;
                                                Case 700:
                                                    $t = "Setecientos";
                                                    break;
                                                Case 800:
                                                    $t = "Ochocientos";
                                                    break;
                                                Case 900:
                                                    $t = "Novecientos";
                                                    break;
                                                Case 1000:
                                                    $t = "Mil";
                                                    break;
                                                Case 1000000:
                                                    $t = "Millón";
                                                    break;
                                            }

                                            If ($t == $this->Void) {
                                                $i = $i + 1;
                                                $x = $x / 1000;
                                                If ($x == 0) $i = 0;
                                            } else
                                                break;

                                        } while ($i != 0);

                                        $Rtn = $t;
                                        Switch ($i) {
                                            Case 0:
                                                $t = $this->Void;
                                                break;
                                            Case 1:
                                                $t = " Mil";
                                                break;
                                            Case 2:
                                                $t = " Millones";
                                                break;
                                            Case 3:
                                                $t = " Billones";
                                                break;
                                        }
                                        return ($Rtn . $t);
                                    }

                                }


                                ?>


                                <table>
                                    <tr>
                                        <td width="30"></td>
                                        <td align="center">
                                            <b>Son </b><?php
                                            $totalLiteral = $totalVenta;
                                            $V = new EnLetras();

                                            $con_letra = strtoupper($V->ValorEnLetras($totalLiteral, $contexto));
                                            echo "<b>" . $con_letra . "</b>";
                                            ?>
                                        </td>
                                    </tr>
                                </table>

                                <?PHP
                                echo '<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Codigo de Control : </b>';
                                echo $codigoControl . '<br/>';

                                $partes = explode("-", $fechaL);

                                $anio = $partes[0];
                                $mes = $partes[1];
                                $dia = $partes[2];
                                $mesRegistro = $dia . '/' . $mes . '/' . $anio;

                                echo '<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha Limite de Emision : </b>';
                                echo $mesRegistro;

                                ?>


                                <table>
                                    <tr>
                                        <td width="100"></td>
                                        <td align="center">
                                            <?PHP
                                             include("Qr.php");
                                            ?>
                                        </td>
                                    </tr>
                                </table>

                                <?php


                                for ($i = 0; $i < $totalNroVentaPedidoDia; $i++) {
                                    $ventasTotalDia = mysqli_fetch_array($ventasTotalDiaCurrent);
                                    $totalVentaFicha = $ventasTotalDia['maxid'];
                                }

                                echo '------------------------------------------------------------------------------';
                                echo '<center> " Esta factura conttribuye al desarrollo del pais el uso   </center>';
                                echo '<center> ilicito de esta sera  sancionado de acuerdo a ley "</center>';
                                echo '------------------------------------------------------------------------------';


                                echo '<br>';
                                echo 'Atendido por : ';
                                echo '<b>';
                                echo $atendido;
                                echo ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                echo '</b>';
                                echo ' FICHA NRO : ';
                                echo $totalVentaFicha;
                                // echo '<b>'; echo $apellidos; echo '</b>';echo '<br>';
                                ?>

                            </td>
                        </tr>
                    </table>


                </div>
            </div>