  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Sistema para el Menejo de Empresas - contabilidad , ventas , ingresos , egresos">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Sistema modo tactil , para escritorio tabletas ">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>iCONT - Sistema Profesional Para Empresas</title>




    <!-- Bootstrap CSS -->
    <link href="<?php echo url(); ?>/Views/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="<?php echo url(); ?>/Views/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="<?php echo url(); ?>/Views/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="<?php echo url(); ?>/Views/css/font-awesome.min.css" rel="stylesheet" />
    <!-- full calendar css-->
    <link href="<?php echo url(); ?>/Views/css/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="<?php echo url(); ?>/Views/css/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
      <!-- <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>-->
       <!-- owl carousel -->
    <link rel="stylesheet" href="<?php echo url(); ?>/Views/css/owl.carousel.css" type="text/css">
	<link href="<?php echo url(); ?>/Views/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="<?php echo url(); ?>/Views/css/fullcalendar.css">
	<link href="<?php echo url(); ?>/Views/css/widgets.css" rel="stylesheet">
    <link href="<?php echo url(); ?>/Views/css/style.css" rel="stylesheet">
	<link href="<?php echo url(); ?>/Views/css/style_pru.css" rel="stylesheet">
    <link href="<?php echo url(); ?>/Views/css/style-responsive.css" rel="stylesheet" />
	<link href="<?php echo url(); ?>/Views/css/xcharts.min.css" rel=" stylesheet">
	<link href="<?php echo url(); ?>/Views/css/jquery-ui-1.10.4.min.css" rel="stylesheet">

	<link href="<?php echo url(); ?>/Views/css/anchomodal.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="<?php echo url(); ?>/Views/js/html5shiv.js"></script>
      <script src="<?php echo url(); ?>/Views/js/respond.min.js"></script>
      <script src="<?php echo url(); ?>/Views/js/lte-ie7.js"></script>
    <![endif]-->
    <!-- Subir Imagen -->
	<link href="<?php echo url(); ?>/Views/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <!-- Calendario -->
    <link href="<?php echo url(); ?>/Views/css/calendario.css" rel="stylesheet">
      <!--<link href="css/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.css" rel="stylesheet">-->
     <link href="<?php echo url(); ?>/Views/js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
     <!-- Ventana modal -->
	<link rel="stylesheet" href="<?php echo url(); ?>/Views/css/basicModal.min.css">

	<!-- Ventana modal style
	<link rel="stylesheet" type="text/css" href="css/default.css" />        -->
	<link rel="stylesheet" type="text/css" href="<?php echo url(); ?>/Views/css/component.css" />

	<link rel="stylesheet" href="<?php echo url(); ?>/Views/css/ngDialog.css">
    <link rel="stylesheet" href="<?php echo url(); ?>/Views/css/ngDialog-theme-default.css">
    <link rel="stylesheet" href="<?php echo url(); ?>/Views/css/ngDialog-theme-plain.css">
    <link rel="stylesheet" href="<?php echo url(); ?>/Views/css/ngDialog-custom-width.css">


	 <link href='<?php echo url(); ?>/Views/css/adaptive-modal.css' rel='stylesheet' type='text/css'>

	 <!-- letra style -->
      <!-- <link rel="stylesheet" href="css/letra.css">-->


    </head>