<?php
require "../Model/ModelUrl.php";
?>

<!DOCTYPE html>
<html lang="en">
<?php
include("head.php");
?>




<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () {
            Shadowbox.open({
                content: '<div><img src="<?php echo url() . "/Views/popup/welcome.jpg"?>" ></div>',
                player: "html",
                title: "Hola !!! ",
                width: 450,
                height: 201
            });
        }, 50);
    });
</script>
<style type="text/css">

    .html, body {
        font-family: Verdana, Geneva, sans-serif;
        font-size: 12px;
    }

    .ejemplo {
        float: left;
        width: 100%;
        padding: 0px;
        margin: 0px;
    }

    .ejemplo img {
        float: left;
        padding: 2px;
        border: 1px solid #999;
        margin-right: 10px;
        margin-bottom: 10px;
    }

</style>
<body>
<!-- container section start -->
<section id="container" class="">


    <header class="header dark-bg">
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i
                        class="icon_menu"></i></div>
        </div>

        <?PHP include("logo.php"); ?>

        <div class="nav search-row" id="top_menu">
            <!--  search form start -->
            <ul class="nav top-menu">
                <li>
                    <form class="navbar-form">
<!--                        <input class="form-control" placeholder="Search" type="text">-->
                    </form>
                </li>
            </ul>
            <!--  search form end -->
        </div>
        <?PHP include("DropDown.php"); ?>
    </header>
    <?PHP include("menu.php"); ?>
    </div>
    </aside>

    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!--overview start-->
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><i class="fa fa-laptop"></i> PRINCIPAL</h3>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="principal.php">Inicio</a></li>
                        <li><i class="fa fa-laptop"></i> Principal</li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="info-box blue-bg">
                        <i class="fa fa-truck"></i>
                        <div class="count"><?PHP
                            // echo $t_pro;

                            ?></div>
                        <div class="title"> Proveedores</div>
                    </div><!--/.info-box-->
                </div><!--/.col-->

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="info-box brown-bg">
                        <i class="icon_piechart"></i>
                        <div class="count"><span style="font-size: xx-small; "><?PHP
                                //echo $ventastotales;
                                ?> </span>
                        </div>
                        <div class="title"> Reportes de Ventas </div>
                    </div><!--/.info-box-->
                </div><!--/.col-->

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="info-box dark-bg">
                        <i class="fa fa-money"></i>
                        <div class="count"><?PHP
                            //echo $gastototales;
                            ?>$us.
                        </div>
                        <div class="title">Gastos y Entradas</div>
                    </div><!--/.info-box-->
                </div><!--/.col-->

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="info-box green-bg">
                        <i class="fa fa-cubes"></i>
                        <div class="count"><?PHP
                            //echo $totalProducto;
                            ?></div>
                        <div class="title">Stock de los productos</div>
                    </div><!--/.info-box-->
                </div><!--/.col-->

            </div><!--/.row-->

            <div class="row">

                <div class="col-lg-9 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2><i class="fa fa-flag-o red"></i><strong>Stock del Producto</strong></h2>

                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                    <tr>
                                        <th>IMAGEN</th>
                                        <th>PRODUCTO</th>
                                        <th>PRECIO DE COMPRA</th>
                                        <th>STOCK</th>
                                        <th>CANTIDAD VENDIDA</th>
                                        <th>PORCENTAJE VENDIDO</th>
                                    </tr>
                                    </thead>
                                    <?php

                                    while ($stockProduct = mysqli_fetch_array($stock)) {

                                        $cantidad = $stockProduct['cantidadVendido'];
                                        $alarma = $stockProduct['alarma'];
                                        if ($alarma > $cantidad) {
                                            ?>

                                            <tr>
                                                <td><img src="<?PHP echo url();
                                                    echo '/Views/';
                                                    echo $stockProduct['imagen']; ?>" width="75" height="75"></td>
                                                </td>
                                                <td><?php echo $stockProduct['nombreProducto']; ?></td>
                                                <td><?php echo $stockProduct['precioCompra']; ?></td>
                                                <td><?php echo $stockProduct['cantidadStock']; ?></td>
                                                <td><?php echo $stockProduct['cantidadVendido']; ?></td>
                                                <td>
                                                    <div class="percent"><?php echo $stockProduct['porcentaje']; ?>%</div>
                                                    <div class="progress progress-striped progress-sm">
                                                        <?php if ($stockProduct['porcentaje'] >= 50) {
                                                            echo '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow=' . $stockProduct['porcentaje'] . ' aria-valuemin="0" aria-valuemax="100" style="width: ' . $stockProduct['porcentaje'] . '%"></div>';
                                                        } else {
                                                            if ($stockProduct['porcentaje'] >= 10 && $stockProduct['porcentaje'] < 50) {
                                                                echo '<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow=' . $stockProduct['porcentaje'] . ' aria-valuemin="0" aria-valuemax="100" style="width: ' . $stockProduct['porcentaje'] . '%"></div>';
                                                            } else {
                                                                echo '<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow=' . $stockProduct['porcentaje'] . ' aria-valuemin="0" aria-valuemax="100" style="width: ' . $stockProduct['porcentaje'] . '%"></div>';
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                    <span class="sr-only">30%</span>
                                                </td>
                                            </tr>


                                            <?php

                                        }
                                    }
                                    ?>


                                </table>
                            </div>
                        </div>

                    </div>

                </div><!--/col-->
                <div class="col-md-3">

                    <!-- Calendario -->
                    <center><h3>Calendario</h3></center>
                    <div id="calendar" class="mb">
                        <div class="panel green-panel no-margin">
                            <div class="panel-body">
                                <div id="date-popover" class="popover top"
                                     style="cursor: pointer; disadding: block; margin-left: 33%; margin-top: -50px; width: 175px;">
                                    <div class="arrow"></div>
                                    <h3 class="popover-title" style="disadding: none;"></h3>
                                    <div id="date-popover-content" class="popover-content"></div>
                                </div>
                                <div id="my-calendar"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin de calendar -->
                    <!----- CALENDARIO ---->
                    <div class="col1of2">
                        <div class="datepicker-placeholder"></div>
                    </div>
                    <!----- FIN CALENDARIO ---->

                </div>

                <!-- statics end -->

        </section>
    </section>
    <!--main content end-->
</section>
<!-- container section start -->

<!-- javascripts -->

<script src="<?php echo url(); ?>/Views/js/jquery.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery-ui-1.10.4.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo url(); ?>/Views/js/jquery-ui-1.9.2.custom.min.js"></script>
<!-- bootstrap -->
<script src="<?php echo url(); ?>/Views/js/bootstrap.min.js"></script>
<!-- nice scroll -->
<script src="<?php echo url(); ?>/Views/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery.nicescroll.js" type="text/javascript"></script>
<!-- charts scripts -->

<script src="<?php echo url(); ?>/Views/js/jquery.sparkline.js" type="text/javascript"></script>
<script src="<?php echo url(); ?>/Views/js/owl.carousel.js"></script>

<!--script for this page only-->
<!-----------------------------------<script src="js/calendar-custom.js"></script>----->
<script src="<?php echo url(); ?>/Views/js/jquery.rateit.min.js"></script>
<!-- custom select -->
<script src="<?php echo url(); ?>/Views/js/jquery.customSelect.min.js"></script>


<!--custome script for all page-->
<script src="<?php echo url(); ?>/Views/js/scripts.js"></script>
<!-- custom script for this page-->
<script src="<?php echo url(); ?>/Views/js/sparkline-chart.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?php echo url(); ?>/Views/js/xcharts.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery.autosize.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery.placeholder.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/gdp-data.js"></script>
<script src="<?php echo url(); ?>/Views/js/morris.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/sparklines.js"></script>
<script src="<?php echo url(); ?>/Views/js/charts.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery.slimscroll.min.js"></script>


<script src="<?php echo url(); ?>/Views/js/zabuto_calendar.js"></script>


<script type="application/javascript">
    $(document).ready(function () {
        $("#my-calendar").zabuto_calendar({
            language: "es",
            today: true,
            nav_icon: {
                prev: '<i class="fa fa-chevron-circle-left"></i>',
                next: '<i class="fa fa-chevron-circle-right"></i>'
            }
        });
    });
</script>

<!-- DataTables JavaScript -->
<script src="<?php echo url(); ?>/Views/js/jquery.dataTables.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/dataTables.bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>

</body>
</html>