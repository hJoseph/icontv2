<?php
require "../Model/ModelUrl.php";
?>

<!DOCTYPE html>
<html lang="en">
<?php
include("head.php");
?>

<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () {
            Shadowbox.open({
                content: '<div><img src="<?php echo url() . "/Views/popup/welcome.jpg"?>" ></div>',
                player: "html",
                title: "Hola !!! ",
                width: 450,
                height: 201
            });
        }, 50);
    });
</script>
<style type="text/css">

    .html, body {
        font-family: Verdana, Geneva, sans-serif;
        font-size: 12px;
    }

    .ejemplo {
        float: left;
        width: 100%;
        padding: 0px;
        margin: 0px;
    }

    .ejemplo img {
        float: left;
        padding: 2px;
        border: 1px solid #999;
        margin-right: 10px;
        margin-bottom: 10px;
    }

</style>
<body>
<!-- container section start -->
<section id="container" class="">


    <header class="header dark-bg">
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i
                        class="icon_menu"></i></div>
        </div>

        <?PHP include("logo.php"); ?>

        <div class="nav search-row" id="top_menu">
            <!--  search form start -->
            <ul class="nav top-menu">
                <li>
                    <form class="navbar-form">
<!--                        <input class="form-control" placeholder="Search" type="text">-->
                    </form>
                </li>
            </ul>
            <!--  search form end -->
        </div>
        <?PHP include("DropDown.php"); ?>
    </header>
    <?PHP include("menu.php"); ?>
    </div>
    </aside>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><i class="fa fa-truck"></i><strong> PEDIDOS POR LA EMPRESA </strong></h3>
                    <div class="<?php echo $alerta; ?>" role="alert">
                        <b><?php echo $messageAlerta; ?> </b>
                    </div>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="principal.php">Inicio</a></li>
                        <li><i class="fa fa-truck"></i>Consolidar Venta</li>
                    </ol>
                </div>
            </div>

            <!--modal start-->

            <header class="panel-heading">
                <div class="panel-body">
                    <div align="right">
                        <button href="#add" title="" data-placement="left" data-toggle="modal"
                                class="btn btn-primary tooltips" type="button" data-original-title="Nuevo Gasto"><span
                                    class="fa fa-plus"></span> AGREGAR NUEVO PEDIDO
                        </button>
                    </div>
                    <div id="add" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                         aria-hidden="true">
                        <form class="form-validate form-horizontal" name="form2" action="RegistroPedido.php"
                              method="post">
                            <input name="usuarioLogin" value="<?php echo $usuario; ?>" type="hidden">
                            <input name="passwordLogin" value="<?php echo $password; ?>" type="hidden">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                        </button>
                                        <h3 id="myModalLabel" align="center">Registrar Nuevo Pedido</h3>
                                    </div>

                                    <div class="modal-body">
                                        <label class="col-sm-2 control-label"> Empresa :</label>
                                        <div class="col-sm-4">
                                            <select class="form-control input-lg m-bot15"
                                                    name="proveedor">
                                                <?php
                                                while ($proveedor = mysqli_fetch_array($allProveedor)) {
                                                    echo '<option value="' . $proveedor['proveedor'] . '">' . $proveedor['proveedor'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <br><br><br>
                                        <label for="descripcion" class="control-label col-lg-2">Detalle/Cant.:</label>
                                        <div class="col-lg-10">
                                            <input class="form-control input-lg m-bot15" id="descripcion"
                                                   name="descripcion"
                                                   minlength="3" type="text" required/>
                                        </div>
                                        <br><br><br>
                                        <label for="total" class="control-label col-lg-2">Total :</label>
                                        <div class="col-lg-10">
                                            <input class="form-control input-lg m-bot15" id="total" name="total"
                                                   type="text" required/>
                                        </div>
                                        <br><br>
                                        <label for="fecha" class="control-label col-lg-2"> Fecha:</label>
                                        <div class="col-lg-10">
                                            <input class="form-control input-lg m-bot15" type="date"
                                                   name="fechaRegistro" autocomplete="off" required
                                                   value="<?php echo date('Y-m-d'); ?>">
                                        </div>
                                        <br><br><br><br>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><strong>Cerrar</strong>
                                        </button>
                                        <button name="nuevo_Pedido" type="submit" class="btn btn-primary">
                                            <strong>Registrar</strong></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </header>

            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th><i class="icon_profile"></i> FECHA</th>
                            <th><i class="icon_briefcase_alt"></i> DETALLE PRDUCTO</th>
                            <th><i class="icon_profile"></i> CANTIDAD</th>
                            <th><i class="icon_profile"></i> TOTAL Sus.</th>
                            <th><i class="icon_contacts_alt"></i> CLIENTE</th>
                            <th><i class="icon_contacts_alt"></i> CODIGO CONTROL</th>
                            <th><i class="icon_contacts_alt"></i> COMMENTARIO</th>
                            <th><i class="icon_contacts_alt"></i> ESTADO</th>
                            <th><i class="icon_cog"></i> ACCIONES</th>
                        </tr>
                        </thead>
                        <?php

                        while ($consolidado = mysqli_fetch_array($allVentas)) {
                            ?>

                            <tr>
                                <td><?php echo $consolidado['fecha']; ?></td>
                                <td><?php echo $consolidado['producto']; ?></td>
                                <td><?php echo $consolidado['cantidad']; ?></td>
                                <td><?php echo $consolidado['total']; ?></td>
                                <td><?php echo $consolidado['nombre']; ?></td>
                                <td><?php echo $consolidado['codTransaccion']; ?></td>
                                <td><?php echo $consolidado['commentario']; ?></td>
                                <td><?php echo $consolidado['estadoTransaccion']; ?></td>


                                <td>
                                    <a href="#a<?php echo $consolidado[0]; ?>" role="button" class="btn btn-warning"
                                       data-toggle="modal"><i class="icon_pencil-edit_alt"></i></a>
                                    <a href="ConsolidarPedido.php?idConsolidar=<?php echo $consolidado[0]; ?>&usuarioLogin=<?php echo $usuario; ?>&passwordLogin=<?php echo $password; ?>"
                                       class="btn btn-success"><i class="icon_box-checked"></i></a>
                                </td>
                            </tr>


                            <div id="a<?php echo $consolidado[0]; ?>" class="modal fade" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true">
                                <form class="form-validate form-horizontal" name="form2" action="ConsolidarPedido.php"
                                      method="post">
                                    <input name="usuarioLogin" value="<?php echo $usuario;?>" type="hidden" >
                                    <input name="passwordLogin" value="<?php echo $password;?>" type="hidden" >
                                    <input type="hidden" name="idConsolidar"
                                           value="<?php echo $consolidado['ventasDetalleId']; ?>">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">×
                                                </button>
                                                <h3 id="myModalLabel" align="center">Escribir Comentario</h3>
                                            </div>

                                            <div class="modal-body">
                                                <div class="form-group ">
                                                    <label for="responsable"
                                                           class="control-label col-lg-2">Detalle de la venta:</label>
                                                    <div class="col-lg-10">
                                                        <input class="form-control input-lg m-bot15" type="text"  name="comentario" >
                                                    </div>
                                                </div>
                                            </div>

                                            <br>
                                            <div class="modal-footer">
                                                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">
                                                    <strong>Cerrar</strong></button>
                                                <button name="consolidar" type="submit" class="btn btn-primary"><strong>Editar</strong>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        <?php } ?>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
        </section>
    </section>
    <!--main content end-->
</section>
<!-- container section end -->
<!-- javascripts -->
<script src="<?php echo url(); ?>/Views/js/jquery.js"></script>
<script src="<?php echo url(); ?>/Views/js/bootstrap.min.js"></script>
<!-- nicescroll -->
<script src="<?php echo url(); ?>/Views/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery.nicescroll.js" type="text/javascript"></script>
<!--custome script for all page-->
<script src="<?php echo url(); ?>/Views/js/scripts.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo url(); ?>/Views/js/jquery.dataTables.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/dataTables.bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>


</body>
</html>