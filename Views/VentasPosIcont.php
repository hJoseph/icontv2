<?php
require "../Model/ModelUrl.php";
?>


<!DOCTYPE html>
<html lang="es">
<?php include("head.php"); ?>
<!--<script language="JavaScript" type="text/javascript" src="-->
<?php //echo url(); ?><!--/Views/js/ajaxTrasacction.js"></script>-->
<body>

<!-- container section start -->
<section id="container" class="sidebar-closed">
    <!--header start-->
    <header class="header dark-bg">

        <style>
            .textbox {
                border: 1px solid #6a98e6;
                font-size: 23px;
                font-family: Arial, Verdana;
                padding-left: 7px;
                padding-right: 7px;
                padding-top: 10px;
                padding-bottom: 1px;
                border-radius: 4px;
                -moz-border-radius: 4px;
                -webkit-border-radius: 4px;
                -o-border-radius: 4px;
                background: #FFFFFF;
                background: linear-gradient(left, #FFFFFF, #6a98e6);
                background: -moz-linear-gradient(left, #FFFFFF, #F7F9FA);
                background: -webkit-linear-gradient(left, #FFFFFF, #F7F9FA);
                background: -o-linear-gradient(left, #FFFFFF, #F7F9FA);
                color: #2E3133;
            }

            .textbox:hover {
                color: #2E3133;
                border-color: #ff0000;
            }
        </style>
        <style>
            .textbox2 {
                border: 1px solid #6a98e6;
                font-size: 23px;
                font-family: Arial, Verdana;
                padding-left: 7px;
                padding-right: 7px;
                padding-top: 0px;
                padding-bottom: 1px;
                border-radius: 4px;
                -moz-border-radius: 4px;
                -webkit-border-radius: 4px;
                -o-border-radius: 4px;
                background: #FFFFFF;
                background: linear-gradient(left, #FFFFFF, #6a98e6);
                background: -moz-linear-gradient(left, #FFFFFF, #F7F9FA);
                background: -webkit-linear-gradient(left, #FFFFFF, #F7F9FA);
                background: -o-linear-gradient(left, #FFFFFF, #F7F9FA);
                color: #2E3133;
            }

            .textbox2:hover {
                color: #2E3133;
                border-color: #ff0000;
            }
        </style>
        <style>
            p:hover {
                background-color: yellow;
                border-color: #ff0000;
            }
        </style>
        <style type="text/css">
            a.nounderline:link {
                text-decoration: none;
            }
        </style>
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i
                        class="icon_menu"></i></div>
        </div>
        <?PHP include("logo.php"); ?>
        <div class="nav search-row" id="top_menu">
            <!--  search form start -->
            <ul class="nav top-menu">
                <li>
                    <form class="navbar-form">
<!--                        <input class="form-control" placeholder="Search" type="text">-->
                    </form>
                </li>
            </ul>
            <!--  search form end -->
        </div>
        <?PHP include("DropDown.php"); ?>
    </header>
    <!--header end-->
    <?PHP include("menu.php"); ?>
    </div>
    </aside>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <!-- page start-->
                        <div class="row">
                            <div class="col-sm-7">
                                <section class="panel">
                                    <div class="row">
                                        <header class="panel-heading tab-bg-primary ">
                                            <?PHP if ($tipoUsuario == 'ADMINISTRADOR') {
                                                include("menuTabs.php");
                                            } ?>

                                            <?PHP if ($tipoUsuario == 'VENTAS') {
                                                include("menuTabs.php");
                                            } ?>
                                        </header>
                                        <div class="panel-body">
                                            <div class="tab-content">
                                                <div id="home" class="tab-pane active">
                                                    <div style="OVERFLOW: auto; HEIGHT: 680px">
                                                        <table class="table table-striped table-bordered table-hover">
                                                            <thead>
                                                            <br>
                                                            <tr class="success" >
                                                                <?PHP
                                                                include("Producto.php");
                                                                ?>
                                                            </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                </section>

                            </div>


                            <div class="col-sm-5">

                                <div id="resultado">
                                    <?php
                                    include('pedido.php');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </section>
    </section>

    <!--main content end-->
</section>
<!-- container section end -->


<!--------------   HOLA ESTO ES PARA MODIFICAR EL BUSCADOR JQUERY  ------------------->


<script src="<?php echo url(); ?>/Views/js/jquery.js"></script>
<script src="<?php echo url(); ?>/Views/js/bootstrap.min.js"></script>

<!--custome script for all page-->
<script src="<?php echo url(); ?>/Views/js/scriptsb.js"></script>

<!-- DataTables JavaScript -->
<!--<script src="--><?php //echo url(); ?><!--/Views/js/jquery.dataTables.min.js"></script>-->
<!--<script src="--><?php //echo url(); ?><!--/Views/js/dataTables.bootstrap.min.js"></script>-->

<!-- nicescroll -->
<!--<script src="--><?php //echo url(); ?><!--/Views/js/jquery.scrollTo.min.js"></script>-->
<script src="<?php echo url(); ?>/Views/js/jquery.nicescroll.js" type="text/javascript"></script>


<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<!--<script language="JavaScript" type="text/javascript" src="-->
<?php //echo url(); ?><!--/Views/js/ajaxPos.js"></script>-->

<script language="JavaScript" type="text/javascript">


    function objetoAjax() {
        var xmlhttp = false;
        try {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (E) {
                xmlhttp = false;
            }
        }

        if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
            xmlhttp = new XMLHttpRequest();
        }
        return xmlhttp;
    }


    //////////////// ACTUALIZAR DATOSss //////////////

    function editarPreventa(idproducto, tipo) {
        //donde se mostrar� el formulario con los datos
        divFormulario = document.getElementById('formulario');
        //instanciamos el objetoAjax
        ajax = objetoAjax();
        ajax.open("POST", "EditPreventa.php");
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4) {
                //mostrar resultados en esta capa
                divFormulario.innerHTML = ajax.responseText
                //mostrar el formulario
                divFormulario.style.display = "block";
            }
        }
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        //enviando los valores
        ajax.send("idproducto=" + idproducto + "&tipo=" + tipo)
    }


    /////////////////// ELIMINAR UNPRODUCTO DE LA PREVENTA//////////////////////

    function deleteDatoP(idproducto) {
        //donde se mostrar� el resultado de la eliminacion
        divResultado = document.getElementById('resultado');
        ajax = objetoAjax();
        ajax.open("GET", "DeleteOnlyPreVenta.php?idproducto=" + idproducto);
        location.reload();
        ajax.onreadystatechange = function () {
            // TODO revisar esto porque el estado es 4 no 1
            // if (ajax.readyState==4) {
            if (ajax.readyState == 1) {
                //mostrar resultados en esta capa
                divResultado.innerHTML = ajax.responseText
            }
        }
        ajax.send(null)
        location.reload();
    }


    function deleteAllPreventa(idUser) {
        divResultado = document.getElementById('resultado');
        ajax = objetoAjax();
        ajax.open("GET", "DeleteALlPreVenta.php?idUser=" + idUser);
        location.reload();
        ajax.onreadystatechange = function () {
            // TODO revisar esto porque el estado es 4 no 1
            // if (ajax.readyState==4) {
            if (ajax.readyState == 1) {
                //mostrar resultados en esta capa
                divResultado.innerHTML = ajax.responseText
            }
        }
        ajax.send(null)
        location.reload();
    }


    function post() {

        var idproducto = $('#idproducto').val();
        var imagen = $('#imagen').val();
        var precio = $('#precio').val();
        var pventa = $('#pventa').val();
        var producto = $('#producto').val();
        var cantidad = $('#cantidad').val();
        var nuevoPrecio = $('#nuevoPrecio').val();


        $.post('../Controller/UpdatePreventa.php', {
                postidproducto: idproducto,
                postimagen: imagen,
                postprecio: precio,
                postpventa: pventa,
                postproducto: producto,
                postcantidad: cantidad,
                postnuevoPrecio: nuevoPrecio
            },
            function (data) {
                if (data == "1") {
                    $('#resultData').html('tienes mas  > de 18 anios')
                }
                if (data == "0") {
                    $('#resultData').html('tienes mas  < de 18 anios')
                }
            });
    }


    function insertarPreventa(idproducto) {
        //donde se mostrará el resultado de la eliminacion
        divResultado = document.getElementById('resultado');

        ajax = objetoAjax();
        ajax.open("GET", "insercionPos2.php?idproducto=" + idproducto);
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4) {
                //mostrar resultados en esta capa
                divResultado.innerHTML = ajax.responseText
            }
        }
        ajax.send(null)

    }


    /////////////////// INSERTAR NUEVO PRODUCTO MESA //////////////////////

    function insertarPedidoMesa(idproducto, iduser) {
        divResultado = document.getElementById('resultado');
        ajax = objetoAjax();
        ajax.open("GET", "InsertarPedidoMesa.php?idproducto=" + idproducto + "&iduser=" + iduser);
        location.reload();
        ajax.onreadystatechange = function () {
            // TODO revisar esto porque el estado es 4 no 1
            // if (ajax.readyState==4) {
            if (ajax.readyState == 1) {
                divResultado.innerHTML = ajax.responseText
            }
        }
        ajax.send(null)
        //location.reload();
        window.location.reload();
    }


    /////////////////// INSERTAR NUEVO PRODUCTO  LLEVAR//////////////////////

    function insertarPedidoLlevar(idproducto, iduser) {
        divResultado = document.getElementById('resultado');
        ajax = objetoAjax();
        ajax.open("GET", "InsertarPedidoLlevar.php?idproducto=" + idproducto + "&iduser=" + iduser);
        location.reload();
        ajax.onreadystatechange = function () {
            // TODO revisar esto porque el estado es 4 no 1
            // if (ajax.readyState==4) {
            if (ajax.readyState == 1) {
                divResultado.innerHTML = ajax.responseText
            }
        }
        ajax.send(null)
        //location.reload();
        window.location.reload();
    }

</script>

</body>
</html>

