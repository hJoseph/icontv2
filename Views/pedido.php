<?php
include_once('../Model/conexion.php');
$con = new conexion();
?>

<section class="panel">
    <!--    <div class="alert alert-success">-->
    <!--        <strong>Se adiciono un nuevo Pedido</strong>-->
    <!--    </div>-->

    <header align="center">

        <div align="center" class="alert alert-info">
            <strong>PRODUCTOS SOLICITADOS</strong>
        </div>
    </header>
    <div id="formulario" style="display:none;">
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <td width="20"><b>Imagen</b></td>
            <td><b>Producto</b></td>
            <td><b>Cant.</b></td>
            <td><b>Precio</b></td>
            <td><b>Total</b></td>
            <td><b>Tipo</b></td>
            <td><b>Opcion</b></td>
        </tr>
        <?php
        $showPreVenta = $con->getPreventaSales();
        while ($preventa = mysqli_fetch_array($showPreVenta)){
        ?>

        <tr>
            <td><img src="<?PHP echo url();
                echo '/Views/';
                echo $preventa['imagen'] ?>" width="60" height="60"></td>
            <td><b><?php echo $preventa['producto']; ?></b></td>
            <td><?php echo $preventa['cantidad']; ?></td>
            <td><?php echo $preventa['precio']; ?></td>
            <td><?php echo $preventa['totalPrecio']; ?></td>
            <td><?php echo $preventa['tipo']; ?></td>
            <td>
                <?PHP
                echo "<a style=\"cursor:pointer;\"  class='btn btn-success'   
                               onclick=\"editarPreventa('" . $preventa['idproducto'] . "','" . $preventa['tipo'] . "')\">
                               <i class='icon_pencil-edit'></i></a>";

                echo "<a style=\"cursor:pointer;\"  class='btn btn-danger'
                         onclick=\"deleteProducto('" . $preventa['idproducto'] . "','" . $preventa['tipo'] . "')\">
                <i class='icon_minus-box'></i></a>"; ?>
            </td>
            <?php } ?>
        </tr>

        <tr>
            <td colspan="3"></td>
            <td> Total :</td>
            <td>
                <b>
                    <h2>
                        <?php
                        $totalPreVenta = $con->getTotalPreventaSales();
                        while ($totalVenta = mysqli_fetch_array($totalPreVenta)) {
                            echo $totalVenta['total'];
                        } ?>
                    </h2>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <a data-toggle="modal" class="btn btn-primary enabled"
                   href="Factura.php?usuario=<?php echo $usuario; ?>&password=<?php echo $contrasena; ?>"
                   data-target="#myModal">
                    <strong> ACEPTAR</strong></a>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        </div><!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </td>
            <td colspan="3" align="rigth">
                <?PHP
                $userId = $con->getTotalPreventaSales();
                while ($valueUserId = mysqli_fetch_array($userId)) {
                    $idUserValue =$valueUserId['idUser'];
                }
                    if (isset($idUserValue)) {
                        echo "<a style=\"cursor:pointer;\"  class='btn btn-danger'
                         onclick=\"deleteAllPreventa('" . $idUserValue . "')\">
                      <i class='icon_minus-box'></i><strong> CANCELAR</strong></a>";
                    }
                //}
                ?>
            </td>
        </tr>


        <tr>
            <td colspan="7" align="center">
                <?php if (isset($messageAlerta)) { ?>
                    <div class="<?php echo $alerta; ?>">
                        <strong><?php echo $messageAlerta; ?></strong>
                    </div>
                <?php } ?>
            </td>
        </tr>

        </thead>
        <!--        <div id="formulario" style="display:none;">-->
        <!--        </div>-->
    </table>
    </div>
    <!-- /.table-responsive -->
    </div>
    <!--main content end-->
</section>