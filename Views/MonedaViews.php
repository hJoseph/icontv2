<?php
require "../Model/ModelUrl.php";
?>

<!DOCTYPE html>
<html lang="en">
<?php
include("head.php");
?>



<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () {
            Shadowbox.open({
                content: '<div><img src="<?php echo url() . "/Views/popup/welcome.jpg"?>" ></div>',
                player: "html",
                title: "Hola !!! ",
                width: 450,
                height: 201
            });
        }, 50);
    });
</script>
<style type="text/css">

    .html, body {
        font-family: Verdana, Geneva, sans-serif;
        font-size: 12px;
    }

    .ejemplo {
        float: left;
        width: 100%;
        padding: 0px;
        margin: 0px;
    }

    .ejemplo img {
        float: left;
        padding: 2px;
        border: 1px solid #999;
        margin-right: 10px;
        margin-bottom: 10px;
    }

</style>
<body>
<!-- container section start -->
<section id="container" class="">


    <header class="header dark-bg">
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i
                    class="icon_menu"></i></div>
        </div>

        <?PHP include("logo.php"); ?>

        <div class="nav search-row" id="top_menu">
            <!--  search form start -->
            <ul class="nav top-menu">
                <li>
                    <form class="navbar-form">
<!--                        <input class="form-control" placeholder="Search" type="text">-->
                    </form>
                </li>
            </ul>
            <!--  search form end -->
        </div>
        <?PHP include("DropDown.php"); ?>
    </header>
    <?PHP include("menu.php"); ?>
    </div>
    </aside>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><i class="fa fa-edit"></i><strong> REGISTROS DE DATOS DEL FACTURA </strong></h3>
                    <div class="<?php echo $alerta;?>" role="alert">
                       <b><?php echo $messageAlerta;?> </b>
                    </div>
                    <ol class="breadcrumb">
                        <?PHP include("menuOpciones.php"); ?>
                    </ol>
                </div>
            </div>

            <!--modal start-->

            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <section class="panel">

                            <header class="panel-heading">
                                <div class="panel-body">
                                    <div align="right">
                                    </div>

                                    <div id="add" class="modal fade" tabindex="-1" role="dialog"
                                         aria-labelledby="myModalLabel" aria-hidden="true">
                                        <form class="form-validate form-horizontal" name="form2" action="Registros.php" method="post">
                                            <input name="usuarioLogin" value="<?php echo $usuario;?>" type="hidden" >
                                            <input name="passwordLogin" value="<?php echo $password;?>" type="hidden" >
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true">x
                                                        </button>
                                                        <h3 id="myModalLabel" align="center">Registrar Nuevo
                                                            Usuario</h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <label for="proveedor"
                                                               class="control-label col-lg-2">Nombre:</label>
                                                        <div class="col-lg-10">
                                                            <input class="form-control input-lg m-bot15" id="nombre"  name="nombre" minlength="5" type="text" required/>
                                                        </div>
                                                        <br><br>
                                                        <label for="responsable"
                                                               class="control-label col-lg-2">Tipo:</label>
                                                        <div class="col-lg-10">
                                                            <select class="form-control input-lg m-bot15" name="tipo">
                                                                <option value="ADMINISTRADOR">ADMINISTRADOR</option>
                                                                <option value="VENTAS">VENTAS</option>

                                                            </select>
                                                            <input class="form-control input-lg m-bot15" id="login"   name="DDDD" minlength="5" type="hidden"/>
                                                        </div>
                                                        <label for="responsable"
                                                               class="control-label col-lg-2">Login:</label>
                                                        <div class="col-lg-10">
                                                            <input class="form-control input-lg m-bot15" id="login" name="login" minlength="5" type="text" required/>
                                                        </div>
                                                        <br><br>
                                                        <label for="responsable" class="control-label col-lg-2">Password:</label>
                                                        <div class="col-lg-10">
                                                            <input class="form-control input-lg m-bot15" id="password"
                                                                   name="password" minlength="5" type="text" required/>
                                                        </div>
                                                        <br><br><br>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-danger" data-dismiss="modal"  aria-hidden="true"><strong>Cerrar</strong></button>

                                                        <button name="a_nuevo" type="submit" class="btn btn-primary"><strong>Registrar</strong></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </header>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover"
                                           id="dataTables-example">
                                        <thead>
                                        <tr>
                                            <th><i class="fa fa-magic"></i> PAIS </th>
                                            <th><i class="fa fa-money"></i> MONEDA </th>
                                            <th><i class="fa fa-money"></i> CONTEXTO </th>
                                           <th><i class="icon_cog"></i> ACCIONES</th>

                                        </tr>
                                        </thead>
                                        <?php
                                        while ($datosUsuarioMoneda = mysqli_fetch_array($dataMoneda)) {  ?>
                                           <tr>
                                                <td><?php echo $datosUsuarioMoneda['pais']; ?></td>
                                               <td><?php echo $datosUsuarioMoneda['contexto']; ?></td>
                                               <td><?php echo $datosUsuarioMoneda['tipoMoneda']; ?></td>
                                                <td>
                                                    <a href="#a<?php echo $datosUsuarioMoneda[0]; ?>" role="button" class="btn btn-success" data-toggle="modal"><i  class="icon_check_alt2"></i></a>
                                                 </td>
                                            </tr>
                                            <div id="a<?php echo $datosUsuarioMoneda[0]; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <form class="form-validate form-horizontal" name="form2" action="RegistrosDataMoneda.php" method="post">
                                                    <input name="usuarioLogin" value="<?php echo $usuario;?>" type="hidden" >
                                                    <input name="passwordLogin" value="<?php echo $password;?>" type="hidden" >
                                                    <input type="hidden" name="idMoneda"  value="<?php echo $datosUsuarioMoneda['idMoneda']; ?>">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                                                </button>
                                                                <h3 id="myModalLabel" align="center">Elegir Moneda </h3>
                                                            </div>

                                                            <div class="modal-body">
                                                                <div class="form-group ">
                                                                    <br>
                                                                    <label for="propietario" class="control-label col-lg-2">Moneda:</label>
                                                                    <div class="col-lg-10">
                                                                        <select class="form-control input-lg m-bot15"
                                                                                name="moneda">
                                                                            <option value="Agentina">Agentina</option>
                                                                            <option value="EUA">EUA</option>
                                                                            <option value="Bolivia">Bolivia</option>
                                                                            <option value="Ecuador">Ecuador</option>
                                                                            <option value="Colombia">Colombia</option>
                                                                            <option value="Peru">Peru</option>
                                                                            <option value="Brasil">Brasil</option>
                                                                            <option value="Chile">Chile</option>
                                                                            <option value="Venezuela">Venezuela</option>
                                                                            <option value="Mexico">Mexico</option>
                                                                            <option value="Espania">Espania</option>
                                                                            <option value="Paraguay">Paraguay</option>
                                                                            <option value="Uruguay">Uruguay</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><strong>Cerrar</strong> </button>
                                                                    <button name="update_data_factura" type="submit"  class="btn btn-primary"><strong>Elegir Moneda</strong></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        <?php } ?>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>

                        </section>
                    </div>
                </div>



                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                        </section>
                    </div>
                </div>
            </div>


            <!---------- final aqui------->


        </section>
    </section>

    <!--main content end-->
</section>


<!-- container section end -->
<!-- javascripts -->
<script src="<?php echo url(); ?>/Views/js/jquery.js"></script>
<script src="<?php echo url(); ?>/Views/js/bootstrap.min.js"></script>
<!-- nicescroll -->
<script src="<?php echo url(); ?>/Views/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery.nicescroll.js" type="text/javascript"></script>
<!--custome script for all page-->
<script src="<?php echo url(); ?>/Views/js/scripts.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo url(); ?>/Views/js/jquery.dataTables.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/dataTables.bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>


</body>
</html>