<?php
require "../Model/ModelUrl.php";
?>

<!DOCTYPE html>
<html lang="en">
<?php
include("head.php");
?>

<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () {
            Shadowbox.open({
                content: '<div><img src="<?php echo url() . "/Views/popup/welcome.jpg"?>" ></div>',
                player: "html",
                title: "Hola !!! ",
                width: 450,
                height: 201
            });
        }, 50);
    });
</script>
<style type="text/css">

    .html, body {
        font-family: Verdana, Geneva, sans-serif;
        font-size: 12px;
    }

    .ejemplo {
        float: left;
        width: 100%;
        padding: 0px;
        margin: 0px;
    }

    .ejemplo img {
        float: left;
        padding: 2px;
        border: 1px solid #999;
        margin-right: 10px;
        margin-bottom: 10px;
    }

</style>
<body>
<!-- container section start -->
<section id="container" class="">


    <header class="header dark-bg">
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i
                        class="icon_menu"></i></div>
        </div>

        <?PHP include("logo.php"); ?>

        <div class="nav search-row" id="top_menu">
            <!--  search form start -->
            <ul class="nav top-menu">
                <li>
                    <form class="navbar-form">
<!--                        <input class="form-control" placeholder="Search" type="text">-->
                    </form>
                </li>
            </ul>
            <!--  search form end -->
        </div>
        <?PHP include("DropDown.php"); ?>
    </header>
    <?PHP include("menu.php"); ?>
    </div>
    </aside>


    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><i class="fa fa-suitcase"></i><strong> Stock</strong></h3>
                    <div class="<?php echo $alerta; ?>" role="alert">
                        <b><?php echo $messageAlerta; ?> </b>
                    </div>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="principal.php">Inicio</a></li>
                        <li><i class="fa fa-table"></i>Stock</li>

                    </ol>
                </div>
            </div>
            <!--<div align="right">
              <td><a class="btn btn-primary" href="#add" title=""><span class="icon_lightbulb_alt" data-original-title="Tooltip on left"></span> Agregar Nuevo</a></td>
                <button href="#add" title="" data-placement="left" data-toggle="tooltip" class="btn btn-primary tooltips" type="button" data-original-title="Nuvo Proveedor"><span class="icon_document_alt"></span> AGREGAR NUEVO PROVEEDOR</button>
            </div>--->

            <!--modal start-->

            <header class="panel-heading">
                <div class="panel-body">
                    <div align="right">

                        <a href="reporte_productos.php" class="btn btn-danger tooltips"><i
                                    class="fa fa-rotate-right"></i> EXPORTAR PDF </a>

                        <button href="#add" title="" data-placement="top" data-toggle="modal"
                                class="btn btn-primary tooltips" type="button" data-original-title="Nuevo Producto">
                            <span class="icon_bag_alt"></span>AGREGAR NUEVO PRODUCTO AL ALMACEN
                        </button>
                    </div>
                    <div id="add" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                         aria-hidden="true">
                        <form action="RegistroStock.php" method="post" enctype="multipart/form-data">
                            <input name="usuarioLogin" value="<?php echo $usuario; ?>" type="hidden">
                            <input name="passwordLogin" value="<?php echo $password; ?>" type="hidden">
                            <div class="modal-dialog" id="mdialTamanio">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x
                                        </button>
                                        <h3 id="myModalLabel" align="center">Registrar Informacion del Producto</h3>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <section class="panel">
                                                    <div><strong>Agregar Imagen</strong></div>
                                                    <br>
                                                    <input id="files" type="file" name="userfile"/>
                                                    <!--<input id="files" name="files" multiple="" type="file">-->
                                                    <!----VISTA PREVIA A ALA IMAGEN QUE SE VA A SUBIR-->
                                                    <output id="list-miniatura"></output>
                                                    <output id="list-datos"></output>
                                                </section>
                                            </div>
                                            <div class="col-lg-8">
                                                <section class="panel">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Proveedor:</label>
                                                        <div class="col-sm-4">
                                                            <select class="form-control input-lg m-bot15"
                                                                    name="proveedor">
                                                                <?php
                                                                while ($proveedor = mysqli_fetch_array($allProveedor)) {
                                                                    echo '<option value="' . $proveedor['proveedor'] . '">' . $proveedor['proveedor'] . '</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <label class="col-sm-2 control-label">Codigo:</label>
                                                        <div class="col-sm-4">
                                                            <input class="form-control input-lg m-bot15" id="codigo"
                                                                   name="codigo" type="text" required/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Descripcion:</label>
                                                        <div class="col-sm-10">
                                                            <input class="form-control input-lg m-bot15"
                                                                   id="descripcion" name="descripcion" type="text"
                                                                   required/>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <label for="pdistribuidor" class="control-label col-lg-2">Precio
                                                            de compra:</label>
                                                        <div class="col-lg-4">
                                                            <input class="form-control input-lg m-bot15"
                                                                   id="precioDeCompra" name="precioDeCompra"
                                                                   placeholder="0.00" type="text" required/>
                                                        </div>
                                                        <label for="pprofesional" class="control-label col-lg-2">Stock:</label>
                                                        <div class="col-lg-4">
                                                            <input class="form-control input-lg m-bot15"
                                                                   id="stock" name="stock"
                                                                   placeholder="0.00" type="text" required/>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ppublico" class="control-label col-lg-2">Alarma:</label>
                                                        <div class="col-lg-4">
                                                            <input class="form-control input-lg m-bot15" id="alarma"
                                                                   name="alarma" placeholder="0.00" type="text"
                                                                   required/>
                                                        </div>
                                                        <label for="pventa"
                                                               class="control-label col-lg-2">Fecha:</label>
                                                        <div class="col-lg-4">
                                                            <input class="form-control input-lg m-bot15" type="date"
                                                                   readonly name="fechaRegistro" autocomplete="off"
                                                                   value="<?php echo date('Y-m-d'); ?>">
                                                        </div>

                                                    </div>

                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><strong>Cerrar</strong>
                                        </button>
                                        <button name="nuevo_Producto" type="submit" class="btn btn-primary">
                                            <strong>Registrar Nuevo Producto</strong></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </header>

            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>IMAGEN</th>
                            <th>CODIGO</th>
                            <th>PRODUCTO</th>
                            <th>PRECIO DE COMPRA</th>
                            <th>STOCK</th>
                            <th>CANTIDAD VENDIDA</th>
                            <th>PROVEEDOR</th>
                            <th>ALARMA</th>
                            <th>FECHA REGISTRO</th>
                            <th>OPCIONES</th>
                        </tr>
                        </thead>
                        <?php while ($product = mysqli_fetch_array($allStockProducto)) { ?>
                            <tr>
                                <td><img src="<?PHP echo url();
                                    echo '/Views/';
                                    echo $product['imagen']; ?>" width="75" height="75"></td>


                                </td>
                                <td><?php echo $product['codigo']; ?></td>
                                <td><?php echo $product['nombreProducto']; ?></td>
                                <td><?php echo $product['precioCompra']; ?></td>
                                <td><?php echo $product['cantidadStock']; ?></td>
                                <td><?php echo $product['cantidadVendido']; ?></td>
                                <td><?php echo $product['proveedor']; ?></td>
                                <td><?php echo $product['alarma']; ?></td>
                                <td><?php echo $product['fechaRegistro']; ?></td>

                                <td>
                                    <a href="#a<?php echo $product[0]; ?>" role="button" class="btn btn-success"
                                       data-toggle="modal"><i class="icon_check_alt2"></i></a>
                                    <a href="RegistroStock.php?idborrar=<?php echo $product[0]; ?>&usuarioLogin=<?php echo $usuario; ?>&passwordLogin=<?php echo $password; ?>"
                                       class="btn btn-danger"><i class="icon_close_alt2"></i></a>

                                </td>
                            </tr>
                            <div id="a<?php echo $product[0]; ?>" class="modal fade" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true">
                                <form class="form-validate form-horizontal" name="form2" enctype="multipart/form-data" action="RegistroStock.php"
                                      method="post">
                                    <input type="hidden" id="idproducto" name="idproducto"
                                           value="<?php echo $product['idproducto']; ?>">
                                    <input type="hidden" name="imagen" value="<?php echo $product['imagen']; ?>">
                                    <input name="usuarioLogin" value="<?php echo $usuario; ?>" type="hidden">
                                    <input name="passwordLogin" value="<?php echo $password; ?>" type="hidden">

                                    <div class="modal-dialog" id="mdialTamanio">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">x
                                                </button>
                                                <h3 id="myModalLabel" align="center">Cambiar Informacion del
                                                    Producto</h3>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <section class="panel">
                                                            <img src="<?PHP echo url();
                                                            echo '/Views/';
                                                            echo $product['imagen']; ?>" width="250" height="250">
                                                            <br><br>
                                                            <div><strong>Cambiar Imagen</strong></div>
                                                            <?php  include("UploadViewImage.php");  ?>
                                                            <!--                                                            <div><strong>Cambiar Imagen</strong></div>-->
                                                            <!--                                                            <input id="files" type="file" name="userfile"/>-->
                                                            <!--                                                            <output id="list-miniaturaEdit"></output>-->
                                                            <!--                                                            <output id="list-datosEdit"></output>-->
                                                        </section>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <section class="panel">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Tipo
                                                                    Producto:</label>
                                                                <div class="col-sm-4">
                                                                    <select class="form-control input-lg m-bot15"
                                                                            name="proveedor">
                                                                        <option><?php echo $product['proveedor']; ?></option>
                                                                        <?php  include("AllProveedor.php");  ?>
                                                                    </select>
                                                                </div>
                                                                <label class="col-sm-2 control-label">Codigo:</label>
                                                                <div class="col-sm-4">
                                                                    <input class="form-control input-lg m-bot15"
                                                                           id="codigo"
                                                                           name="codigo" type="text"
                                                                           value="<?php echo $product['codigo']; ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Descripcion:</label>
                                                                <div class="col-sm-10">
                                                                    <input class="form-control input-lg m-bot15"
                                                                           id="descripcion" name="descripcion"
                                                                           type="text"
                                                                           value="<?php echo $product['nombreProducto']; ?>"/>
                                                                </div>

                                                            </div>
                                                            <div class="form-group">
                                                                <label for="pdistribuidor"
                                                                       class="control-label col-lg-2">Precio
                                                                    de Compra:</label>
                                                                <div class="col-lg-4">
                                                                    <input class="form-control input-lg m-bot15"
                                                                           id="precioDeCompra" name="precioDeCompra"
                                                                           placeholder="0.00" type="text"
                                                                           value="<?php echo $product['precioCompra']; ?>"/>
                                                                </div>
                                                                <label for="pprofesional"
                                                                       class="control-label col-lg-2">Cantidad en Stock:</label>
                                                                <div class="col-lg-4">
                                                                    <input class="form-control input-lg m-bot15"
                                                                           id="stock" name="stock"
                                                                           placeholder="0.00" type="text"
                                                                           value="<?php echo $product['cantidadStock']; ?>"/>
                                                                </div>

                                                            </div>


                                                            <div class="form-group">
                                                                <label for="ppublico" class="control-label col-lg-2">Alarma:</label>
                                                                <div class="col-lg-4">
                                                                    <input class="form-control input-lg m-bot15"
                                                                           id="alarma"
                                                                           name="alarma" placeholder="0.00"
                                                                           type="text"
                                                                           value="<?php echo $product['alarma']; ?>"/>
                                                                </div>
                                                                <label for="pventa"
                                                                       class="control-label col-lg-2">Fecha:</label>
                                                                <div class="col-lg-4">
                                                                    <input class="form-control input-lg m-bot15"
                                                                           type="date"
                                                                           readonly name="fechaRegistro"
                                                                           autocomplete="off"
                                                                           value="<?php echo date('Y-m-d'); ?>">
                                                                </div>
                                                            </div>
                                                        </section>

                                                    </div>
                                                </div>


                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">
                                                    <strong>Cerrar</strong></button>
                                                <button name="update_producto" type="submit" class="btn btn-primary">
                                                    <strong>Editar</strong>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>


                        <?php } ?>


                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>


        </section>
    </section>
    <!--main content end-->
</section>
<!-- container section end -->
<!-- javascripts -->
<script src="<?php echo url(); ?>/Views/js/jquery.js"></script>
<script src="<?php echo url(); ?>/Views/js/bootstrap.min.js"></script>
<!-- nicescroll -->
<script src="<?php echo url(); ?>/Views/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery.nicescroll.js" type="text/javascript"></script>
<!--custome script for all page-->
<script src="<?php echo url(); ?>/Views/js/scripts.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo url(); ?>/Views/js/jquery.dataTables.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/dataTables.bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script>
    function handleFileSelect(evt) {
        evt.stopPropagation();
        evt.preventDefault();

        var files = evt.dataTransfer.files; // FileList object.
        // files is a FileList of File objects. List some properties.
        var output = [];
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['Nombre: ', escape(theFile.name), ' || Tamanio: ', escape(theFile.size), ' bytes || type: ', escape(theFile.type), '<br /><img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '"style="width:100%;"/><br />'].join('');
                    document.getElementById('list-miniatura').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
        document.getElementById('list-datos').innerHTML = '<ul>' + output.join('') + '</ul>';
    }

    function handleDragOver(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    }

    // Setup the dnd listeners.
    var dropZone = document.getElementById('drop_zone');
    dropZone.addEventListener('dragover', handleDragOver, false);
    dropZone.addEventListener('drop', handleFileSelect, false);
</script>

<script>
    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<br /><img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '" style="width:100%;"/><br />'].join('');
                    document.getElementById('list-miniatura').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>


</body>
</html>