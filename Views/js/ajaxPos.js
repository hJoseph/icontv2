//Desarrollado por Jesus Liñán
//webmaster@ribosomatic.com
//ribosomatic.com
//Puedes hacer lo que quieras con el código
//pero visita la web cuando te acuerdes

function objetoAjax(){
	var xmlhttp=false;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
		}
	}

	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}

function insertarPedidoMesa(idproducto, iduser){
	divResultado = document.getElementById('resultado');
	ajax=objetoAjax();
	ajax.open("GET", "InsertarPedidoMesa.php?idproducto=" + idproducto + "&iduser=" + iduser);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			divResultado.innerHTML = ajax.responseText
		}
	}
	ajax.send()
}

function insertarPedidoLlevar(idproducto, iduser) {
	divResultado = document.getElementById('resultado');
	ajax = objetoAjax();
	ajax.open("GET", "InsertarPedidoLlevar.php?idproducto=" + idproducto + "&iduser=" + iduser);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			divResultado.innerHTML = ajax.responseText
		}
	}
	ajax.send()
}


function deleteAllPreventa(idUser) {
	divResultado = document.getElementById('resultado');
	ajax = objetoAjax();
	ajax.open("GET", "DeleteALlPreVenta.php?idUser=" + idUser);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			divResultado.innerHTML = ajax.responseText
		}
	}
	ajax.send()
}

function editarPreventa(idproducto, tipo) {
	divFormulario = document.getElementById('formulario');
	ajax = objetoAjax();
	ajax.open("POST", "EditPreventa.php");
	ajax.onreadystatechange = function () {
		if (ajax.readyState == 4) {
			divFormulario.innerHTML = ajax.responseText
			divFormulario.style.display = "block";
		}
	}
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("idproducto=" + idproducto + "&tipo=" + tipo)
}


function deleteProducto(idproducto, tipo){

	divResultado = document.getElementById('resultado');
	ajax = objetoAjax();
	ajax.open("GET", "DeleteOnlyPreVenta.php?idproducto="+idproducto+ "&tipo=" + tipo);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			divResultado.innerHTML = ajax.responseText
		}
	}
	ajax.send()
}


