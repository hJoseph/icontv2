<?php
require "../Model/ModelUrl.php";
?>

<!DOCTYPE html>
<html lang="en">
<?php
include("head.php");
?>

<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () {
            Shadowbox.open({
                content: '<div><img src="<?php echo url() . "/Views/popup/welcome.jpg"?>" ></div>',
                player: "html",
                title: "Hola !!! ",
                width: 450,
                height: 201
            });
        }, 50);
    });
</script>
<style type="text/css">

    .html, body {
        font-family: Verdana, Geneva, sans-serif;
        font-size: 12px;
    }

    .ejemplo {
        float: left;
        width: 100%;
        padding: 0px;
        margin: 0px;
    }

    .ejemplo img {
        float: left;
        padding: 2px;
        border: 1px solid #999;
        margin-right: 10px;
        margin-bottom: 10px;
    }

</style>
<body>
<!-- container section start -->
<section id="container" class="">

    <header class="header dark-bg">
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i
                        class="icon_menu"></i></div>
        </div>

        <?PHP include("logo.php"); ?>

        <div class="nav search-row" id="top_menu">
            <!--  search form start -->
            <ul class="nav top-menu">
                <li>
                    <form class="navbar-form">
<!--                        <input class="form-control" placeholder="Search" type="text">-->
                    </form>
                </li>
            </ul>
            <!--  search form end -->
        </div>
        <?PHP include("DropDown.php"); ?>
    </header>
    <?PHP include("menu.php"); ?>
    </div>
    </aside>


    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><i class="fa fa-edit"></i><strong> REGISTROS DEL SISTEMA </strong></h3>
                    <div class="<?php echo $alerta; ?>" role="alert">
                        <b><?php echo $messageAlerta; ?> </b>
                    </div>
                    <ol class="breadcrumb">
                        <?PHP include("menuOpciones.php"); ?>
                    </ol>
                </div>
            </div>

            <!--modal start-->

            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <section class="panel">

                            <header class="panel-heading">
                                Usuarios
                            </header>
                            <header class="panel-heading">
                                <div class="panel-body">
                                    <div align="right">
                                        <button href="#add" title="" data-placement="left" data-toggle="modal"
                                                class="btn btn-primary tooltips" type="button"
                                                data-original-title="Nuevo Usuario"><span class="fa fa-plus"></span>
                                            AGREGAR NUEVO USUARIO
                                        </button>
                                    </div>

                                    <div id="add" class="modal fade" tabindex="-1" role="dialog"
                                         aria-labelledby="myModalLabel" aria-hidden="true">
                                        <form class="form-validate form-horizontal" name="form2" action="Registros.php"
                                              method="post" enctype="multipart/form-data">
                                            <input name="usuarioLogin" value="<?php echo $usuario; ?>" type="hidden">
                                            <input name="passwordLogin" value="<?php echo $password; ?>" type="hidden">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true">x
                                                        </button>
                                                        <h3 id="myModalLabel" align="center">Registrar Nuevo
                                                            Usuario</h3>
                                                    </div>
                                                    <div class="modal-body">

                                                        <section class="panel" class="col-lg-6">
                                                            <div><strong>Agregar Imagen de usuario</strong></div>
                                                            <input id="files" type="file" name="userfile"/>
                                                            <!--<input id="files" name="files" multiple="" type="file">-->
                                                            <!----VISTA PREVIA A ALA IMAGEN QUE SE VA A SUBIR-->
                                                            <output id="list-miniatura"></output>
                                                            <output id="list-datos"></output>
                                                        </section>


                                                        <label for="proveedor"
                                                               class="control-label col-lg-2">Nombre:</label>
                                                        <div class="col-lg-10">
                                                            <input class="form-control input-lg m-bot15" id="nombre"
                                                                   name="nombre" minlength="5" type="text" required/>
                                                        </div>
                                                        <br><br>
                                                        <label for="responsable"
                                                               class="control-label col-lg-2">Tipo:</label>
                                                        <div class="col-lg-10">
                                                            <select class="form-control input-lg m-bot15" name="tipo">
                                                                <option value="ADMINISTRADOR">ADMINISTRADOR</option>
                                                                <option value="VENTAS">VENTAS</option>

                                                            </select>
                                                            <input class="form-control input-lg m-bot15" id="login"
                                                                   name="DDDD" minlength="5" type="hidden"/>
                                                        </div>
                                                        <label for="responsable"
                                                               class="control-label col-lg-2">Login:</label>
                                                        <div class="col-lg-10">
                                                            <input class="form-control input-lg m-bot15" id="login"
                                                                   name="login" minlength="5" type="text" required/>
                                                        </div>
                                                        <br><br>
                                                        <label for="responsable" class="control-label col-lg-2">Password:</label>
                                                        <div class="col-lg-10">
                                                            <input class="form-control input-lg m-bot15" id="password"
                                                                   name="password" minlength="5" type="text" required/>
                                                        </div>
                                                        <br><br><br>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-danger" data-dismiss="modal"
                                                                aria-hidden="true"><strong>Cerrar</strong></button>

                                                        <button name="a_nuevo" type="submit" class="btn btn-primary">
                                                            <strong>Registrar</strong></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </header>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover"
                                           id="dataTables-example">
                                        <thead>
                                        <tr>
                                            <th><i class="icon_images"></i> FOTO</th>
                                            <th><i class="icon_contacts"></i> NOMBRE</th>
                                            <th><i class="icon_folder"></i> TIPO</th>
                                            <th><i class="icon_contacts_alt"></i> LOGIN</th>
                                            <th><i class="icon_key"></i> PASSWORD</th>
                                            <th><i class="icon_cog"></i> ACCIONES</th>

                                        </tr>
                                        </thead>
                                        <?php
                                        while ($datosUsuario = mysqli_fetch_array($allUsuarios)) { ?>

                                            <tr>
                                                <td><img src="<?PHP echo url();
                                                    echo '/Views/';
                                                    echo $datosUsuario['foto']; ?>" width="50" height="50"></td>
                                                <td><?php echo $datosUsuario['nombre']; ?></td>
                                                <td><?php echo $datosUsuario['tipo']; ?></td>
                                                <td><?php echo $datosUsuario['login']; ?></td>
                                                <td><?php echo $datosUsuario['password']; ?></td>
                                                <td>
                                                    <a href="#a<?php echo $datosUsuario[0]; ?>" role="button"
                                                       class="btn btn-success" data-toggle="modal"><i
                                                                class="icon_check_alt2"></i></a>
                                                    <a href="Registros.php?idborrar=<?php echo $datosUsuario[0]; ?>&usuarioLogin=<?php echo $usuario; ?>&passwordLogin=<?php echo $password; ?>"
                                                       class="btn btn-danger"><i class="icon_close_alt2"></i></a>
                                                </td>
                                            </tr>
                                            <div id="a<?php echo $datosUsuario[0]; ?>" class="modal fade" tabindex="-1"
                                                 role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <form class="form-validate form-horizontal" name="form2"
                                                      action="Registros.php" method="post"  enctype="multipart/form-data" >
                                                    <input name="usuarioLogin" value="<?php echo $usuario; ?>"
                                                           type="hidden">
                                                    <input name="passwordLogin" value="<?php echo $password; ?>"
                                                           type="hidden">
                                                    <input type="hidden" name="idUsuario"
                                                           value="<?php echo $datosUsuario['id_usu']; ?>">
                                                    <input type="hidden" name="imagen" value="<?php echo $datosUsuario['foto']; ?>">

                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">×
                                                                </button>
                                                                <h3 id="myModalLabel" align="center">Cambiar Informacion
                                                                    del Usuario</h3>
                                                            </div>
                                                            <div class="modal-body">
                                                                <img src="<?PHP echo url();
                                                                echo '/Views/';
                                                                echo $datosUsuario['foto']; ?>" width="250" height="250">
                                                                <br><br>
                                                                <section class="panel" class="col-lg-6">
                                                                    <div><strong>Cambiar Imagen de usuario</strong></div>
                                                                    <?php  include("UploadViewImage.php");  ?>
                                                                </section>


                                                                <div class="form-group ">
                                                                    <label for="proveedor"
                                                                           class="control-label col-lg-2">Nombre:</label>
                                                                    <div class="col-lg-10">
                                                                        <input class="form-control input-lg m-bot15"
                                                                               type="text" name="nombre"
                                                                               value="<?php echo $datosUsuario['nombre']; ?>">
                                                                        <input type="hidden" name="idUsuario"
                                                                               value="<?php echo $datosUsuario['id_usu']; ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="responsable"
                                                                           class="control-label col-lg-2">Tipo:</label>
                                                                    <div class="col-lg-10">
                                                                        <select class="form-control input-lg m-bot15"
                                                                                name="tipo">
                                                                            <option value="<?php echo $datosUsuario['tipo']; ?>"><?php echo $datosUsuario['tipo']; ?></option>
                                                                            <option value="ADMINISTRADOR">
                                                                                ADMINISTRADOR
                                                                            </option>
                                                                            <option value="VENTAS">VENTAS</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="direccion"
                                                                           class="control-label col-lg-2">Login:</label>
                                                                    <div class="col-lg-10">
                                                                        <input class="form-control input-lg m-bot15"
                                                                               type="text" name="login"
                                                                               value="<?php echo $datosUsuario['login']; ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="telefono"
                                                                           class="control-label col-lg-2">Password:</label>
                                                                    <div class="col-lg-10">
                                                                        <input class="form-control input-lg m-bot15"
                                                                               type="text" name="password"
                                                                               value="<?php echo $datosUsuario['password']; ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn btn-default" data-dismiss="modal"
                                                                            aria-hidden="true"><strong>Cerrar</strong>
                                                                    </button>
                                                                    <button name="update_data" type="submit"
                                                                            class="btn btn-primary"><strong>Actualizar
                                                                            Datos</strong></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        <?php } ?>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>

                        </section>
                    </div>
                </div>


                </table>
            </div>
            <!-- /.table-responsive -->
            </div>
        </section>
        </div>
        </div>
        </div>


        <!---------- final aqui------->


    </section>
</section>

<!--main content end-->
</section>


<!-- container section end -->
<!-- javascripts -->
<script src="<?php echo url(); ?>/Views/js/jquery.js"></script>
<script src="<?php echo url(); ?>/Views/js/bootstrap.min.js"></script>
<!-- nicescroll -->
<script src="<?php echo url(); ?>/Views/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery.nicescroll.js" type="text/javascript"></script>
<!--custome script for all page-->
<script src="<?php echo url(); ?>/Views/js/scripts.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo url(); ?>/Views/js/jquery.dataTables.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/dataTables.bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>

<script>
    function handleFileSelect(evt) {
        evt.stopPropagation();
        evt.preventDefault();

        var files = evt.dataTransfer.files; // FileList object.
        // files is a FileList of File objects. List some properties.
        var output = [];
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['Nombre: ', escape(theFile.name), ' || Tamanio: ', escape(theFile.size), ' bytes || type: ', escape(theFile.type), '<br /><img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '"style="width:50%;"/><br />'].join('');
                    document.getElementById('list-miniatura').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
        document.getElementById('list-datos').innerHTML = '<ul>' + output.join('') + '</ul>';
    }

    function handleDragOver(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    }

    // Setup the dnd listeners.
    var dropZone = document.getElementById('drop_zone');
    dropZone.addEventListener('dragover', handleDragOver, false);
    dropZone.addEventListener('drop', handleFileSelect, false);
</script>

<script>
    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<br /><img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '" style="width:50%;"/><br />'].join('');
                    document.getElementById('list-miniatura').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    document.getElementById('files').addEventListener('change', handleFileSelect, false);
</script>


</body>
</html>