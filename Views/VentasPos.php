<?php
require "../Model/ModelUrl.php";
?>


<!DOCTYPE html>
<html lang="es">
<?php include("head.php"); ?>
<body>

<!-- container section start -->
<section id="container" class="sidebar-closed">
    <!--header start-->
    <header class="header dark-bg">

        <style>
            .textbox {
                border: 1px solid #6a98e6;
                font-size: 23px;
                font-family: Arial, Verdana;
                padding-left: 7px;
                padding-right: 7px;
                padding-top: 10px;
                padding-bottom: 1px;
                border-radius: 4px;
                -moz-border-radius: 4px;
                -webkit-border-radius: 4px;
                -o-border-radius: 4px;
                background: #FFFFFF;
                background: linear-gradient(left, #FFFFFF, #6a98e6);
                background: -moz-linear-gradient(left, #FFFFFF, #F7F9FA);
                background: -webkit-linear-gradient(left, #FFFFFF, #F7F9FA);
                background: -o-linear-gradient(left, #FFFFFF, #F7F9FA);
                color: #2E3133;
            }

            .textbox:hover {
                color: #2E3133;
                border-color: #ff0000;
            }
        </style>
        <style>
            .textbox2 {
                border: 1px solid #6a98e6;
                font-size: 23px;
                font-family: Arial, Verdana;
                padding-left: 7px;
                padding-right: 7px;
                padding-top: 0px;
                padding-bottom: 1px;
                border-radius: 4px;
                -moz-border-radius: 4px;
                -webkit-border-radius: 4px;
                -o-border-radius: 4px;
                background: #FFFFFF;
                background: linear-gradient(left, #FFFFFF, #6a98e6);
                background: -moz-linear-gradient(left, #FFFFFF, #F7F9FA);
                background: -webkit-linear-gradient(left, #FFFFFF, #F7F9FA);
                background: -o-linear-gradient(left, #FFFFFF, #F7F9FA);
                color: #2E3133;
            }

            .textbox2:hover {
                color: #2E3133;
                border-color: #ff0000;
            }
        </style>
        <style>
            p:hover {
                background-color: yellow;
                border-color: #ff0000;
            }
        </style>
        <style type="text/css">
            a.nounderline:link {
                text-decoration: none;
            }
        </style>
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i
                        class="icon_menu"></i></div>
        </div>
        <?PHP include("logo.php"); ?>
        <div class="nav search-row" id="top_menu">
            <!--  search form start -->
            <ul class="nav top-menu">
                <li>
                    <form class="navbar-form">
                        <!--                        <input class="form-control" placeholder="Search" type="text">-->
                    </form>
                </li>
            </ul>
            <!--  search form end -->
        </div>
        <?PHP include("DropDown.php"); ?>
    </header>
    <!--header end-->
    <?PHP include("menu.php"); ?>
    </div>
    </aside>
    <!--sidebar end-->
    <!--main content start-->

    <!--main content start-->
    <br>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <p>Hi!</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <!-- page start-->
                        <div class="row">
                            <div class="col-sm-7">

                                <section class="panel">
                                    <div class="row">
                                        <header class="panel-heading tab-bg-primary ">

                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a data-toggle="tab" href="#home">PRODUCTOS</a>
                                                </li>
                                            </ul>

                                        </header>
                                        <div class="panel-body">
                                            <div class="tab-content">
                                                <div id="home" class="tab-pane active">

                                                    <table class="table table-striped table-bordered table-hover"
                                                           id="dataTables-example">
                                                        <thead>
                                                        <tr>

                                                        </tr>
                                                        </thead>

                                                        <?PHP
                                                        include("Producto.php");
                                                        ?>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                            </div>
                            <div class="col-sm-5">
                                <div id="resultado">
                                    <?php
                                    include('pedido.php');
                                    ?>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </section>
    </section>

    <!--main content end-->
</section>
<!-- container section end -->


<!--------------   HOLA ESTO ES PARA MODIFICAR EL BUSCADOR JQUERY  ------------------->


<script src="<?php echo url(); ?>/Views/js/jquery.js"></script>
<script src="<?php echo url(); ?>/Views/js/bootstrap.min.js"></script>
<!-- nicescroll -->
<script src="<?php echo url(); ?>/Views/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery.nicescroll.js" type="text/javascript"></script>
<!--custome script for all page-->
<script src="<?php echo url(); ?>/Views/js/scriptsb.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo url(); ?>/Views/js/jquery.dataTables.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/dataTables.bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script language="JavaScript" type="text/javascript" src="<?php echo url(); ?>/Views/js/ajaxPos.js"></script>

</body>
</html>